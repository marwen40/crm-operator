package main;


import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import java.util.Date;

//import org.apache.pdfbox.pdmodel.PDPageContentStream;
//import org.apache.pdfbox.pdmodel.font.PDType1Font;

//import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
//import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
//import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;

import com.itextpdf.text.pdf.PdfWriter;

import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Store;
import tn.esprit.CRM.persistence.StoreProduit;

public class PdfUtils {

	
	 public static String addDocument(Product p,Store s , int quantity) throws DocumentException, URISyntaxException, IOException {
	    	Document document = new Document();
	    	String pdfName = "facture_"+s.getNom_Store()+"_"+String.valueOf(new Date().getTime())+".pdf";
	    	PdfWriter.getInstance(document, new FileOutputStream(
				"C:\\wamp64\\www\\factures\\"+pdfName));
	    	 
	    	document.open();
	    	
	    	Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
	    	
	    	Paragraph chunk = new Paragraph("Store Name : " +s.getNom_Store(), font);	 
	    	document.add(chunk);
	    	
	    	chunk = new Paragraph("Product name : " +p.getName_product(), font);	 
	    	document.add(chunk);
	    	
	    	chunk = new Paragraph("Added quantity : " +quantity, font);	 
	    	document.add(chunk);
	    	
	    	chunk = new Paragraph("Added Date : " +new Date(), font);	 
	    	document.add(chunk);
	    	
	    	chunk = new Paragraph("   ", font);	 
	    	document.add(chunk);
	   	 
	    	chunk = new Paragraph("   ", font);	 
	    	document.add(chunk);
	    	
	    	
	    	 
	    	document.close();
	    	return pdfName;
    }
	 
/*	private static void addTableHeader(PdfPTable table) {
	    Stream.of("Nom", "Fonction", "Hébergement")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	
	*/
}
