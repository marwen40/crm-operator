package main;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.CRM.persistence.Category;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.StoreProduit;
import tn.esprit.CRM.services.CategoryServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;
import tn.esprit.CRM.services.StaffServiceRemote;
import tn.esprit.CRM.services.StoreServiceRemote;

public class Utilislam {
	static String jndiStore ="CRM-ear/CRM-ejb/StoreService!tn.esprit.CRM.services.StoreServiceRemote";
    static String jndiProduit = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
    static String jndiCategory = "CRM-ear/CRM-ejb/CategoryService!tn.esprit.CRM.services.CategoryServiceRemote";
    static String jndi = "CRM-ear/CRM-ejb/StaffService!tn.esprit.CRM.services.StaffServiceRemote";
    
    //**********************Retourner la quantité de produits par boutique********************************************//
	
    public static HashMap<String, Integer> getQtyByStore(List<StoreProduit> listsp){
		HashMap<String, Integer> hashMap = new HashMap<>();
		for(StoreProduit sp: listsp){
			String nomStore = sp.getStore().getNom_Store();
			if(hashMap.get(nomStore)==null){
				hashMap.put(nomStore, sp.getQuantity());
			}else{
				hashMap.put(nomStore, hashMap.get(nomStore) + sp.getQuantity() );
			}
		}
		
		return hashMap;
	}
    
    //**********************Retourner la quantité de tous les produits********************************************//

	
	public static HashMap<Product, Integer> getQtyByProduct(List<StoreProduit> listsp){
		HashMap<Product, Integer> hashMap = new HashMap<>();
		for(StoreProduit sp: listsp){
			if(hashMap.get(sp.getProduct())==null){
				hashMap.put(sp.getProduct(), sp.getQuantity());
			}else{
				hashMap.put(sp.getProduct(), hashMap.get(sp.getProduct()) + sp.getQuantity() );
			}
		}
		
		return hashMap;
	}
	
	public static ProductServiceRemote getProductService(){
		Context context;
		ProductServiceRemote productServiceRemote = null;
		try {
			context = new InitialContext();
		    productServiceRemote = (ProductServiceRemote) context.lookup(jndiProduit);	
			 
		} catch (NamingException e) {
			e.printStackTrace();
			System.out.println("error binding !!");
		}
		
		return productServiceRemote;
	}
	
	public static StoreServiceRemote getStoreService(){
		Context context;
		StoreServiceRemote storeServiceRemote = null;
		try {
			context = new InitialContext();
			storeServiceRemote = (StoreServiceRemote) context.lookup(jndiStore);	
			 
		} catch (NamingException e) {
			e.printStackTrace();
			System.out.println("error binding !!");
		}
		
		return storeServiceRemote;
	}
	
	public static CategoryServiceRemote getCategoryService(){
		Context context;
		CategoryServiceRemote categoryServiceRemote = null;
		try {
			context = new InitialContext();
			categoryServiceRemote = (CategoryServiceRemote) context.lookup(jndiCategory);	
			 
		} catch (NamingException e) {
			e.printStackTrace();
			System.out.println("error binding !!");
		}
		
		return categoryServiceRemote;
	}
	
	
	//***********************************************Send Email********************************************//

	public static void sendMail(String email,String sujet,String contenu) {
		
		final String username = "teammates322@gmail.com";
		final String password = "TeamMatesProjetPdev4INFOB123";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(email));
			message.setSubject(sujet);
			message.setText(contenu);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	//ONS
	public static StaffServiceRemote getStaffService(){
		Context context;
		StaffServiceRemote StaffServiceRemote = null;
		try {
			context = new InitialContext();
			StaffServiceRemote = (StaffServiceRemote) context.lookup(jndi);	
			 
		} catch (NamingException e) {
			e.printStackTrace();
			System.out.println("error binding !!");
		}
		
		return StaffServiceRemote;
	}
	
	
	
}
