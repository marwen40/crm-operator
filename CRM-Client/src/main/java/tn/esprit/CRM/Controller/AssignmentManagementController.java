package tn.esprit.CRM.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import main.Utilislam;
import tn.esprit.CRM.services.CategoryServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;
import tn.esprit.CRM.services.StoreServiceRemote;

public class AssignmentManagementController implements Initializable{
	
    @FXML
    private JFXComboBox<String> StoreComboBox;

    @FXML
    private JFXButton AignBtn;

    @FXML
    private JFXComboBox<String> ProductComboBox;

    @FXML
    private JFXSlider QuantitySlider;

    @FXML
    private JFXComboBox<?> productionComboBox;

    @FXML
    private DatePicker newDate;

    @FXML
    private JFXTextField newQuantity;

    @FXML
    private TableView<?> ProductionTable;

    @FXML
    private TableColumn<?, ?> descriptionT;

    @FXML
    private TableColumn<?, ?> dateT;

    @FXML
    private TableColumn<?, ?> employeeT;

    @FXML
    private TableColumn<?, ?> productT;

    @FXML
    private TableColumn<?, ?> quantityT;

    @FXML
    private TableColumn<?, ?> statusT;

    @FXML
    private JFXTextField searchText;

    @FXML
    private Label sortLabel;

    @FXML
    private JFXComboBox<?> sortCombobox;

    @FXML
    private JFXButton refreshBtn;

    @FXML
    private JFXButton searchBtn;

    @FXML
    private JFXButton returnBtn;

    @FXML
    void AssignProductToStore(ActionEvent event) {
    	

    }

    
    private final static String jndi2 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
    private final static String jndi3 = "CRM-ear/CRM-ejb/StoreService!tn.esprit.CRM.services.StoreServiceRemote";
    
    
    @FXML
    void Return(ActionEvent event) {

    }

    @FXML
    void SortBy(ActionEvent event) {

    }

    @FXML
    void UpdateProduction(ActionEvent event) {

    }

    @FXML
    void refresh(ActionEvent event) {

    }

    @FXML
    void search(KeyEvent event) {

    }

    @FXML
    void setProductionAttributes(ActionEvent event) {

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		// ************************Initialisation de la liste des produits*****************************//
		
		List<String> produits = new ArrayList<>();
		try {
			Context context = new InitialContext();
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi2);
			List<String> productOld = productServiceRemote.DisplayALlProductsNames();
			for (String prod : productOld) {
				produits.add(prod);
				ObservableList<String> items = FXCollections.observableArrayList(produits);
				ProductComboBox.setItems(items);
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		// ************************ Initialisation de la liste des stores *****************************//
		
		List<String> stores = new ArrayList<>();
		try {
			Context context = new InitialContext();
			StoreServiceRemote storeServiceRemote = (StoreServiceRemote) context.lookup(jndi3);
			List<String> storeOld = storeServiceRemote.DisplayALlStoresNames();
			for (String st : storeOld) {
				produits.add(st);
				ObservableList<String> itemz = FXCollections.observableArrayList(stores);
				ProductComboBox.setItems(itemz);
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		
		
		
	}

}
