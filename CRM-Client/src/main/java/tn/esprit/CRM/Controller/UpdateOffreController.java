package tn.esprit.CRM.Controller;

import java.net.URL;

import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.services.OffreServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

import java.time.LocalDate;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UpdateOffreController implements Initializable {

    @FXML
    private Label OffreTitle;

    @FXML
    private JFXComboBox<String> Product_LD;

    @FXML
    private JFXDatePicker startDate;

    @FXML
    private JFXDatePicker endDate;

    @FXML
    private JFXButton updateBtn;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private Label fileDirectory;

    @FXML
    private JFXTextField gain_tf;
    public static Offre offre;
    private String jndi = "CRM-ear/CRM-ejb/OffreService!tn.esprit.CRM.services.OffreServiceRemote";
    private String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";


    
    @FXML
    void CancelUpdate(ActionEvent event) {
    	Stage stage = (Stage) cancelBtn.getScene().getWindow();
		stage.close();

    }
    
    @FXML
    void updateTask(ActionEvent event) throws NamingException {
    	 AjoutOffreController aoc = new  AjoutOffreController();
    	 Offre offre  = aoc.offre;
    		Context context = new InitialContext();
    		OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);
    		ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
    		offre.setTitle_offre( OffreTitle.getText());
    		offre.setPercent_gain_offre(Float.parseFloat(gain_tf.getText()));
    		String lproduit = Product_LD.getValue();
    		
    		offre.setStart_date_offre(java.sql.Date.valueOf(startDate.getValue()));
    		offre.setEnd_date_offre(java.sql.Date.valueOf(endDate.getValue()));
    		Offre g =offreServiceRemote.update(offre);
    		productServiceRemote.assignProductToOfferCyrine(g.getId_offre(),productServiceRemote.findIdProductByTitleCyrine(lproduit));
    		Stage stage = (Stage) updateBtn.getScene().getWindow();
    		//tc.display();
    		stage.close();
    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		   
		List<String> categories = new ArrayList<>();
  		try 
  		{
  			Context context = new InitialContext();
  			//CategoryServiceRemote categoryServiceRemote = (CategoryServiceRemote) context.lookup(jndi2);
  			ProductServiceRemote categoryServiceRemote  = (ProductServiceRemote) context.lookup(jndi1);
  			List<String> categoryOld =categoryServiceRemote.DisplayALlProductsNamesCyrine();
  			for(String category:categoryOld)
  			{
  				categories.add(category);
  				ObservableList<String> items =FXCollections.observableArrayList(categories);
  				Product_LD.setItems(items);		
  			}			
  		} 
  		catch (NamingException e) 
  		{
  			e.printStackTrace();
  		}
		updateBtn.setCursor(Cursor.HAND);
		cancelBtn.setCursor(Cursor.HAND);
		AjoutOffreController aoc = new  AjoutOffreController();
		 Offre offre  = aoc.offre;
		 OffreTitle.setText(offre.getTitle_offre());
		 gain_tf.setText(offre.getPercent_gain_offre().toString());
				 //offre.getPercent_gain_offre());
		 Product_LD.setValue(offre.getProduct().getName_product());
		Date start = (Date) offre.getStart_date_offre();
		 LocalDate date1 = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 startDate.setValue(date1);
		 Date end=  (Date) offre.getEnd_date_offre();
		 LocalDate date2 = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 endDate.setValue(date2);
		 
	
		 
		 
		 
		 
	}

}
