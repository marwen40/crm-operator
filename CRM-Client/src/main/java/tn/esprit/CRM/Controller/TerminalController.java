package tn.esprit.CRM.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class TerminalController implements Initializable {

    @FXML
    private TextArea area_id;
    @FXML
    private Button bt_id;
    @FXML
    private Button reset;
    @FXML
    private ComboBox<String> Combo_id;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void action(ActionEvent event) throws IOException {
         String url ="https://www.statista.com/statistics/263264/top-companies-in-the-world-by-market-value/";
      //  String  url1 ="https://www.telegraph.co.uk/business/2016/07/20/revealed-the-biggest-companies-in-the-world-in-2016/";
      // String url="https://www.forbes.com/global2000/list/";
       
       if(Combo_id.getValue().toString().equals("link1")){
           
    Document doc = Jsoup.connect(url).get();

   List<String> D =  new ArrayList<>();
    Elements table = doc.select("td"); //select the first table.
    for (Element i:table) {
        if (!i.getElementsByTag("td").text().equals("")){
           System.out.println("");
            
                String data=i.getElementsByTag("td").text();
                area_id.appendText(data+"\n"+"\n");
                System.out.println(data);
                      
   }
    }
        }
    /*    
       if(Combo_id.getValue().toString().equals("link2")){ 
           
       Document doc = Jsoup.connect(url1).get();

     List<String> D =  new ArrayList<>();
     
    Elements table = doc.select("h4"); //select the first table.
    for (Element j:table) {
        if (!j.getElementsByTag("h4").text().equals("")){
           System.out.println("");
            
                String data1=j.getElementsByTag("h4").text();
                area_id.appendText(data1+"\n"+"\n");
                System.out.println(data1);
                      
        }
        
        
        }
       }*/
           
    }
 
    
    
    
    @FXML
    void reset(ActionEvent event) {
        area_id.clear();
    }
    
}
