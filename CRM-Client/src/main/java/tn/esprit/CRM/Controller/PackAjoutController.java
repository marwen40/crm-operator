package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Pack;

import tn.esprit.CRM.services.AbonnementServiceRemote;
import tn.esprit.CRM.services.OffreServiceRemote;
import tn.esprit.CRM.services.PackServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

public class PackAjoutController implements Initializable {

    @FXML
    private JFXTextField tf_Pack_name;

    @FXML
    private JFXTextField quantite;

    @FXML
    private JFXComboBox<String> Product_LD;

    @FXML
    private DatePicker Dp_deb;

    @FXML
    private JFXTextField gain_tf;

    @FXML
    private DatePicker dP_fin;

    @FXML
    private JFXComboBox<String> abonnement_LD;

    @FXML
    private JFXTextArea Desc_Tf;

    @FXML
    private TableView<Pack> OfferTable;

    @FXML
    private TableColumn<Pack, String> TitleT;

    @FXML
    private TableColumn<Pack, Date> SDateT;

    @FXML
    private TableColumn<Pack, Date> EnDateT;

    @FXML
    private TableColumn<Pack, List> productT;

    @FXML
    private TableColumn<Pack, String> abonnT;

    @FXML
    private TableColumn<Pack, Float> PriceT;

    @FXML
    private TableColumn<Pack, Integer> quantitT;

    @FXML
    private TableColumn<Pack, String> DescripT;
    @FXML
    private TableColumn<Pack, String> VisibT;

    @FXML
    private JFXTextField searchText;

    @FXML
    private JFXButton refreshBtn;

    @FXML
    private JFXButton searchBtn;

    @FXML
    private JFXButton returnBtn;

    @FXML
    private JFXButton deletOfBtn;

    @FXML
    private JFXButton updatebtn;

    @FXML
    private JFXButton Setinvi;

    @FXML
    private JFXButton Setinvi1;

    @FXML
    private JFXButton addAbon;

    @FXML
    private JFXButton addproduct;
    private String jndi = "CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote";
	private String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
	private String jndi2 ="CRM-ear/CRM-ejb/AbonnementService!tn.esprit.CRM.services.AbonnementServiceRemote";
	
	public static Pack pack;



    @FXML
    void AddAbonnAction(ActionEvent event) throws NamingException, IOException {
    	if (OfferTable.getSelectionModel().getSelectedIndex() < 0) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
    		alert.setTitle("Error Dialog");
    		alert.setHeaderText("Pack not selected");
    		alert.setContentText("Please select a pack");
    		alert.showAndWait();
    	} else {
    		this.pack = OfferTable.getSelectionModel().getSelectedItem();
    		Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/AddAbonCyrine.fxml")));
    		Stage stage = new Stage();
    		stage.setScene(scene);
    		stage.show();
    	}
    	this.refresh(event);


    }

    @FXML
    void AddProdAction(ActionEvent event) throws IOException, NamingException {	if (OfferTable.getSelectionModel().getSelectedIndex() < 0) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Pack not selected");
		alert.setContentText("Please select a pack");
		alert.showAndWait();
	} else {
		this.pack = OfferTable.getSelectionModel().getSelectedItem();
		Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/AddProdCyrine.fxml")));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}
	this.refresh(event);

    }

    @FXML
    void AddProduction(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
		PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
		ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
		 AbonnementServiceRemote  abonnementServiceRemote = (AbonnementServiceRemote) context.lookup(jndi2);
		 Pack pack = new Pack(java.sql.Date.valueOf(Dp_deb.getValue()), java.sql.Date.valueOf(dP_fin.getValue()), Float.parseFloat(gain_tf.getText()), tf_Pack_name.getText(), Integer.parseInt(quantite.getText()),Desc_Tf.getText()  );
			int startDate =java.sql.Date.valueOf(Dp_deb.getValue()).getDate();
			int newDate=new java.util.Date().getDate();
			if ((startDate<newDate)){
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText(java.sql.Date.valueOf(Dp_deb.getValue())+" is not valid");
				alert.showAndWait();
			} else {
				Integer g = packServiceRemote.createPack(pack);
				String lproduit = Product_LD.getValue();
				String labonn =abonnement_LD.getValue();
				Integer pp =productServiceRemote.findIdProductByTitleCyrine(lproduit);
				Integer abb = packServiceRemote.findIdAbonnemmentByTitle(labonn);
				packServiceRemote.Addabbon(g, abb);
				packServiceRemote.Addprod(g, pp);
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
				alert.setTitle("Success!!");
				alert.setContentText("The pack has been successfully added");
				alert.showAndWait();
				}
			this.refresh(event);
			tf_Pack_name.setText(null);
			gain_tf.setText(null);
			Dp_deb.setValue(null);
			dP_fin.setValue(null);
			Product_LD.setValue(null);
			abonnement_LD.setValue(null);
			Desc_Tf.setText(null);
			gain_tf.setText(null);
			} 
   

    @FXML
    void DeleteAction(ActionEvent event) throws NamingException {
    	Context context;
		context = new InitialContext();
		String jndi = "CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote";
		PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);	
		Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
		Pack pack =(Pack) OfferTable.getSelectionModel().getSelectedItem();
		if (OfferTable.getSelectionModel().isSelected(selected)) {
			packServiceRemote.deleteOffre(OfferTable.getItems().get(selected));
		} else {
			System.out.println("selectionner un élement non traité" + selected);
			
			this.refresh(event);
		}

    }

    @FXML
    void InvisibAction(ActionEvent event) throws NamingException {
    	Context context;
		context = new InitialContext();
		PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
		Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
		Pack pack = (Pack) OfferTable.getSelectionModel().getSelectedItem();
		if (OfferTable.getSelectionModel().isSelected(selected)&&pack.getVisibily_pack()==true) {
			 packServiceRemote.invisible(OfferTable.getItems().get(selected));
			 //(OfferTable.getItems().get(selected));
			
		}
		else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Ppack is invisible");
			alert.setContentText("Please select a visible Pack");
			alert.showAndWait();
		}
		List<Pack> packs = packServiceRemote.findalladminPack();
		TitleT.setCellValueFactory(new PropertyValueFactory<Pack, String>("title_pack"));
		SDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("start_date_pack"));
		EnDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("end_date_pack"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, List>("products"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, String>("abonnement"));
		PriceT.setCellValueFactory(new PropertyValueFactory<Pack, Float>("price_pack"));
		VisibT.setCellValueFactory(new PropertyValueFactory<Pack,String>("visibily_pack"));
		quantitT.setCellValueFactory(new PropertyValueFactory<Pack,Integer>("numbre"));
		DescripT.setCellValueFactory(new PropertyValueFactory<Pack,String>("description_pack"));
		ObservableList<Pack> data = FXCollections.observableArrayList(packServiceRemote.findalladminPack());
		OfferTable.setItems(data);
		String email= "islam.benfredj@esprit.tn";
		String sujet ="Visibilité";
		 String Contenu ="Le pack" +pack.getTitle_pack() + "est invisible";
		 packServiceRemote.sendMail(email, sujet, Contenu);
		this.refresh(event);
		 
		 
    }

    @FXML
    void Return(ActionEvent event) {
    	tf_Pack_name.setText(null);
		gain_tf.setText(null);
		Dp_deb.setValue(null);
		dP_fin.setValue(null);
		Product_LD.setValue(null);
		abonnement_LD.setValue(null);
		Desc_Tf.setText(null);
		gain_tf.setText(null);

    }

    @FXML
    void UpdatepackAction(ActionEvent event) throws IOException, NamingException {if (OfferTable.getSelectionModel().getSelectedIndex() < 0) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Offer not selected");
		alert.setContentText("Please select an Offer");
		alert.showAndWait();
	} else {
		this.pack = OfferTable.getSelectionModel().getSelectedItem();
		Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/UpdatePack.fxml")));
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}
	this.refresh(event);

    }

    @FXML
    void VisibAction(ActionEvent event) throws NamingException {
    	Context context;
    	context = new InitialContext();
    	PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
    	Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
		Pack pack = (Pack) OfferTable.getSelectionModel().getSelectedItem();
		if (OfferTable.getSelectionModel().isSelected(selected)&&pack.getVisibily_pack()==false) {
			 packServiceRemote.visible(OfferTable.getItems().get(selected));
			 //(OfferTable.getItems().get(selected));
			
		}
		else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Ppack is visible");
			alert.setContentText("Please select an invisible Pack");
			alert.showAndWait();
		}
		List<Pack> packs = packServiceRemote.findalladminPack();
		TitleT.setCellValueFactory(new PropertyValueFactory<Pack, String>("title_pack"));
		SDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("start_date_pack"));
		EnDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("end_date_pack"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, List>("products"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, String>("abonnement"));
		PriceT.setCellValueFactory(new PropertyValueFactory<Pack, Float>("price_pack"));
		VisibT.setCellValueFactory(new PropertyValueFactory<Pack,String>("visibily_pack"));
		quantitT.setCellValueFactory(new PropertyValueFactory<Pack,Integer>("numbre"));
		DescripT.setCellValueFactory(new PropertyValueFactory<Pack,String>("description_pack"));
		ObservableList<Pack> data = FXCollections.observableArrayList(packServiceRemote.findalladminPack());
		OfferTable.setItems(data);
		this.refresh(event);

    }

    @FXML
    void refresh(ActionEvent event) throws NamingException {
    	try {
    	InitialContext ctx;
    	PackServiceRemote proxy;
    	ctx = new InitialContext();
    	proxy = (PackServiceRemote) ctx
				.lookup("CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote");
    	
    	
    	TitleT.setCellValueFactory(new PropertyValueFactory<Pack, String>("title_pack"));
		SDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("start_date_pack"));
		EnDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("end_date_pack"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, List>("products"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, String>("abonnement"));
		PriceT.setCellValueFactory(new PropertyValueFactory<Pack, Float>("price_pack"));
		VisibT.setCellValueFactory(new PropertyValueFactory<Pack,String>("visibily_pack"));
		quantitT.setCellValueFactory(new PropertyValueFactory<Pack,Integer>("numbre"));
		DescripT.setCellValueFactory(new PropertyValueFactory<Pack,String>("description_pack"));
		ObservableList<Pack> data = FXCollections.observableArrayList(proxy.findalladminPack());
		for (Pack x : data) {
			

			System.out.println();
			

			if ((x.getEnd_date_pack().getDate() <new java.util.Date().getDate())) {
//				Alert alert = new Alert(Alert.AlertType.ERROR);
//				alert.setTitle("Error Dialog");
//				alert.se	tHeaderText("Offer ha been deleted ");
//
//				alert.showAndWait();
				proxy.deleteOffre(x);
				System.out.println("egale");	
			}
		}
		OfferTable.setItems(data); }
    	catch (NamingException e1) {

		}
		
		
	} 


    @FXML
    void search(KeyEvent event) {

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		List<String> categories = new ArrayList<>();
		List<String> categoriesab = new ArrayList<>();
		
		try {
			Context context = new InitialContext();
			List<String> categoryOldd = new ArrayList<>();
			List<String> categoryOlddAbb = new ArrayList<>();
			PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
			 AbonnementServiceRemote  abonnementServiceRemote = (AbonnementServiceRemote) context.lookup(jndi2);

			 categoryOldd = productServiceRemote.DisplayALlProductsNamesCyrine();
			 categoryOlddAbb=packServiceRemote.DisplayALlAbonnementNames();
			 for (String category : categoryOldd) {
					categories.add(category);
					ObservableList<String> items = FXCollections.observableArrayList(categories);
					
					Product_LD.setItems(items);
					
					
					
				}
			 for (String categorys : categoryOlddAbb) {
				 categoriesab.add(categorys);
				 ObservableList<String> itemsAb = FXCollections.observableArrayList(categoriesab);
				 abonnement_LD.setItems(itemsAb);
			 }
			} catch (NamingException e) {
				e.printStackTrace();
			}
		try {
			InitialContext ctx;
			PackServiceRemote proxy;
			ctx = new InitialContext();
			proxy = (PackServiceRemote) ctx
					.lookup("CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote");
	
    	
    	
    	TitleT.setCellValueFactory(new PropertyValueFactory<Pack, String>("title_pack"));
		SDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("start_date_pack"));
		EnDateT.setCellValueFactory(new PropertyValueFactory<Pack, Date>("end_date_pack"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, List>("products"));
		//productT.setCellValueFactory(new PropertyValueFactory<Pack, String>("abonnement"));
		PriceT.setCellValueFactory(new PropertyValueFactory<Pack, Float>("price_pack"));
		VisibT.setCellValueFactory(new PropertyValueFactory<Pack,String>("visibily_pack"));
		quantitT.setCellValueFactory(new PropertyValueFactory<Pack,Integer>("numbre"));
		DescripT.setCellValueFactory(new PropertyValueFactory<Pack,String>("description_pack"));
		ObservableList<Pack> data = FXCollections.observableArrayList(proxy.findalladminPack());
		OfferTable.setItems(data);

		} catch (NamingException e1) {

		}
		

			
		
	}

}
