package tn.esprit.CRM.Controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.services.AbonnementServiceRemote;
import tn.esprit.CRM.services.PackServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

public class UpdatePackController implements Initializable {

    @FXML
    private Label PackTitle;

    @FXML
    private JFXComboBox<String> Product_LD;

    @FXML
    private JFXDatePicker startDate;

    @FXML
    private JFXDatePicker endDate;

    @FXML
    private JFXButton updateBtn;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private Label fileDirectory;

    @FXML
    private JFXTextField gain_tf;

    @FXML
    private JFXComboBox<String> Abonnement_LD;

    @FXML
    private JFXTextArea Desc_TF;

    @FXML
    private JFXTextField QuantityTF;
    public static Pack pack;
    private String jndi = "CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote";
    private String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
    private String jndi2 ="CRM-ear/CRM-ejb/AbonnementService!tn.esprit.CRM.services.AbonnementServiceRemote";

    @FXML
    void CancelUpdate(ActionEvent event)  {
    	Stage stage = (Stage) cancelBtn.getScene().getWindow();
		stage.close();

    }

    @FXML
    void updateTask(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
    	ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
    	PackAjoutController pac = new PackAjoutController ();
    	Pack pack= pac.pack;
    
    	pack.setTitle_pack(PackTitle.getText());
    	pack.setPrice_pack(Float.parseFloat(gain_tf.getText()));
    	String lproduit = Product_LD.getValue();
    	String labonn = Abonnement_LD.getValue();
    	pack.setDescription_pack(Desc_TF.getText());
    	pack.setNumbre(Integer.parseInt(QuantityTF.getText()));
    	pack.setEnd_date_pack(java.sql.Date.valueOf(startDate.getValue()));
    	pack.setStart_date_pack(java.sql.Date.valueOf(endDate.getValue()));
    	 Integer g = packServiceRemote.update(pack);
    	//Integer pp =productServiceRemote.findIdProductByTitle(lproduit);
		//Integer abb = packServiceRemote.findIdAbonnemmentByTitle(labonn);
		//packServiceRemote.Addabbon(g, abb);
		//packServiceRemote.Addprod(g, pp);
		Stage stage = (Stage) updateBtn.getScene().getWindow();
	
		stage.close();

    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		List<String> categories = new ArrayList<>();
		List<String> categoriesab = new ArrayList<>();
		
		try {
			Context context = new InitialContext();
			List<String> categoryOldd = new ArrayList<>();
			List<String> categoryOlddAbb = new ArrayList<>();
			PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
			 AbonnementServiceRemote  abonnementServiceRemote = (AbonnementServiceRemote) context.lookup(jndi2);

			 categoryOldd = productServiceRemote.DisplayALlProductsNamesCyrine();
			 categoryOlddAbb=packServiceRemote.DisplayALlAbonnementNames();
			 for (String category : categoryOldd) {
					categories.add(category);
					ObservableList<String> items = FXCollections.observableArrayList(categories);
					
					Product_LD.setItems(items);
					
					
					
				}
			 for (String categorys : categoryOlddAbb) {
				 categoriesab.add(categorys);
				 ObservableList<String> itemsAb = FXCollections.observableArrayList(categoriesab);
				 Abonnement_LD.setItems(itemsAb);
			 }
			} catch (NamingException e) {
				e.printStackTrace();
			}
		try {
			InitialContext ctx;
			PackServiceRemote proxy;
			ctx = new InitialContext();
			proxy = (PackServiceRemote) ctx
					.lookup("CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote");
	
		
	}
		catch (NamingException e) 
  		{
  			e.printStackTrace();
  		}
		updateBtn.setCursor(Cursor.HAND);
		cancelBtn.setCursor(Cursor.HAND);
		PackAjoutController pac = new PackAjoutController();
		Pack pack = pac.pack;
		PackTitle.setText(pack.getTitle_pack());
		 //gain_tf.setText(pack.getPrice_pack().toString());
		 
		 Date start = (Date) pack.getStart_date_pack();
		 LocalDate date1 = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 startDate.setValue(date1);
		 Date end=  (Date) pack.getEnd_date_pack();
		 LocalDate date2 = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 endDate.setValue(date2);
		 Desc_TF.setText(pack.getDescription_pack());
		 QuantityTF.setText(pack.getNumbre().toString());
		 
		 
		 
		 
} }
