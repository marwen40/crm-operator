package tn.esprit.CRM.Controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Map.Entry;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.itextpdf.text.ImgCCITT;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Label;


import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.Utilislam;
import tn.esprit.CRM.persistence.Category;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.StoreProduit;
import tn.esprit.CRM.services.CategoryServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;


public class ProductManagementController implements Initializable {
	@FXML
	private JFXTextField NameTF;

	@FXML
	private JFXTextArea DescriptionTA;

	@FXML
	private JFXTextField QuantityTF;

	@FXML
	private JFXTextField PriceTF;

	@FXML
	private JFXComboBox<String> CategoryComboBox;

	@FXML
	private JFXComboBox<String> brandComboBox;

	@FXML
	private JFXTextField newQuantity;

	@FXML
	private JFXButton UpdateButton;

	@FXML
	private TableView<Product> ProductionTable;

	@FXML
	private TableColumn<Product, String> descriptionT;

	@FXML
	private TableColumn<?, ?> dateT;

	@FXML
	private TableColumn<Product, Integer> employeeT;

	@FXML
	private TableColumn<Product, Float> productT;

	@FXML
	private TableColumn<Product, String> quantityT;

	@FXML
	private TableColumn<Product, String> BrandT;

	@FXML
	private JFXTextField searchText;
	   @FXML
	    private JFXButton cancelBtn;

	@FXML
	private Label sortLabel;

	@FXML
	private JFXComboBox<?> sortCombobox;

	@FXML
	private JFXButton refreshBtn;

	@FXML
	private JFXButton searchBtn;

	@FXML
	private JFXButton returnBtn;

	@FXML
	private JFXTextField recuperateTF;

	@FXML
	private JFXButton removeButton;

	@FXML
	private JFXButton updateButton;
	
	
	//Error Labels
	
    @FXML
    private Label NameError;

    @FXML
    private Label categoryError;

    @FXML
    private Label BrandError;

    @FXML
    private Label QuantityError;

    @FXML
    private Label PriceError;
    
    @FXML
    private Label DescriptionError;

    @FXML
    private Label nameImg;

    

	private String jndip = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
	private String jndic = "CRM-ear/CRM-ejb/CategoryService!tn.esprit.CRM.services.CategoryServiceRemote";


	// ************************ ADD NEW PRODUCT*********************************//

	public boolean QuantityIsNumber(String x){
        boolean verif=true;
        try{
            Integer.parseInt(x);
        }
        catch(NumberFormatException e){
            verif=false;
        }
        return verif;
    }
	

	public boolean PriceIsNumber(String x){
        boolean verif=true;
        try{
            Float.parseFloat(x);
        }
        catch(NumberFormatException e){
            verif=false;
        }
        return verif;
    }
	
	
	   @FXML
	    void Cancel(ActionEvent event) {
		   NameTF.setText(null);
		   QuantityTF.setText(null);
			PriceTF.setText(null);
			DescriptionTA.setText(null);
			brandComboBox.setValue(null);
			CategoryComboBox.setValue(null);
			
			NameError.setText(null);
			QuantityError.setText(null);
			PriceError.setText(null);
			DescriptionError.setText(null);		   
	    }
	
	//********************contrôle de saisie sur le champ Product Name*********************//
    
	@FXML
    void controlProductName(KeyEvent event) {
		
		
		if(NameTF.getText().equals("")==true)
		{
		NameError.setText("Product Name undefined");
		NameError.setTextFill(Color.RED);
		NameError.setVisible(true);
		}
		else {
			NameError.setText("Correct");
			NameError.setTextFill(Color.GREEN);
			NameError.setVisible(true);
		}

    }
    
	//********************contrôle de saisie sur le champ Quantity*********************//

    @FXML
    void ControlQuantity(KeyEvent event) 
    {
    	boolean res = QuantityIsNumber(QuantityTF.getText());
    	
		if(QuantityTF.getText().equals("")==true)
		{
			QuantityError.setText("Undefined Quantity");
			QuantityError.setTextFill(Color.RED);
			QuantityError.setVisible(true);
		}
	 if(res==false)
		{
			QuantityError.setText("It's a quantity please type a number!!!");
			QuantityError.setTextFill(Color.RED);
			QuantityError.setVisible(true);	
		}
		else
		{
			QuantityError.setText("Correct");
			QuantityError.setTextFill(Color.GREEN);
			QuantityError.setVisible(true);
		}
    }
    
    
	//********************contrôle de saisie sur le champ Price *********************//
    @FXML
    void ControlPrice(KeyEvent event) {
    	boolean p = PriceIsNumber(PriceTF.getText());
    	if(PriceTF.getText().equals("")==true )
    	{
			PriceError.setText("Undefined Price");
			PriceError.setTextFill(Color.RED);
			PriceError.setVisible(true);
    	}
    	else if(p==false)
    	{
    		PriceError.setText("Please type a number");
    		PriceError.setTextFill(Color.RED);
    		PriceError.setVisible(true);
    	}
    	else
    	{
    		PriceError.setText("Correct");
    		PriceError.setTextFill(Color.GREEN);
    		PriceError.setVisible(true);
    	}
    	

    }
 

 
    
	//********************contrôle de saisie sur le champ Description*********************//
 
    @FXML
    void controlDescription(KeyEvent event) throws Exception {
		
		if(DescriptionTA.getText().equals("")==true)
		{
		DescriptionError.setText("Add a description ");
		DescriptionError.setTextFill(Color.RED);
		DescriptionError.setVisible(true);
		}
		else {
			DescriptionError.setText("Correct");
			DescriptionError.setTextFill(Color.GREEN);
			DescriptionError.setVisible(true);
		}
    }
    
	@FXML
	void AddProduct(ActionEvent event) {
		Date sysdate = new Date();
		if((NameError.getText().equals("Correct"))&&(DescriptionError.getText().equals("Correct"))&&(QuantityError.getText().equals("Correct"))&&(PriceError.getText().equals("Correct"))&&	(CategoryComboBox.getValue()!=null)&&(brandComboBox.getValue()!=null))
		{
			Product produit = new Product(NameTF.getText(), Integer.parseInt(QuantityTF.getText()),
					Float.parseFloat(PriceTF.getText()), DescriptionTA.getText(), "", brandComboBox.getValue(),
					CategoryComboBox.getValue(), sysdate);

		  
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Your are going to Add a new product");
			alert.setContentText("Are you sure?");
			alert.showAndWait();
			if(alert.getResult().getText().equalsIgnoreCase("OK"))
			{  produit.setBrand_name(brandComboBox.getValue());
			produit.setCategory_name(CategoryComboBox.getValue());
				produit.setImageName(nameImg.getText());
				Integer g = Utilislam.getProductService().createProduct(produit);
				System.out.println(g);
				Integer p = Utilislam.getCategoryService().returnCategoryID(CategoryComboBox.getValue());
				Utilislam.getCategoryService().affecterProduitACategorie(g, p);
				
				Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
				alert2.setTitle("Information dialog");
				alert2.setHeaderText(null);
				alert2.setContentText("Product added");
				alert2.showAndWait();
				this.display();
				System.out.println(sysdate);
				this.display();
				this.Cancel(event);
			}
			else {
				this.Cancel(event);
			}
			
		
		}
		else 
			{
				
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setContentText("Check Your Information Product Please");
				alert.showAndWait();
			}
			
			
			
		}
	
		
	
		
	

	// ******************************* DELETE PRODUCT ****************************************//
	@FXML
	void Delete(ActionEvent event) throws NamingException {
		if (ProductionTable.getSelectionModel().getSelectedIndex() < 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Product not selected");
			alert.setContentText("Please select a product");
			alert.showAndWait();
		}

		else {
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Your are going to delete the selected product");
			alert.setContentText("Are you sure?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
			
				Utilislam.getProductService().deleteProduct(product);

				Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Information Dialog");
				alert.setHeaderText(null);
				alert.setContentText("Product deleted");
				alert.showAndWait();
				this.display();
			} else {
				this.display();
			}
		}

	}

	@FXML
	void Return(ActionEvent event) {

	}

	// *************************** UPDATE PRODUCT****************************************************//

	public static Product product;

	@FXML
	void UpdateProduct(ActionEvent event) throws IOException {

		if (ProductionTable.getSelectionModel().getSelectedIndex() < 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Task not selected");
			alert.setContentText("Please select a product");
			alert.showAndWait();
		} 
		else {

			this.product = ProductionTable.getSelectionModel().getSelectedItem();
			Scene scene = new Scene(
					FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/UpdateProductFXML.fxml")));
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
		}

	}

	private final static String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
	private final static String jndi2 = "CRM-ear/CRM-ejb/CategoryService!tn.esprit.CRM.services.CategoryServiceRemote";
	private final static String jndi3 = "CRM-ear/CRM-ejb/StoreService!tn.esprit.CRM.services.StoreServiceRemote";
	
	
	// ********************************** DISPLAY ALL PRODUCTS*******************************************//

	@FXML
	public void refresh(ActionEvent event) {
		this.display();

	}

	@FXML
	void search(KeyEvent event) {

	}
	
	@FXML
	void onStore(ActionEvent event) throws Exception {
		this.loadStatStore();
	}
	
	@FXML
	void onProduct(ActionEvent event) throws Exception {
		loadStatProduit();
	}

	
	//affichage des produits
	public void display() {
		Context context;
		try {
			context = new InitialContext();
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndip);
			descriptionT.setCellValueFactory(new PropertyValueFactory<Product, String>("name_product"));
			dateT.setCellValueFactory(new PropertyValueFactory<>("categorie"));
			BrandT.setCellValueFactory(new PropertyValueFactory<Product, String>("brand_name"));
			employeeT.setCellValueFactory(new PropertyValueFactory<Product, Integer>("quantity_product"));
			productT.setCellValueFactory(new PropertyValueFactory<Product, Float>("price_product"));
			quantityT.setCellValueFactory(new PropertyValueFactory<Product, String>("description_product"));
			ObservableList<Product> data = FXCollections.observableArrayList(productServiceRemote.findAll());
			ProductionTable.setItems(data);
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void update(ActionEvent event) throws IOException {
		if (ProductionTable.getSelectionModel().getSelectedIndex() < 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Product not selected");
			alert.setContentText("Please select a product");
			alert.showAndWait();
		} else {
			this.product = ProductionTable.getSelectionModel().getSelectedItem();
			Scene scene = new Scene(
					FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/UpdateProductFXML.fxml")));
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();

		}

	}
	

	/**
	 * Initializes the controller class.
	 **/

	public void searchProduct(){
		List<Product> products  = Utilislam.getProductService().getProductByName(searchText.getText());
		ObservableList<Product> data = FXCollections.observableArrayList(products);
		ProductionTable.setItems(data);			
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {


	this.display();
	
	//************************Initialisation de la liste des categories*****************************//

	ObservableList<String> datacategory= FXCollections.observableArrayList(Utilislam.getCategoryService().DisplayALlCategoriesNames());
	CategoryComboBox.setItems(datacategory);
	
	/*
	List<String> categories = new ArrayList<>();
	try {
		Context context = new InitialContext();
		CategoryServiceRemote categoryServiceRemote = (CategoryServiceRemote) context.lookup(jndi2);
		List<String> categoryOld = categoryServiceRemote.DisplayALlCategoriesNames();
		for (String category : categoryOld)
		{
			categories.add(category);
			ObservableList<String> items = FXCollections.observableArrayList(categories);
			CategoryComboBox.setItems(items);
		}
	} catch (NamingException e) {
		e.printStackTrace();
	}

	*/
		searchBtn.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				searchText.setText(null);
				List<Product> products  = Utilislam.getProductService().findAll();
				ObservableList<Product> data = FXCollections.observableArrayList(products);
				ProductionTable.setItems(data);					
			}
		});
		
		searchText.setOnKeyReleased(new EventHandler<KeyEvent>()
	    {
	        @Override
	        public void handle(KeyEvent ke)
	        {
	           
					searchProduct();

	        }
	    });

		

		// ************************Initialisation de la liste des brands*****************************//

		List<String> brands = new ArrayList<>();
		brands.add("Samsung");
		brands.add("Apple");
		brands.add("Asus");
		brands.add("Acer");
		brands.add("Huwai");
		brands.add("Nokia");
		brands.add("Lenovo");
		brands.add("MSI");
		brands.add("HP");
		brands.add("Dell");
		ObservableList<String> Items = FXCollections.observableArrayList(brands);
		brandComboBox.setItems(Items);
		
		

		
		// affichage
		List<Product> descriptions = new ArrayList<>();
		try {
			Context context = new InitialContext();
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndip);
			List<Product> products = productServiceRemote.findAll();
			for (Product product : products) {
				descriptions.add(product);
			}
		} catch (Exception e) {
		}
	}
	
	

	 public void loadStatStore() throws Exception {
		 	Stage primaryStage = new Stage();
	        primaryStage.setTitle("BarChart Experiments");

	        CategoryAxis xAxis    = new CategoryAxis();
	        xAxis.setLabel("Store");

	        NumberAxis yAxis = new NumberAxis();
	        yAxis.setLabel("Quantité");

	        BarChart     barChart = new BarChart(xAxis, yAxis);

	        XYChart.Series dataSeries1 = new XYChart.Series();
	        dataSeries1.setName("Quantité");
	        
	        List<StoreProduit> list =  Utilislam.getStoreService().getAllStoreProduct();
	        HashMap<String, Integer> listHM = Utilislam.getQtyByStore(list);
	        
	        for(Entry e : listHM.entrySet()){
				System.out.println("nom store : " + e.getKey() + " quantité totale "+e.getValue());
		        dataSeries1.getData().add(new XYChart.Data(e.getKey(), e.getValue()));

			}
	        
	      

	        barChart.getData().add(dataSeries1);

	        VBox vbox = new VBox(barChart);

	        Scene scene = new Scene(vbox, 400, 200);

	        primaryStage.setScene(scene);
	        primaryStage.setHeight(500);
	        primaryStage.setWidth(300);

	        primaryStage.show();
	    }
	 

	 public void loadStatProduit() throws Exception {
		 	Stage primaryStage = new Stage();
	        primaryStage.setTitle("BarChart Experiments");

	        CategoryAxis xAxis    = new CategoryAxis();
	        xAxis.setLabel("Produit");

	        NumberAxis yAxis = new NumberAxis();
	        yAxis.setLabel("Quantité");

	        BarChart     barChart = new BarChart(xAxis, yAxis);

	        XYChart.Series dataSeries1 = new XYChart.Series();
	        dataSeries1.setName("Quantité");
	        
	        List<StoreProduit> list =  Utilislam.getStoreService().getAllStoreProduct();
	        
	        HashMap<Product, Integer> listHM = Utilislam.getQtyByProduct(list);
	        
	        for(Entry e : listHM.entrySet()){
				System.out.println("nom produit : " + e.getKey() + " quantité totale "+e.getValue());
				Product cle = (Product) e.getKey();
		        dataSeries1.getData().add(new XYChart.Data(cle.getName_product(), e.getValue()));

			}
	        
	      

	        barChart.getData().add(dataSeries1);

	        VBox vbox = new VBox(barChart);

	        Scene scene = new Scene(vbox, 400, 200);

	        primaryStage.setScene(scene);
	        primaryStage.setHeight(500);
	        primaryStage.setWidth(300);

	        primaryStage.show();
	    }
	 
    @FXML
    void onUpload(ActionEvent event) throws IOException {
    	FileChooser fileChooser = new FileChooser();
    	FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.jpg)", "*.jpg");
    	fileChooser.getExtensionFilters().add(extFilter);
    	File file = fileChooser.showOpenDialog(nameImg.getScene().getWindow());
    	
    	byte[] fileContent = Files.readAllBytes(file.toPath());	
		System.out.println(file);
		nameImg.setText(file.getName());
		FileOutputStream outputStream = new FileOutputStream("C:\\wamp64\\www\\factures\\uploads\\"+file.getName());
		outputStream.write(fileContent);
		outputStream.close();	
    }

	


}
