package tn.esprit.CRM.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.services.CategoryServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;


public class UpdateProductController implements Initializable
{
	  @FXML
	    private JFXTextField productNameTF;

	    @FXML
	    private JFXTextField quantityTF;

	    @FXML
	    private JFXTextField priceTF;

	    @FXML
	    private JFXButton update_button;

	    @FXML
	    private JFXButton cancel_button;

	    @FXML
	    private JFXComboBox<String> categoryCombo;

	    @FXML
	    private JFXTextArea decriptionTF;

	    @FXML
	    private JFXComboBox<String> brandConbo;
	    @FXML
	    private ImageView imgView;
	    @FXML
	    void CancelAction(ActionEvent event) {
	    	Stage stage = (Stage) cancel_button.getScene().getWindow();
			stage.close();
	    }
//*********************************************UPDATE PRODUCT*********************************************************//
	    @FXML
	    void UpdateAction(ActionEvent event) throws NamingException {
	    
	    	Product product = ProductManagementController.product;
	    	Context context = new InitialContext();
	        String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
	    	ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
	    	product.setName_product(productNameTF.getText());
	    	product.setCategory_name(categoryCombo.getValue().toString());
	    	product.setBrand_name(brandConbo.getValue().toString());
	    	product.setQuantity_product(Integer.parseInt(quantityTF.getText()));
	    	product.setPrice_product(Float.parseFloat(priceTF.getText()));
	    	product.setDescription_product(decriptionTF.getText());
	    	
	    	productServiceRemote.updateProduct(product);
	    	
	    	Stage stage = (Stage) update_button.getScene().getWindow();
    		stage.close();
    	
	    }

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
		
			//************************Initialisation de la liste des categories*****************************//
			
			String jndi2 = "CRM-ear/CRM-ejb/CategoryService!tn.esprit.CRM.services.CategoryServiceRemote";
			 List<String> categories = new ArrayList<>();
		  		try 
		  		{
		  			Context context = new InitialContext();
		  			CategoryServiceRemote categoryServiceRemote  = (CategoryServiceRemote) context.lookup(jndi2);
		  			List<String> categoryOld =categoryServiceRemote.DisplayALlCategoriesNames();
		  			for(String category:categoryOld)
		  			{
		  				categories.add(category);
		  				ObservableList<String> items =FXCollections.observableArrayList(categories);
		  				categoryCombo.setItems(items);		
		  			}			
		  		} 
		  		catch (NamingException e) 
		  		{
		  			e.printStackTrace();
		  		}
		  		
		  		Image image = new Image("http://localhost/factures/uploads/"+ProductManagementController.product.getImageName());
		  		imgView.setImage(image);
			
  		
		  		///////////////////////////////Initialisation de la liste des brands////////////////////////////////
		/*  		
				List<String> brands = new ArrayList<>();
		  		try 
		  		{
		  			Context context = new InitialContext();
		  			 String jndip = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
			    	ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndip);	    	

		  			
		  			List <Brand> brandOld = productServiceRemote.findAllBrands();
		  			List<String> lista = new ArrayList<>();
		  			for(Brand brand:brandOld)
		  			{
		  				
		  				lista.add(brand.getBrand_type());
		  				ObservableList<String> Items =FXCollections.observableArrayList(lista);
		  				brandConbo.setItems(Items);		
		  			}			
		  		} 
		  		catch (NamingException e) 
		  		{
		  			e.printStackTrace();
		  		}
		  		*/
			////////////////////////////////////// récupération des données du produit à modifier ////////////////////////////////////////
		  		
			update_button.setCursor(Cursor.HAND);
			cancel_button.setCursor(Cursor.HAND);
			
			ProductManagementController pmc = new ProductManagementController();
			Product product = pmc.product;
			productNameTF.setText(product.getName_product());
			categoryCombo.setValue(product.getCategory_name());
			brandConbo.setValue(product.getBrand_name());
			quantityTF.setText(product.getQuantity_product().toString());
			priceTF.setText(product.getPrice_product().toString());
			decriptionTF.setText(product.getDescription_product());
			
			
		}

}
