package tn.esprit.CRM.Controller;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.itextpdf.text.DocumentException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;

import main.PdfUtils;
import main.Utilislam;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Store;
import tn.esprit.CRM.persistence.StoreProduit;



import javafx.fxml.Initializable;

public class StoreManagementController implements Initializable
{
    @FXML
    private JFXButton CancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private JFXTextField addresTF;

    @FXML
    private JFXTextField nbEmployeesTF;

    @FXML
    private TabPane tabPane;


    @FXML
    private JFXTextField GovernorateTF;

    @FXML
    private JFXTextField storeNameTF;

    @FXML
    private TableView<Store> projectTable;

    @FXML
    private TableColumn<Store, String> titleT;

    @FXML
    private TableColumn<Store, String> clientT;

    @FXML
    private TableColumn<Store, String> supervisorT;

    @FXML
    private TableColumn<Store, Integer> sectorT;

    @FXML
    private TableColumn<?, ?> startDateT;

    @FXML
    private TableColumn<?, ?> endDateT;

    @FXML
    private JFXButton deleteBtn;

    @FXML
    private JFXButton timesheetBtn;

    @FXML
    private JFXTextField searchTitle;

    @FXML
    private JFXButton updateBtn;

    @FXML
    private JFXComboBox<?> sortCB;
    
    @FXML
    private JFXComboBox<Store> StoreComboBox;
    
    @FXML
    private JFXComboBox<Product> productComboBox;
    
    @FXML
    private JFXSlider quantitySlider;

    @FXML
    private Tab tabStores;
    @FXML
    private Tab tabAssign;

    @FXML
    private TableView<StoreProduit> storeTable;

    @FXML
    private TableColumn<StoreProduit, Product> prodName;

    @FXML
    private TableColumn<StoreProduit, Integer> quantityStore;

    @FXML
    private JFXComboBox<Store> selectStore;
    


    
    int sliderInt=0;
    Store selectedSotre;
    Product selectedProd;
   
   // private String jndic = "CRM-ear/CRM-ejb/CategoryService!tn.esprit.CRM.services.CategoryServiceRemote";
   // private String jndib = "CRM-ear/CRM-ejb/BrandService!tn.esprit.CRM.services.BrandServiceRemote";

   //************************************ADD NEW STORE********************************************/

	
   @FXML
    void AddStore(ActionEvent event) 
    {
	    	Store store = new Store(addresTF.getText(), Integer.parseInt(nbEmployeesTF.getText()), storeNameTF.getText(), GovernorateTF.getText());    	
	    	Utilislam.getStoreService().createStore(store);
	    	this.display();
    }
  
	public void display() {
		
		List<Store> products = Utilislam.getStoreService().findAllStores();
		titleT.setCellValueFactory(new PropertyValueFactory<Store, String>("nom_Store"));
		clientT.setCellValueFactory(new PropertyValueFactory<Store, String>("address_store"));
		supervisorT.setCellValueFactory(new PropertyValueFactory<Store, String>("governorate"));
		sectorT.setCellValueFactory(new PropertyValueFactory<Store, Integer>("employee_number"));
		ObservableList<Store> data = FXCollections.observableArrayList(Utilislam.getStoreService().findAllStores());
		projectTable.setItems(data);
		
	}
	
		public void displayStoreProduit() {

		prodName.setCellValueFactory(new PropertyValueFactory<StoreProduit, Product>("product"));
		quantityStore.setCellValueFactory(new PropertyValueFactory<StoreProduit, Integer>("quantity"));
		
		quantityStore.setCellFactory(column -> {
		    return new TableCell<StoreProduit, Integer>() {
		        @Override
		        protected void updateItem(Integer item, boolean empty) {
		            super.updateItem(item, empty);

		            if (item == null || empty) {
		                setText(null);
		                setStyle("");
		            } else {
	                	setText(String.valueOf(item));

		                if (item<5) {
		                    setTextFill(Color.WHITE);
		                    setStyle("-fx-background-color: red");
		                }else{
		                    setTextFill(Color.WHITE);
		                    setStyle("-fx-background-color: green");

		                }
		            }
		        }
		    };
		});
		ObservableList<StoreProduit> data;
		if(selectedSotre ==null){
			data = FXCollections.observableArrayList(Utilislam.getStoreService().getAllStoreProduct());

		}else{
			data = FXCollections.observableArrayList(Utilislam.getStoreService().getStoreProductByStore(selectedSotre));

		}
		storeTable.setItems(data);
	
	}

    @FXML
    void CancelProject(ActionEvent event)
    {
    	
    }

    @FXML
    void SortBy(ActionEvent event) {

    }

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		// ******************************Initialisation de la liste des categories*****************************//

		this.display();
		displayStoreProduit();
		ObservableList<Store> dataStore = FXCollections.observableArrayList(Utilislam.getStoreService().findAllStores());
		ObservableList<Product> dataProduct= FXCollections.observableArrayList(Utilislam.getProductService().findAll());
		quantitySlider.valueProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                sliderInt = (int)quantitySlider.getValue();
            }
        });
		StoreComboBox.setItems(dataStore);
		productComboBox.setItems(dataProduct);
		selectStore.setItems(dataStore);
		storeTable.setRowFactory(tv -> {
		    TableRow<StoreProduit> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (! row.isEmpty() && event.getButton()==MouseButton.PRIMARY 
		             && event.getClickCount() == 2) {

		        	StoreProduit clickedRow = row.getItem();
		        	selectedProd = clickedRow.getProduct();
					productComboBox.getSelectionModel().select(selectedProd);
					StoreComboBox.getSelectionModel().select(clickedRow.getStore());
						
					tabPane.getSelectionModel().select(tabAssign);
				
		        }
		    });
		    return row ;
		});

/*
		List<String> categories = new ArrayList<>();
		try {
			Context context = new InitialContext();
			CategoryServiceRemote categoryServiceRemote = (CategoryServiceRemote) context.lookup(jndi2);
			List<String> categoryOld = categoryServiceRemote.DisplayALlCategoriesNames();
			for (String category : categoryOld) {
				categories.add(category);
				ObservableList<String> items = FXCollections.observableArrayList(categories);
				CategoryComboBox.setItems(items);
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
*/
		
	
		
	}
	
	@FXML
	public void AssignProductToStore(){
		Product selectedProduct = productComboBox.getSelectionModel().getSelectedItem();
		Store selectedStr = StoreComboBox.getSelectionModel().getSelectedItem();
		
		if(selectedProduct == null || selectedStr == null){
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Please Select product and store");
			alert.setContentText("Confirm your slection");
			alert.show();
		}else{
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Assignment");
			alert.setContentText("Assignment of the product "+selectedProduct.getName_product());
			alert.showAndWait();
			if(alert.getResult().getText().equalsIgnoreCase("OK"))
			{ 
				Utilislam.getStoreService().affectProductStore(selectedProduct, 
						selectedStr,sliderInt);
				displayStoreProduit();
				
				String contenu = "Ajout de "+ sliderInt+ " produit "+selectedProduct.getName_product()
				+" dans votre store " + selectedStr.getNom_Store();
				
				String email = selectedStr.getEmail();
				String sujet = "Ajout produit";
				
				//Utilislam.sendMail(email , sujet , contenu);
				
				
				//generation de pdf
				try {
					String name = PdfUtils.addDocument(selectedProduct,selectedStr,sliderInt);
				    Desktop.getDesktop().browse(new URI("http://localhost/factures/"+name));
				    
					
				} 
				 catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				selectStore.getSelectionModel().select(selectedStr);
				tabPane.getSelectionModel().select(tabStores);
			}
			
		}
		
		
		
	}
	
	@FXML
	public void Return(){
		
	}
	
	@FXML
	public void onSelectStore(){
		System.out.println("heyy");
		selectedSotre = selectStore.getSelectionModel().getSelectedItem();
		displayStoreProduit();
	}
	
	

}
