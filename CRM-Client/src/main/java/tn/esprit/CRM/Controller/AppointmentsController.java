package tn.esprit.CRM.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;

public class AppointmentsController {

    @FXML
    private JFXComboBox<?> type_combo;

    @FXML
    private JFXComboBox<?> client_combo;

    @FXML
    private DatePicker date_picker;

    @FXML
    private TextArea sujet_area;

    @FXML
    private JFXButton confirm_button;

    @FXML
    private JFXButton cancel_button;

    @FXML
    private TableView<?> appointment_tab;

    @FXML
    private TableColumn<?, ?> client_colone;

    @FXML
    private TableColumn<?, ?> type_colone;

    @FXML
    private TableColumn<?, ?> date_colone;

    @FXML
    private TableColumn<?, ?> topic_colone;

    @FXML
    private JFXTextField search_text;

    @FXML
    private JFXButton search_button;

    @FXML
    private JFXButton delete_button;

    @FXML
    private JFXButton update_button;

    @FXML
    private JFXRadioButton client_radio;

    @FXML
    private JFXRadioButton type_radio;

    @FXML
    private JFXRadioButton topic_radio;

    @FXML
    private JFXButton refrech_button;

    @FXML
    void CancelAction(ActionEvent event) {

    }

    @FXML
    void ConfirmAction(ActionEvent event) {

    }

    @FXML
    void DeleteAction(ActionEvent event) {

    }

    @FXML
    void RefrechAction(ActionEvent event) {

    }

    @FXML
    void SearchAction(ActionEvent event) {

    }

    @FXML
    void clientSelected(ActionEvent event) {

    }

    @FXML
    void topicSelected(ActionEvent event) {

    }

    @FXML
    void typeSelected(ActionEvent event) {

    }

    @FXML
    void updateAction(ActionEvent event) {

    }

}
