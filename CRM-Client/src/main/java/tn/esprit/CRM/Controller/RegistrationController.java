package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import tn.esprit.CRM.persistence.Physical_Person;
import tn.esprit.CRM.services.UserServiceRemote;

public class RegistrationController implements Initializable {

	private static final String JNDI_NAME = "CRM-ear/CRM-ejb/UserService!tn.esprit.CRM.services.UserServiceRemote";

	private static Physical_Person physical_person = new Physical_Person();

	@FXML
	private FontAwesomeIconView retour_first;

	@FXML
	private JFXCheckBox physique;

	@FXML
	private JFXCheckBox moral;

	@FXML
	private FontAwesomeIconView next1;

	@FXML
	private Pane registrationWizard;

	@FXML
	private FontAwesomeIconView retour;

	@FXML
	private JFXTextField tf_cin;

	@FXML
	private JFXPasswordField tf_password;

	@FXML
	private JFXPasswordField tf_repeter;

	@FXML
	private JFXTextField firstName;

	@FXML
	private JFXTextField lastName;

	@FXML
	private JFXPasswordField password;

	@FXML
	private JFXTextField userName;

	@FXML
	private JFXTextField email;

	@FXML
	private JFXTextField tf_address;

	@FXML
	private JFXTextField tf_tel;

	@FXML
	private Label ver_nom;

	@FXML
	private Label ver_user;

	@FXML
	private Label erreurMail;

	@FXML
	private Label ver_mot;

	@FXML
	private Label ver_rep;

	@FXML
	private Label ver_prenom;

	@FXML
	private Label ver_tel;

	@FXML
	private Label ver_cin;

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	@FXML
	void close(MouseEvent event) {
		System.exit(0);
	}

	@FXML
	void back_to_menu(MouseEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/LoginFXML.fxml"));
		MainFX.stage.getScene().setRoot(root);
	}

	@FXML
	void goToRegistrationStepTwo(MouseEvent event) throws IOException {
		// update user

		// change view
		if (physique.isSelected()) {
			physical_person.setRole_user("Physique");
			Parent secondView = FXMLLoader
					.load(getClass().getResource("/tn/esprit/CRM/gui/SecondRegistrationFXML.fxml"));
			MainFX.stage.getScene().setRoot(secondView);
		} else if (moral.isSelected()) {

			Parent secondView = FXMLLoader
					.load(getClass().getResource("/tn/esprit/CRM/gui/MoralRegistrationFXML.fxml"));
			MainFX.stage.getScene().setRoot(secondView);
		} else {
			BoxBlur blur = new BoxBlur(3, 3, 3);
			registrationWizard.setEffect(blur);
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Focus Please");
			alert.setHeaderText("Type not selected");
			alert.setContentText("Select a type");
			alert.showAndWait();
			registrationWizard.setEffect(null);
		}
	}

	@FXML
	void goToRegistrationStepThree(MouseEvent event) throws IOException {
		// update user

		physical_person.setFirst_Name(firstName.getText());
		physical_person.setLast_Name(lastName.getText());
		physical_person.setUser_Name(userName.getText());
		physical_person.setPassword(tf_password.getText());
		physical_person.setEmail(email.getText());

		// change view
		Parent thirdView = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/ThirdRegistrationFXML.fxml"));
		MainFX.stage.getScene().setRoot(thirdView);
	}

	@FXML
	void doRegistration(MouseEvent event) throws IOException {
		try {
			physical_person.setAddress(tf_address.getText());
			physical_person.setPhone_number(Integer.parseInt(tf_tel.getText()));
			physical_person.setCIN(Integer.parseInt(tf_cin.getText()));
			physical_person.setType_client("Prospect");
			createUser();
			Parent loginView = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/LoginFXML.fxml"));
			MainFX.stage.getScene().setRoot(loginView);
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	private void createUser() throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);

		proxy.createUser(physical_person);
	}

	@FXML
	void checkMor(ActionEvent event) {

		if (moral.isSelected()) {
			physique.setSelected(false);
			System.out.println("Moral is Selected!");
		}
	}

	@FXML
	void checkPhy(ActionEvent event) {
		if (physique.isSelected()) {
			moral.setSelected(false);
			System.out.println("Physique is Selected!");
		}
	}

	@FXML
	private void CheckNom(KeyEvent event) {
		String regex = "^[a-zA-Z]+$";
		if (firstName.getText().matches(regex)) {
			ver_nom.setText("Correct ");
			ver_nom.setTextFill(Color.GREEN);
			ver_nom.setVisible(true);
		} else {
			ver_nom.setText("Ce n'est pas un nom!!");
			ver_nom.setTextFill(Color.RED);
			ver_nom.setVisible(true);
		}
	}

	@FXML
	private void CheckPrenom(KeyEvent event) {
		String regex1 = "^[a-zA-Z]+$";
		if (lastName.getText().matches(regex1)) {
			ver_prenom.setText("Correct ");
			ver_prenom.setTextFill(Color.GREEN);
			ver_prenom.setVisible(true);
		} else {
			ver_prenom.setText("Ce n'est pas un prenom!!");
			ver_prenom.setTextFill(Color.RED);
			ver_prenom.setVisible(true);
		}

	}

	@FXML
	private void CheckUser(KeyEvent event) throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);
		String regex = "^[a-zA-Z0-9.\\-_]+$";
		if (userName.getText().matches(regex)) {
			ver_user.setText("C'est possible");
			ver_user.setTextFill(Color.GREEN);
			ver_user.setVisible(true);
		}

		else {
			if (proxy.findUserByUserName(userName.getText()).equals(userName)) {
				ver_user.setText("Exist");
				ver_user.setTextFill(Color.RED);
				ver_user.setVisible(true);
				return;

			}

			else {
				ver_user.setText("Verifie user_name!!");
				ver_user.setTextFill(Color.BLUE);
				ver_user.setVisible(true);
			}
		}
	}

	@FXML
	private void Verif_mail(KeyEvent event) throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);
		if (proxy.findUserByEmail(email.getText()).getId_user() != null) {
			erreurMail.setText("mail exists");
			erreurMail.setTextFill(Color.RED);
			erreurMail.setVisible(true);
			return;
		}

		else {
			if (validateEmail(email.getText())) {
				erreurMail.setText("email Correct!");
				erreurMail.setTextFill(Color.GREEN);
				erreurMail.setVisible(true);
			}

			else {
				erreurMail.setText("Mail incorrect!");
				erreurMail.setTextFill(Color.RED);
				erreurMail.setVisible(true);
			}
		}

	}

	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	@FXML
	private void CheckStength(KeyEvent event) {

		String passs = tf_password.getText();
		boolean lengthRule = passs.length() >= 8 && passs.length() <= 50;

		boolean upperRule = !passs.equals(passs.toLowerCase());
		boolean lowerRule = !passs.equals(passs.toUpperCase());
		boolean numeralRule = passs.matches("[0-9]");
		boolean nonAlphaRule = passs.matches("(.*)[^A-Za-z0-9](.*)");
		int ruleCount = (upperRule ? 1 : 0) + (lowerRule ? 1 : 0) + (numeralRule ? 1 : 0) + (nonAlphaRule ? 1 : 0);
		if (ruleCount >= 3 && lengthRule) {
			ver_mot.setText("Strong password");
			ver_mot.setTextFill(Color.GREEN);
			ver_mot.setVisible(true);
		} else {

			if (ruleCount < 3 || !lengthRule) {
				ver_mot.setText("Weak password!");
				ver_mot.setTextFill(Color.GREEN);
				ver_mot.setVisible(true);
			} else {
				ver_mot.setText("Rpeter!");
				ver_mot.setTextFill(Color.RED);
				ver_mot.setVisible(true);
			}
		}
	}

	@FXML
	private void CheckPassword(KeyEvent event) {
		String pas = tf_password.getText();
		String confirmpas = tf_repeter.getText();

		if ((pas.length() > 0) && (confirmpas.length() > 0) && (pas.equals(confirmpas))) {

			ver_rep.setText("Passwords Identique");
			ver_rep.setTextFill(Color.GREEN);
			ver_rep.setVisible(true);

		} else {
			ver_rep.setText("Passwords Non Identique");
			ver_rep.setTextFill(Color.RED);
			ver_rep.setVisible(true);
		}
	}

	@FXML
	private void CheckPhonenumber(KeyEvent event) {
		if ((tf_tel.getText().length() == 8) && (IsNumber(tf_tel.getText()))) {
			ver_tel.setText("Not a PhoneNumber");
			ver_tel.setTextFill(Color.GREEN);
			ver_tel.setVisible(true);
		} else {
			ver_tel.setText("Correct PhoneNumber");
			ver_tel.setTextFill(Color.RED);
			ver_tel.setVisible(true);
		}
	}

	public boolean IsNumber(String x) {
		boolean verif = true;
		try {
			Float.parseFloat(x);
		} catch (NumberFormatException e) {
			verif = false;
		}
		return verif;
	}

	@FXML
	private void CheckCIN(KeyEvent event) throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);
		if ((tf_cin.getText().length() == 8) && (IsNumber(tf_cin.getText()))
				&& (proxy.findUserByCIN(Integer.parseInt(tf_cin.getText())) == null)) {
			ver_cin.setText("Correct");
			ver_cin.setTextFill(Color.GREEN);
			ver_cin.setVisible(true);
		} else {
			ver_cin.setText("Verify cin");
			ver_cin.setTextFill(Color.RED);
			ver_cin.setVisible(true);
		}
	}

}
