package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class AdminFXMLController implements Initializable {

	@FXML
	private HBox id_clients;

	@FXML
	private Pane content;

	@FXML
	void btn_clients(MouseEvent event) {
		if (event.getTarget() == id_clients) {
			try {
				AnchorPane newLoadedPane = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/Home.fxml"));
				content.getChildren().clear();
				content.getChildren().add(newLoadedPane);
			} catch (IOException ex) {
				Logger.getLogger(ListUsersController.class.getName()).log(Level.SEVERE, null, ex);
			}

		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

}
