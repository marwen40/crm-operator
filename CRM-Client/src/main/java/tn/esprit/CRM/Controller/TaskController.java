package tn.esprit.CRM.Controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.Separator;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import tn.esprit.CRM.persistence.Task;

import tn.esprit.CRM.services.TaskServiceRemote;





public class TaskController implements Initializable  {
	@PersistenceContext
	EntityManager entityManager;

	@FXML
	private Separator sep2;

	@FXML
	private Pane pane1;

	@FXML
	private Pane pane3;

	@FXML
	private Separator sep1;
	@FXML
	private ScrollBar scroll;
	
	 private String jndi1 = "CRM-ear/CRM-ejb/TaskService!tn.esprit.CRM.services.TaskServiceRemote";
	
	private double x1,y1,x2,y2 = 60,x3,y3 = 60;
	
	
	
	
	
	
	public void verifToDo() {
		Context context;
		try {
			context = new InitialContext();
			TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
			List<Task> tasks = taskServiceRemote.findTaskByStatus("ToDo");
			double a = 60;
			for (Task task : tasks) {
				Button button = new Button();
				button.setDisable(false);
				button.setStyle(
						"-fx-background-color: #FFA500;-fx-background-radius: 5,4,3,5;-fx-background-insets: 0,1,2,0;-fx-text-fill: white;-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );-fx-font-family: "
								+ "Arial"
								+ ";-fx-text-fill: linear-gradient(white, #d0d0d0);-fx-font-size: 11px;-fx-padding: 10 20 10 20;");
				button.setCursor(Cursor.HAND);
				button.setText(task.getTitle() + "\n" + "Description : " + task.getDescription_task() + "\n"
						+ "Started Date : " + task.getStart_date_task());
				button.setLayoutX(20);
				button.setLayoutY(a);
				button.setPrefWidth(210);
				button.setPrefHeight(70);
				pane1.getChildren().add(button);
				double x = button.getLayoutX();
				double y = button.getLayoutX();
				button.setOnMouseDragged(e -> {
					button.setLayoutX(e.getSceneX() - 300);
					button.setLayoutY(e.getSceneY() - 150);
				});
				button.setOnMouseReleased(e -> {
					try {
						
						TaskServiceRemote taskServiceRemote1 = (TaskServiceRemote) context
								.lookup(jndi1);
						
						if (button.getLayoutX() > sep1.getLayoutX() && button.getLayoutX() < sep2.getLayoutX()) {
							button.setLayoutX(x2);
							button.setLayoutY(y2 + 75);
							x2 = button.getLayoutX();
							y2 = button.getLayoutY();
							task.setStatus_task("Doing");
							taskServiceRemote1.update(task);
							///this.verifToDo();
							//this.verifDoing();
							//this.verifDone();
							button.setStyle(
									"-fx-background-color: #FFA500;-fx-background-radius: 5,4,3,5;-fx-background-insets: 0,1,2,0;-fx-text-fill: white;-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );-fx-font-family: "
											+ "Arial"
											+ ";-fx-text-fill: linear-gradient(white, #d0d0d0);-fx-font-size: 11px;-fx-padding: 10 20 10 20;");
							this.refreshPlanning();
							
						
							
						}
						if ((button.getLayoutX() > sep2.getLayoutX())) {
							Alert alert = new Alert(Alert.AlertType.ERROR);
							alert.setTitle("Warning");
							alert.setContentText("Wrong update for this Task");
							alert.showAndWait();
							button.setLayoutX(x);
							button.setLayoutY(y);
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				});
				button.setOnMouseClicked(e -> {
			            if((e.getClickCount()) == 2 && (e.getButton() == MouseButton.PRIMARY)){
			            	Alert alert = new Alert(Alert.AlertType.INFORMATION);
							alert.setTitle("Information");
							alert.setContentText("Description : "+task.getDescription_task()+"\n"+"Starting Date : "+task.getStart_date_task()+"\n"+"Product : "+task.getUser().getUser_Name()+"\n"+"End Date : "+task.getEnd_date_task()+"\n"+"Status : "+task.getStatus_task()+"\n");
							alert.showAndWait();
			            }
				});
				button.setOnMouseClicked(e ->{
					if (e.getButton() == MouseButton.SECONDARY) {
						final ContextMenu contextMenu = new ContextMenu();
						MenuItem details = new MenuItem("Details");
						details.setOnAction(r ->{
							Alert alert = new Alert(Alert.AlertType.INFORMATION);
							alert.setTitle("Information");
							alert.setContentText("Description : "+task.getDescription_task()+"\n"+"Starting Date : "+task.getStart_date_task()+"\n"+"Product : "+task.getUser().getUser_Name()+"\n"+"End Date : "+task.getEnd_date_task()+"\n"+"Status : "+task.getStatus_task()+"\n");	
							alert.showAndWait();
						});
						MenuItem delete = new MenuItem("Delete");
						delete.setOnAction(t ->{
							Alert alert = new Alert(Alert.AlertType.WARNING);
							alert.setTitle("Warning");
							alert.setContentText("Are you sure for Deleting this Task ?");
							alert.showAndWait();
							if (alert.getResult().getText().equalsIgnoreCase("OK")) {
								taskServiceRemote.deleteTask(task);
								this.refreshPlanning();
								
								//this.verifToDo();
								//this.verifDoing();
								//this.verifDone();
							}
						});
						contextMenu.getItems().addAll(details, delete);
						button.setContextMenu(contextMenu);
					}
				});
				a = button.getLayoutY() + button.getPrefHeight() + 5;
			}
		} catch (NamingException e2) {
			e2.printStackTrace();
		}
	}
	
	
	public void verifDoing() {
		Context context;
		try {
			context = new InitialContext();
			TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
			List<Task> task1 = taskServiceRemote.findTaskByStatus("Doing");
			double b = 60;
			for (Task task : task1) {
				Button button = new Button();
				button.setId("button");
				button.setDisable(false);
				button.setCursor(Cursor.HAND);
				button.setText(task.getTitle() + "\n" + "Description : " + task.getDescription_task() + "\n"
						+ "Started Date : " + task.getStart_date_task());
				button.setLayoutX(sep1.getLayoutX() + 20 );
				button.setLayoutY(b);
				button.setPrefWidth(210);
				button.setPrefHeight(70);
				double x = button.getLayoutX();
				double y = button.getLayoutY();
				button.setStyle(
						"-fx-background-color: #daa520;-fx-background-radius: 5,4,3,5;-fx-background-insets: 0,1,2,0;-fx-text-fill: white;-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );-fx-font-family: "
								+ "Arial"
								+ ";-fx-text-fill: linear-gradient(white, #d0d0d0);-fx-font-size: 11px;-fx-padding: 10 20 10 20;");
				pane1.getChildren().add(button);
				x2 = button.getLayoutX();
				y2 = button.getLayoutY();
				button.setOnMouseDragged(e -> {
					button.setLayoutX(e.getSceneX() - 300);
					button.setLayoutY(e.getSceneY() - 150);
				});
				button.setOnMouseReleased(e -> {
					try {
						Context context2 = new InitialContext();
						TaskServiceRemote taskServiceRemote1 = (TaskServiceRemote) context
								.lookup(jndi1);
						//ProductServiceRemote productServiceRemote = (ProductServiceRemote) context2.lookup(jndi1);
						if (button.getLayoutX() < sep1.getLayoutX()) {
							Alert alert = new Alert(Alert.AlertType.ERROR);
							alert.setTitle("Warning");
							alert.setContentText("Wrong update for this Task");
							alert.showAndWait();
							button.setLayoutX(x);
							button.setLayoutY(y);
						}
						if (button.getLayoutX() > sep1.getLayoutX() && button.getLayoutX() < sep2.getLayoutX()) {
							button.setLayoutX(x);
							button.setLayoutY(y);
						}
						if ((button.getLayoutX() > sep2.getLayoutX())) {
							button.setLayoutX(x3);
							button.setLayoutY(y3 + 75);
							x3 = button.getLayoutX();
							y3 = button.getLayoutY();
							task.setStatus_task("Done");
							taskServiceRemote.update(task);
							
							//this.verifToDo();
							//this.verifDoing();
							//this.verifDone();
							//User user = user.getUser_Name();
							//user.setQuantity(product.getQuantity() + production.getQuantite());
							//productServiceRemote.update(product);
							button.setStyle(
									"-fx-background-color: #32cd32;-fx-background-radius: 5,4,3,5;-fx-background-insets: 0,1,2,0;-fx-text-fill: white;-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );-fx-font-family: "
											+ "Arial"
											+ ";-fx-text-fill: linear-gradient(white, #d0d0d0);-fx-font-size: 11px;-fx-padding: 10 20 10 20;");
							this.refreshPlanning();
							
							//manufacturingStatistiqueController.statistique();
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				});
				button.setOnMouseClicked(e -> {
		            if((e.getClickCount()) == 2 && (e.getButton() == MouseButton.PRIMARY)){
		            	Alert alert = new Alert(Alert.AlertType.INFORMATION);
						alert.setTitle("Information");
						alert.setContentText("Description : "+task.getDescription_task()+"\n"+"Starting Date : "+task.getStart_date_task()+"\n"+"Product : "+task.getUser().getUser_Name()+"\n"+"End Date : "+task.getEnd_date_task()+"\n"+"Status : "+task.getStatus_task()+"\n");
						alert.showAndWait();
		            }
			});
				button.setOnMouseClicked(e ->{
					if (e.getButton() == MouseButton.SECONDARY) {
						final ContextMenu contextMenu = new ContextMenu();
						MenuItem details = new MenuItem("Details");
						details.setOnAction(r ->{
							Alert alert = new Alert(Alert.AlertType.INFORMATION);
							alert.setTitle("Information");
							alert.setContentText(task.getTitle() + "\n" + "Description : " + task.getDescription_task() + "\n"
									+ "Started Date : " + task.getStart_date_task());	
							alert.showAndWait();
						});
						MenuItem update = new MenuItem("Send to Done Task");
						update.setOnAction(t ->{
							Alert alert = new Alert(Alert.AlertType.WARNING);
							alert.setTitle("Warning");
							alert.setContentText("Are you sure that this Task is DONE ?");
							alert.showAndWait();
							if (alert.getResult().getText().equalsIgnoreCase("OK")) {
								task.setStatus_task("Done");
								taskServiceRemote.update(task);
								
								//((this.verifToDo();
								//this.verifDoing();
								//this.verifDone();
								this.refreshPlanning();
							}
						});
						contextMenu.getItems().addAll(details, update);
						button.setContextMenu(contextMenu);
					}
				});
				b = button.getLayoutY() + button.getPrefHeight() + 5;
			}
		} catch (NamingException e2) {
			e2.printStackTrace();
		}

	}
	
	
	
	public void verifDone() {
		Context context;
		try {
			context = new InitialContext();
			TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
			List<Task> task2 = taskServiceRemote.findTaskByStatus("Done");
			double c = 60;
			for (Task task : task2) {
				Button button = new Button();
				button.setStyle(
						"-fx-background-color: #32cd32;-fx-background-radius: 5,4,3,5;-fx-background-insets: 0,1,2,0;-fx-text-fill: white;-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );-fx-font-family: "
								+ "Arial"
								+ ";-fx-text-fill: linear-gradient(white, #d0d0d0);-fx-font-size: 11px;-fx-padding: 10 20 10 20;");
				button.setDisable(false);
				button.setCursor(Cursor.HAND);
				button.setText(task.getTitle() + "\n" + "Description : " + task.getDescription_task() + "\n"
						+ "Started Date : " + task.getStart_date_task());
				button.setLayoutX(sep2.getLayoutX() + 20);
				button.setLayoutY(c);
				button.setPrefWidth(210);
				button.setPrefHeight(70);
				pane1.getChildren().add(button);
				double x = button.getLayoutX();
				double y = button.getLayoutY();
				x3 = button.getLayoutX();
				y3 = button.getLayoutY();
				button.setOnMouseDragged(e -> {
					button.setLayoutX(e.getSceneX() - 300);
					button.setLayoutY(e.getSceneY() - 150);
				});
				button.setOnMouseReleased(e -> {
					try {
						Context context2 = new InitialContext();
						TaskServiceRemote taskServiceRemote1 = (TaskServiceRemote) context
								.lookup(jndi1);
						if (button.getLayoutX() > sep1.getLayoutX() && button.getLayoutX() < sep2.getLayoutX()) {
							Alert alert = new Alert(Alert.AlertType.ERROR);
							alert.setTitle("Warning");
							alert.setContentText("Wrong update for this Task");
							alert.showAndWait();
							button.setLayoutX(x);
							button.setLayoutY(y);
						}
						if (button.getLayoutX() < sep1.getLayoutX()) {
							/*
							 * production.setProductionStatus("Done");
							 * productionServiceRemote.update(production);
							 */
							/*task.setStatus_task("Done");
							taskServiceRemote1.update(task);*/
							Alert alert = new Alert(Alert.AlertType.ERROR);
							alert.setTitle("Warning");
							alert.setContentText("Wrong update for this Task");
							alert.showAndWait();
							button.setLayoutX(x);
							button.setLayoutY(y);
						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				});
				button.setOnMousePressed(e -> {
		            if(e.getClickCount() == 2){
		            	Alert alert = new Alert(Alert.AlertType.INFORMATION);
						alert.setTitle("Information");
						alert.setContentText("Description : "+task.getDescription_task()+"\n"+"Starting Date : "+task.getStart_date_task()+"\n"+"Product : "+task.getUser().getUser_Name()+"\n"+"End Date : "+task.getEnd_date_task()+"\n"+"Status : "+task.getStatus_task()+"\n");
						alert.showAndWait();
		            }
			});
				button.setOnMouseClicked(e ->{
					if (e.getButton() == MouseButton.SECONDARY) {
						final ContextMenu contextMenu = new ContextMenu();
						MenuItem details = new MenuItem("Details");
						details.setOnAction(r ->{
							Alert alert = new Alert(Alert.AlertType.INFORMATION);
							alert.setTitle("Information");
							alert.setContentText("Description : "+task.getDescription_task()+"\n"+"Starting Date : "+task.getStart_date_task()+"\n"+"Product : "+task.getUser().getUser_Name()+"\n"+"End Date : "+task.getEnd_date_task()+"\n"+"Status : "+task.getStatus_task()+"\n");
							alert.showAndWait();
						});
						contextMenu.getItems().addAll(details);
						button.setContextMenu(contextMenu);
					}
				});
				c = button.getLayoutY() + button.getPrefHeight() + 5;
			}
		} catch (NamingException e2) {
			e2.printStackTrace();
		}

	}

	
	public void refreshPlanning(){
		
		pane1.getChildren().clear();
		this.verifToDo();
		this.verifDoing();
		this.verifDone();
	
	}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		x2 = sep1.getLayoutX() + 20;
		x3 = sep2.getLayoutX() + 20;
		//this.verifToDo();
		this.refreshPlanning();
		//this.verifDoing();
		//this.verifDone();
		//this.verifToDo();
		
		
	}
	
	
	
}
