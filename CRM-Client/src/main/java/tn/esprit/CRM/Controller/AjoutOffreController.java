package tn.esprit.CRM.Controller;

import javax.naming.Context;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
//import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
//import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Product;
//import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.services.OffreServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

public class AjoutOffreController implements Initializable {

	@FXML
	private JFXTextField tf_offer_name;

	@FXML
	private JFXTextField quantite;

	@FXML
	private JFXComboBox<String> Product_LD;

	@FXML
	private DatePicker Dp_deb;

	@FXML
	private JFXTextField gain_tf;

	@FXML
	private DatePicker dP_fin;

	@FXML
	private JFXComboBox<?> productionComboBox;

	@FXML
	private DatePicker newDate;

	@FXML
	private JFXTextField newQuantity;

	@FXML
	private TableView<Offre> OfferTable;

	@FXML
	private TableColumn<Offre, String> TitleT;

	@FXML
	private TableColumn<Offre, Date> SDateT;

	@FXML
	private TableColumn<Offre, Date> EnDateT;

	@FXML
	private TableColumn<Offre, String> productT;

	@FXML
	private TableColumn<Offre, Float> PriceT;
    @FXML
    private TableColumn<Offre, String> VisibT;
	@FXML
	private JFXTextField searchText;

	@FXML
	private JFXButton refreshBtn;

	@FXML
	private JFXButton searchBtn;

	@FXML
	private JFXButton returnBtn;
    @FXML
    private JFXButton VIsibBtn;

    @FXML
    private JFXButton InviBtn;

	@FXML
	private JFXButton deletOfBtn;

	private String jndi = "CRM-ear/CRM-ejb/OffreService!tn.esprit.CRM.services.OffreServiceRemote";
	private String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
	public static Offre offre;

	@FXML
	void AddProduction(ActionEvent event) {

		try {
			Context context = new InitialContext();
			OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
			Offre offre = new Offre(tf_offer_name.getText(), java.sql.Date.valueOf(Dp_deb.getValue()),
					java.sql.Date.valueOf(dP_fin.getValue()), Float.parseFloat(gain_tf.getText()));

			
			int startDate =java.sql.Date.valueOf(Dp_deb.getValue()).getDate();
			int newDate=new java.util.Date().getDate();
		
			
			if ((startDate<newDate)){
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error Dialog");
				alert.setHeaderText(java.sql.Date.valueOf(Dp_deb.getValue())+" is not valid");
				alert.showAndWait();
			} else  {
			
			Integer g = offreServiceRemote.createOffre(offre);
			System.out.println(g.toString());
			String lproduit = Product_LD.getValue();
			System.out.println(lproduit);
			Product p = productServiceRemote.FindProductByTitleCyrine(lproduit);

			productServiceRemote.assignProductToOfferCyrine(g, p.getId_product());

			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Success!!");
			alert.setContentText("The offer has been successfully added");
			alert.showAndWait();
			}
		} catch (Exception e) {

		}
		this.refresh(event);
		tf_offer_name.setText(null);
		gain_tf.setText(null);
		Dp_deb.setValue(null);
		dP_fin.setValue(null);
		Product_LD.setValue(null);

	}

	@FXML
	void Return(ActionEvent event) throws NamingException {
		Context context = new InitialContext();
		OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);

	}

	@FXML
	void SortBy(ActionEvent event) {

	}

	@FXML
	void UpdateProduction(ActionEvent event) {

	}

	@FXML
	void refresh(ActionEvent event) {
		try {
			InitialContext ctx;
			OffreServiceRemote proxy;
			ctx = new InitialContext();
			proxy = (OffreServiceRemote) ctx
					.lookup("CRM-ear/CRM-ejb/OffreService!tn.esprit.CRM.services.OffreServiceRemote");
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) ctx.lookup(jndi1);
		
		
			
			TitleT.setCellValueFactory(new PropertyValueFactory<Offre, String>("title_offre"));
			SDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("start_date_offre"));
			EnDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("end_date_offre"));
			productT.setCellValueFactory(new PropertyValueFactory<Offre, String>("product"));
			PriceT.setCellValueFactory(new PropertyValueFactory<Offre, Float>("percent_gain_offre"));
			VisibT.setCellValueFactory(new PropertyValueFactory<Offre,String>("visibility_offre"));
			
			ObservableList<Offre> data = FXCollections.observableArrayList(proxy.findalladmin());
			for (Offre x : data) {
			
				if ((x.getEnd_date_offre().getDate() <new java.util.Date().getDate())) {
//					Alert alert = new Alert(Alert.AlertType.ERROR);
//					alert.setTitle("Error Dialog");
//					alert.setHeaderText("Offer ha been deleted ");
//
//					alert.showAndWait();
					proxy.invisible(x);
					this.refresh(event);
				}
			}
			OfferTable.setItems(data);

		} catch (NamingException e1) {

		}
		

	}

	@FXML
	void search(KeyEvent event) {

	}

	@FXML
	void setProductionAttributes(ActionEvent event) {

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		List<String> categories = new ArrayList<>();
		try {
			Context context = new InitialContext();
			
			//ProductServiceRemote categoryServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
			//List<String> categoryOld = categoryServiceRemote.DisplayALlProductsNames();
			List<String> categoryOldd = new ArrayList<>();
			OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);
			 categoryOldd = offreServiceRemote.findproductlibre();
for (String category : categoryOldd) {
				categories.add(category);
				ObservableList<String> items = FXCollections.observableArrayList(categories);
				Product_LD.setItems(items);
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}

		try {
			InitialContext ctx;
			OffreServiceRemote proxy;
			ctx = new InitialContext();
			proxy = (OffreServiceRemote) ctx
					.lookup("CRM-ear/CRM-ejb/OffreService!tn.esprit.CRM.services.OffreServiceRemote");
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) ctx.lookup(jndi1);
			TitleT.setCellValueFactory(new PropertyValueFactory<Offre, String>("title_offre"));
			SDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("start_date_offre"));
			EnDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("end_date_offre"));
			productT.setCellValueFactory(new PropertyValueFactory<Offre, String>("product"));
			PriceT.setCellValueFactory(new PropertyValueFactory<Offre, Float>("percent_gain_offre"));
			VisibT.setCellValueFactory(new PropertyValueFactory<Offre,String>("visibility_offre"));
			ObservableList<Offre> data = FXCollections.observableArrayList(proxy.findalladmin());

			OfferTable.setItems(data);

		} catch (NamingException e1) {

		}

	}

	@FXML
	void DeleteOffre(ActionEvent event) throws NamingException {

		Context context;
		context = new InitialContext();

		OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);

		Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
		Offre off = (Offre) OfferTable.getSelectionModel().getSelectedItem();
		if (OfferTable.getSelectionModel().isSelected(selected)) {
			offreServiceRemote.deleteOffre(OfferTable.getItems().get(selected));
		} else {
			System.out.println("selectionner un élement non traité" + selected);
		}
		this.refresh(event);
		
	}

	@FXML
	void CancelOBtn(ActionEvent event) {
		tf_offer_name.setText(null);
		gain_tf.setText(null);
		Dp_deb.setValue(null);
		dP_fin.setValue(null);
		Product_LD.setValue(null);

	}

	@FXML
	void updateOfferBtn(ActionEvent event) throws IOException {
		if (OfferTable.getSelectionModel().getSelectedIndex() < 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Offer not selected");
			alert.setContentText("Please select an Offer");
			alert.showAndWait();
		} else {
			this.offre = OfferTable.getSelectionModel().getSelectedItem();
			Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/UpdateOffre.fxml")));
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
		}
		this.refresh(event);

	}

    @FXML
    void VisibAction(ActionEvent event) throws NamingException {Context context;
	context = new InitialContext();

	OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);
	

	Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
	Offre off = (Offre) OfferTable.getSelectionModel().getSelectedItem();
	if (OfferTable.getSelectionModel().isSelected(selected) && (off.getVisibility_offre()==false)) {
		//offreServiceRemote.deleteOffre(OfferTable.getItems().get(selected));
		offreServiceRemote.visible(OfferTable.getItems().get(selected));
		this.refresh(event);
	} else {Alert alert = new Alert(Alert.AlertType.ERROR);
	alert.setTitle("Error Dialog");
	alert.setHeaderText("Offer is visible");
	alert.setContentText("Please select an invisible Offer");
	alert.showAndWait();
		
	}
	
	this.refresh(event);
}

   

    @FXML
    void InviAction(ActionEvent event) throws NamingException {
    	Context context;
		context = new InitialContext();

		OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);

		Integer selected = OfferTable.getSelectionModel().getSelectedIndex();
		Offre off = (Offre) OfferTable.getSelectionModel().getSelectedItem();
		if (OfferTable.getSelectionModel().isSelected(selected)&&off.getVisibility_offre()==true) {
			//offreServiceRemote.deleteOffre(OfferTable.getItems().get(selected));
			offreServiceRemote.invisible(OfferTable.getItems().get(selected));
			
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error Dialog");
			alert.setHeaderText("Offer is invisible");
			alert.setContentText("Please select a visible Offer");
			alert.showAndWait();
		}
		List<Offre> Offres = offreServiceRemote.findalladmin();
		TitleT.setCellValueFactory(new PropertyValueFactory<Offre, String>("title_offre"));
		SDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("start_date_offre"));
		EnDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("end_date_offre"));
		// productT.setCellValueFactory(new
		// PropertyValueFactory<Offre,Integer>("id_product"));
		PriceT.setCellValueFactory(new PropertyValueFactory<Offre, Float>("percent_gain_offre"));
		VisibT.setCellValueFactory(new PropertyValueFactory<Offre,String>("visibility_offre"));
		ObservableList<Offre> data = FXCollections.observableArrayList(offreServiceRemote.findalladmin());
		OfferTable.setItems(data);
		this.refresh(event);
    }
 

    @FXML
    void Searchbytitle(KeyEvent event) {
    	for (int i=0;i<OfferTable.getItems().size();i++){
    		OfferTable.getItems().clear();
		}
    	Context context;
    	try {
			context = new InitialContext();
			OffreServiceRemote offreServiceRemote = (OffreServiceRemote) context.lookup(jndi);
			TitleT.setCellValueFactory(new PropertyValueFactory<Offre, String>("title_offre"));
			SDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("start_date_offre"));
			EnDateT.setCellValueFactory(new PropertyValueFactory<Offre, Date>("end_date_offre"));
			productT.setCellValueFactory(new PropertyValueFactory<Offre, String>("product"));
			PriceT.setCellValueFactory(new PropertyValueFactory<Offre, Float>("percent_gain_offre"));
			VisibT.setCellValueFactory(new PropertyValueFactory<Offre,String>("visibility_offre"));
			ObservableList<Offre> data = FXCollections.observableArrayList(offreServiceRemote.findOffreByTitle(searchText.getText()));
			OfferTable.setItems(data);
    	}catch (NamingException e) {
			e.printStackTrace();
		}
    }

}
