package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.UserServiceRemote;

public class LoginController implements Initializable {

	@FXML
	private JFXTextField username_login;

	@FXML
	private JFXPasswordField password_login;

	@FXML
	private JFXButton btn_login;

	@FXML
	void login(ActionEvent event) throws NamingException, IOException {
		String jndi = "CRM-ear/CRM-ejb/UserService!tn.esprit.CRM.services.UserServiceRemote";
		Context context = new InitialContext();
		UserServiceRemote userserviceremote = (UserServiceRemote) context.lookup(jndi);
		MainFX.user = userserviceremote.login(username_login.getText(), password_login.getText());
		if (MainFX.user != null) {
			if(MainFX.user.getRole_user().equals("RH")){
			Parent pp = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/test.fxml"));
			Scene scene = new Scene(pp);
			MainFX.stage.setScene(scene);
			} 
			else if(MainFX.user.getRole_user().equals("staff")){
					Parent pp = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/MesConge.fxml"));
					Scene scene = new Scene(pp);
					MainFX.stage.setScene(scene);
					}
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Authentification error");
			alert.setContentText("Please check your data");
			alert.showAndWait();
		}
	}

	@FXML
	void open_registration(MouseEvent event) throws IOException {
		Parent pp = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/FirstRegistrationFXML.fxml"));
		Scene scene = new Scene(pp);
		MainFX.stage.setScene(scene);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@FXML
	void close(MouseEvent event) {
	}

	@FXML
	void close_app(MouseEvent event) {
		System.exit(0);
	}
}
