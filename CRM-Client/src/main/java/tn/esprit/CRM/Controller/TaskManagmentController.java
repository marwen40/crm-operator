package tn.esprit.CRM.Controller;



import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFileChooser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;

import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Status_type;
import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.services.TaskServiceRemote;
import tn.esprit.CRM.services.TodoServiceRemote;





public class TaskManagmentController implements Initializable{
    @FXML
    private JFXTextField title;

    @FXML
    private JFXTextField Description;

    @FXML
    private JFXComboBox<String> PriorityCombobox;

    @FXML
    private JFXComboBox<String> EmployeeComboBox;


    @FXML
    private JFXDatePicker endDate;

	    @FXML
	    private JFXComboBox<?> productionComboBox;

	    @FXML
	    private TableView<Task> TaskTable;

	    @FXML
	    private TableColumn<Task, String> TitleT;

	    @FXML
	    private TableColumn<Task, Date> EnddateT;
	    @FXML
	    private TableColumn<Task, Date> StartdateT;

	    @FXML
	    private TableColumn<?, ?> employeeT;

	    @FXML
	    private TableColumn<Task, String> StatusT;

	    @FXML
	    private TableColumn<Task, String> DescirptionT;

	    @FXML
	    private TableColumn<Task, String> PriorityT;

	    @FXML
	    private JFXTextField searchText;
	    @FXML
	    private JFXButton ScrumBtn;
	    @FXML
	    private Label sortLabel;
	    @FXML
	    private Label fileDirectory;

	   

	    @FXML
	    private JFXButton searchBtn;

	    @FXML
	    private JFXButton refreshBtn;

	    @FXML
	    private JFXButton returnBtn;

	    @FXML
	    private JFXButton mailBtn;
	    @FXML
	    private Label Error;
	    @FXML
	    private Label titleController;
	    @FXML
	    private Label descriptionController;

	 
	    @FXML
	    private Button addFileBtn;
	    @FXML
	    private JFXButton updateBtn;

	    @FXML
	    private JFXButton deletBtn;

	    @FXML
	    private JFXComboBox<String> sortCB;
	    private String jndi1 = "CRM-ear/CRM-ejb/TaskService!tn.esprit.CRM.services.TaskServiceRemote";
	    private String jndi = "CRM-ear/CRM-ejb/TodoService!tn.esprit.CRM.services.TodoServiceRemote";
		String message1 = "bonjour";
		String path = null;
		 public static Task task;
		 String regex1 = "^[a-zA-Z]+$";
			private void displayByPriority() {
				for (int i=0;i<TaskTable.getItems().size();i++){
					TaskTable.getItems().clear();
				}
				Context context;
				try {
					context = new InitialContext();
					//ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context.lookup(jndi2);
					TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
					TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
					TitleT.setPrefWidth(150); 
					EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
					EnddateT.setPrefWidth(175);
					StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
					StartdateT.setPrefWidth(175);
					employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
					employeeT.setPrefWidth(150);
					StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
					StatusT.setPrefWidth(150);
					DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
					DescirptionT.setPrefWidth(120);
					PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
					PriorityT.setPrefWidth(120);

					ObservableList<Task> data = FXCollections.observableArrayList(taskServiceRemote.sortTaskByPriority());
					
					TaskTable.setItems(data);
				
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		   
			
			
			private void displayByStatus() {
				for (int i=0;i<TaskTable.getItems().size();i++){
					TaskTable.getItems().clear();
				}
				Context context;
				try {
					context = new InitialContext();
					TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
					TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
					TitleT.setPrefWidth(150); 
					EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
					EnddateT.setPrefWidth(175);
					StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
					StartdateT.setPrefWidth(175);
					employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
					employeeT.setPrefWidth(150);
					StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
					StatusT.setPrefWidth(150);
					DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
					DescirptionT.setPrefWidth(120);
					PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
					PriorityT.setPrefWidth(120);

					ObservableList<Task> data = FXCollections.observableArrayList(taskServiceRemote.sortTaskByStatus());
					
					TaskTable.setItems(data);
				
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}

			
			
			private void displayByEndDate() {
				for (int i=0;i<TaskTable.getItems().size();i++){
					TaskTable.getItems().clear();
				}
				Context context;
				try {
					context = new InitialContext();
					//ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context.lookup(jndi2);
					TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
					TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
					TitleT.setPrefWidth(150); 
					EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
					EnddateT.setPrefWidth(175);
					StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
					StartdateT.setPrefWidth(175);
					employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
					employeeT.setPrefWidth(150);
					StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
					StatusT.setPrefWidth(150);
					DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
					DescirptionT.setPrefWidth(120);
					PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
					PriorityT.setPrefWidth(120);

					ObservableList<Task> data = FXCollections.observableArrayList(taskServiceRemote.sortTaskByEndDate());
					
					TaskTable.setItems(data);
				
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			private void displayByStartDate() {
				for (int i=0;i<TaskTable.getItems().size();i++){
					TaskTable.getItems().clear();
				}
				Context context;
				try {
					context = new InitialContext();
					//ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context.lookup(jndi2);
					TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
					TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
					TitleT.setPrefWidth(150); 
					EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
					EnddateT.setPrefWidth(175);
					StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
					StartdateT.setPrefWidth(175);
					employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
					employeeT.setPrefWidth(150);
					StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
					StatusT.setPrefWidth(150);
					DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
					DescirptionT.setPrefWidth(120);
					PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
					PriorityT.setPrefWidth(120);

					ObservableList<Task> data = FXCollections.observableArrayList(taskServiceRemote.sortTaskByStartDate());
					
					TaskTable.setItems(data);
				
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			@FXML
		    void sortBy(ActionEvent event) {
		    	if (sortCB.getValue().toString().equalsIgnoreCase("Priority")){
		    		this.displayByPriority();
		    	}
		    	if (sortCB.getValue().toString().equalsIgnoreCase("Status")){
		    		this.displayByStatus();
		    	}
		    	if (sortCB.getValue().toString().equalsIgnoreCase("End Date")){
		    		this.displayByEndDate();
		    	}
		    	if (sortCB.getValue().toString().equalsIgnoreCase("Start Date")){
		    		this.displayByStartDate();
		    	}
		    }
		    @FXML
		void TitleController(KeyEvent event) {
		    	
				if( (title.getText().matches(regex1))) {
					titleController.setText("Correct");
					titleController.setTextFill(Color.GREEN);
					titleController.setVisible(true);
				} 
				else if ((title.getText().equals("")==true)) {
					titleController.setText("Title Empty");
					titleController.setTextFill(Color.RED);
					titleController.setVisible(true);
					
				}
			
				else {
					titleController.setText("Do not put numbers OR Spaces");
					titleController.setTextFill(Color.RED);
					titleController.setVisible(true);
				}
				
		    }
		    @FXML
		void DescriptionController(KeyEvent event) {
		    	if (Description.getText().equals("")==false) {
					descriptionController.setText("Correct");
					descriptionController.setTextFill(Color.GREEN);
					descriptionController.setVisible(true);
				} else {
					descriptionController.setText("Description empty");
					descriptionController.setTextFill(Color.RED);
					descriptionController.setVisible(true);
				}
		    }
		
		  
	    @FXML
	    void AddFileToTask(ActionEvent event) throws NamingException {
	    	final JFileChooser fc = new JFileChooser();
	        fc.showOpenDialog(fc);
	        File selectedFile = fc.getSelectedFile();
	        String path = selectedFile.getAbsolutePath();
	        this.path = path;
	        fileDirectory.setText(path);
	    }
	    @FXML
	    void AddTask(ActionEvent event) throws Exception {
	    
	    	Context context = new InitialContext();
			TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
			TodoServiceRemote todoServiceRemote = (TodoServiceRemote) context.lookup(jndi);
			
			Status_type status_type;
			Date sysdate = new Date();
			Date dates = java.sql.Date.valueOf(endDate.getValue());
			String oldstring = endDate.getValue().toString();
			String user1 = EmployeeComboBox.getValue(); 
		
					LocalDateTime datetime1 = LocalDateTime.now();  
				    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
				    String formatDateTime = datetime1.format(format);   
				    System.out.println(formatDateTime);    	 
					if(titleController.getText()==null&& descriptionController.getText()==null&& title.getText()==null &&Description.getText()==null&&endDate.getValue()==null&&PriorityCombobox.getValue()==null&&fileDirectory.getText()==null&&EmployeeComboBox.getValue()==null)   {

						Alert alert = new Alert(Alert.AlertType.ERROR);
						alert.setTitle("Error");
						alert.setContentText("Empty Task");
						alert.showAndWait();
					}else 
					{
			if ((titleController.getText().equals("Correct"))&& descriptionController.getText().equals("Correct")&& (EmployeeComboBox.getValue()!=null)&&(PriorityCombobox.getValue()!=null))   {
			if (dates.compareTo(sysdate) == 1) {
				status_type = Status_type.valueOf("ToDo");
			Task task = new Task(title.getText(), Description.getText(),sysdate,java.sql.Date.valueOf(endDate.getValue()),status_type.toString(),PriorityCombobox.getValue(),fileDirectory.getText());
			Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
	        alert1.setTitle("Warning");
	        alert1.setContentText("Do you really want to Add this Task");
	        alert1.showAndWait();
	        if (alert1.getResult().getText().equalsIgnoreCase("OK")) {
	        	Integer g =taskServiceRemote.createTask(task);
	        	String user = EmployeeComboBox.getValue();
			    todoServiceRemote.assignTaskToEmployee(g,todoServiceRemote.findIdUserByUsername(user) );
			    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	        	alert.setTitle("Information Dialog");
	        	alert.setHeaderText(null);
	        	alert.setContentText("Task Added");
	        	alert.showAndWait();
	        	this.display();
			    this.display();
				
				   
			    this.CancelTask(event);
	        }
	        else   {
	        	this.CancelTask(event);
	        }
			}
			else if (oldstring.equals(formatDateTime)) {
				status_type = Status_type.valueOf("Doing");
				Task task = new Task(title.getText(), Description.getText(),sysdate,java.sql.Date.valueOf(endDate.getValue()),status_type.toString(),PriorityCombobox.getValue(),fileDirectory.getText());
				Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
		        alert1.setTitle("Warning");
		        alert1.setContentText("Do you really want to Add this Task");
		        alert1.showAndWait();  
				if (alert1.getResult().getText().equalsIgnoreCase("OK")) {
			        	Integer g =taskServiceRemote.createTask(task);
			        	String user = EmployeeComboBox.getValue();
					    todoServiceRemote.assignTaskToEmployee(g,todoServiceRemote.findIdUserByUsername(user) );
					    Alert alert = new Alert(Alert.AlertType.INFORMATION);
			        	alert.setTitle("Information Dialog");
			        	alert.setHeaderText(null);
			        	alert.setContentText("Task Added");
			        	alert.showAndWait();
			        	this.display();
					    this.display();
						
						   
					    this.CancelTask(event);
			        }
			        else   {
			        	this.CancelTask(event);
			        }
		
			} 
			 else
			{
				
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setContentText("The date is minor to the Sysdate, Please choose another date");
				alert.showAndWait();
			}}
			
				else 
			{
				
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setContentText("Check Your Information Task Please");
				alert.showAndWait();
			}
			
	    
	    }
			
				}
				  
	    public  void callURL(String a) {
	         
	        String myURL="https://rest.nexmo.com/sms/json?api_key=d921339e&api_secret=DqE07LGB7Ipt3VtK&to=216"+
	                "21502464"+"&from=OrionErp&text=you+have+an+appointment+tomorrow+please+be+in+Time+"+a;
	        System.out.println(myURL);
	        StringBuilder sb = new StringBuilder();
	        URLConnection urlConn = null;
	        InputStreamReader in = null;
	        try {
	            URL url = new URL(myURL);
	            urlConn = url.openConnection();
	            if (urlConn != null)
	                urlConn.setReadTimeout(60 * 1000);
	            if (urlConn != null && urlConn.getInputStream() != null) {
	                in = new InputStreamReader(urlConn.getInputStream(),
	                        Charset.defaultCharset());
	                BufferedReader bufferedReader = new BufferedReader(in);
	                if (bufferedReader != null) {
	                    int cp;
	                    while ((cp = bufferedReader.read()) != -1) {
	                        sb.append((char) cp);
	                    }
	                    bufferedReader.close();
	                }
	            }
	            in.close();
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while calling URL:"+ myURL, e);
	        }
	        
	        
	    }	
	    public void SendSMS(){
	    	
	    	try {
				Context context = new InitialContext();
				TaskServiceRemote appointmentServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
				List<Task> lista = appointmentServiceRemote.FindAllTask();
				Date dates = new Date();
				for (Task appointment : lista) {
					Date dates2 = appointment.getEnd_date_task();
					long x = ((dates2.getTime()-dates.getTime())/3600000);
						if(x<24 && x>0){
							this.callURL(appointment.getUser().getUser_Name());
							//System.out.println("cc");
						}
							
						}
					
				
				
			} catch (NamingException e) {
		
			}}
	    
	    @FXML
	    void Return(ActionEvent event) {
	    	
	    }
	   
	    public void envoyer(String ex) {
	        final String mail = "emna.abid@esprit.tn";
	        final String password = "sfaxsince1994";
	      
	 
	    	// Etape 1 : Création de la session
	    	        Properties props = new Properties();
	    	        props.put("mail.smtp.auth", "true");
	    	        props.put("mail.smtp.starttls.enable", "true");
	    	        props.put("mail.smtp.host", "smtp.gmail.com");
	    	        props.put("mail.smtp.port", "587");
	    	        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	    	            protected PasswordAuthentication getPasswordAuthentication() {
	    	                return new PasswordAuthentication(mail, password);
	    	            }
	    	        });
	    	        try {
	    	        	
	    	// Etape 2 : Création de l'objet Message
	    	            Message message = new MimeMessage(session);
	    	            message.setFrom(new InternetAddress("emna.abid@esprit.tn"));
	    	            message.setRecipients(Message.RecipientType.TO,
	    	                    InternetAddress.parse(ex));
	    	            message.setSubject("Close end date for some tasks");
	    	            message.setText("Hello, "
	    	                    + "Please check your taks. ");
	    	            
	    	// Etape 3 : Envoyer le message
	    	            Transport.send(message);
	    	            System.out.println("mail sent");
	    	        } catch (MessagingException e) {
	    	            throw new RuntimeException(e);
	    	        }
	    	    }

	    @FXML
	    void sendMail(ActionEvent event) throws NamingException {
	    	Context context = new InitialContext();
	    	TodoServiceRemote todoServiceRemote = (TodoServiceRemote) context.lookup(jndi);
	    	
	    	if (TaskTable.getSelectionModel().getSelectedIndex()<0) {
	    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        	alert.setTitle("Error Dialog");
	        	alert.setHeaderText("Task not selected");
	        	alert.setContentText("Please select a task");
	        	alert.showAndWait();
	    	}
	    	else {
	    		String l = TaskTable.getSelectionModel().getSelectedItem().getUser().getEmail();
	    		this.envoyer(l);
	    		 Alert alert = new Alert(Alert.AlertType.INFORMATION);
		        	alert.setTitle("Information Dialog");
		        	alert.setHeaderText(null);
		        	alert.setContentText("Mail Sent");
		        	alert.showAndWait();
	    }

	    }

	    @FXML
	    void prioritySelected(ActionEvent event) {

	    }
	    @FXML
	    void taskSelected(ActionEvent event) {

	    }
	   

	    @FXML
	    void Scrum(ActionEvent event) throws IOException{
	    	Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/Task.fxml")));
    		Stage stage = new Stage();
    		stage.setScene(scene);
    		stage.show();
	    }
	    @FXML
	    void CancelTask(ActionEvent event) {
	    	title.setText(null);
	    	Description.setText(null);
	    	endDate.setValue(null);
	    	PriorityCombobox.setValue(null);
	    	fileDirectory.setText(null);
	    	EmployeeComboBox.setValue(null);
	    	titleController.setText(null);
	    	descriptionController.setText(null);
	    }
	   
	    
	    @FXML
	    void SearchByTitle(KeyEvent event) {
	    	for (int i=0;i<TaskTable.getItems().size();i++){
				TaskTable.getItems().clear();
			}
			Context context;
			try {
				context = new InitialContext();
				TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(jndi1);
				TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
				TitleT.setPrefWidth(150); 
				EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
				EnddateT.setPrefWidth(175);
				StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
				StartdateT.setPrefWidth(175);
				employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
				employeeT.setPrefWidth(150);
				StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
				StatusT.setPrefWidth(150);
				DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
				DescirptionT.setPrefWidth(120);
				PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
				PriorityT.setPrefWidth(120);

				ObservableList<Task> l = FXCollections.observableArrayList(taskServiceRemote.getTasksByTitle(searchText.getText()));
				TaskTable.setItems(l);
			} catch (NamingException e) {
				e.printStackTrace();
			}
	    }
	    

	    public void display() {
	    	
	    
	    	
	    	
	    	
	    	
	    	try {
				 InitialContext ctx;
				TaskServiceRemote proxy;
				ctx = new InitialContext();
				proxy = (TaskServiceRemote) ctx.lookup("CRM-ear/CRM-ejb/TaskService!tn.esprit.CRM.services.TaskServiceRemote");


		
			
			
				TitleT.setCellValueFactory(new PropertyValueFactory<Task,String>("title"));
				TitleT.setPrefWidth(150); 
				EnddateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("end_date_task"));
				EnddateT.setPrefWidth(175);
				StartdateT.setCellValueFactory(new PropertyValueFactory<Task,Date> ("start_date_task"));
				StartdateT.setPrefWidth(175);
				employeeT.setCellValueFactory(new PropertyValueFactory<>("user"));
				employeeT.setPrefWidth(150);
				StatusT.setCellValueFactory(new PropertyValueFactory<Task,String> ("status_task"));
				StatusT.setPrefWidth(150);
				DescirptionT.setCellValueFactory(new PropertyValueFactory<Task,String> ("description_task"));
				DescirptionT.setPrefWidth(120);
				PriorityT.setCellValueFactory(new PropertyValueFactory<Task,String> ("priority"));
				PriorityT.setPrefWidth(120);

			ObservableList<Task> data = FXCollections.observableArrayList(proxy.FindAllTask());
			
			TaskTable.setItems(data);

		
	
			
		} catch (NamingException e1) {
			
		}
		}

	    @FXML
	    void refreshTask(ActionEvent event) {
	    	this.display();
	    	sortCB.setValue(null);
	    }
	    @FXML
	    void DeleteTask(ActionEvent event)  throws NamingException{
	    	if (TaskTable.getSelectionModel().getSelectedIndex()<0) {
	    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        	alert.setTitle("Error Dialog");
	        	alert.setHeaderText("Task not selected");
	        	alert.setContentText("Please select a task");
	        	alert.showAndWait();
	    	}
	    	else {
	    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    		alert.setTitle("Confirmation Dialog");
	    		alert.setHeaderText("Your are going to delete the selected task");
	    		alert.setContentText("Are you sure?");

	    		Optional<ButtonType> result = alert.showAndWait();
	    		if (result.get() == ButtonType.OK){
	    			Context context = new InitialContext();
	    	    	TaskServiceRemote taskServiceRemote =  (TaskServiceRemote) context.lookup(jndi1);
	    	    	Task task = TaskTable.getSelectionModel().getSelectedItem();
	    	    	taskServiceRemote.deleteTask(task);
	    	    	
	    	    	
	    	    	Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
	            	alert.setTitle("Information Dialog");
	            	alert.setHeaderText(null);
	            	alert.setContentText("Task deleted");
	            	alert.showAndWait();
	            	this.display();
	    		} else {
	    		    this.display();
	    		}
	    	}  
	    	
	    	
	    }
	    @FXML
	    void UpdateTask(ActionEvent event)throws IOException {

	    	if (TaskTable.getSelectionModel().getSelectedIndex()<0) {
	    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        	alert.setTitle("Error Dialog");
	        	alert.setHeaderText("Task not selected");
	        	alert.setContentText("Please select a task");
	        	alert.showAndWait();
	    	}
	    	else {
	    		this.task = TaskTable.getSelectionModel().getSelectedItem();
	        	Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/TaskUpdate.fxml")));
	    		Stage stage = new Stage();
	    		stage.setScene(scene);
	    		stage.show();
	    	}

	    }	
	    
	    
		@Override
		public void initialize(URL location, ResourceBundle resources) {
		
			List<String> users = new ArrayList<>();	
			try 
	  		{
	  			Context context = new InitialContext();
	  			TodoServiceRemote todoServiceRemote = (TodoServiceRemote) context.lookup(jndi);
	  			List<String> usersId =todoServiceRemote.findAll();
	  			for(String userss:usersId)
	  			{
	  				users.add(userss);
	  				ObservableList<String> items =FXCollections.observableArrayList(users);
	  				
	  				EmployeeComboBox.setItems(items);		
	  			}	
			
	  		} 
	  		catch (NamingException e) 
	  		{
	  			
	  			e.printStackTrace();
	  		}
			
			ObservableList<String> items1 = FXCollections.observableArrayList("Low",
			        "Meduim","Hard");
			PriorityCombobox.setItems(items1);
			ObservableList<String> items2 = FXCollections.observableArrayList("Priority",
			        "Status","End Date","Start Date");
			sortCB.setItems(items2);

	this.display();
	
		}
}
