package tn.esprit.CRM.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.services.AbonnementServiceRemote;
import tn.esprit.CRM.services.PackServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

public class AddProdCyrineController implements Initializable {

    @FXML
    private JFXComboBox<String> ProdCombo;
    @FXML
    private JFXButton ConfirmBtn;

    @FXML
    private JFXButton CloseBtn;
    private String jndi = "CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote";
   	private String jndi1 = "CRM-ear/CRM-ejb/ProductService!tn.esprit.CRM.services.ProductServiceRemote";
   	
   	
   	public static Pack pack;



    @FXML
    void AddProdCyrineAction(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
	PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
	ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
	PackAjoutController pac = new PackAjoutController ();
	Pack pack= pac.pack;
	 Integer p = pack.getId_pack();
	String lproduit = ProdCombo.getValue();
	Integer pp =productServiceRemote.findIdProductByTitleCyrine(lproduit);
	packServiceRemote.Addprod(p, pp);
	Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	alert.setTitle("Success!!");
	alert.setContentText("The product has been successfully added to the pack");
	alert.showAndWait();
	Stage stage = (Stage) ConfirmBtn.getScene().getWindow();
	
	stage.close();

    }

    @FXML
    void CloseAction(ActionEvent event) {
    	Stage stage = (Stage) CloseBtn.getScene().getWindow();
	stage.close();

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Context context;
		List<String> categories = new ArrayList<>();
		try {
			context = new InitialContext();
			
			List<String> categoryOldd = new ArrayList<>();
			PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
			ProductServiceRemote productServiceRemote = (ProductServiceRemote) context.lookup(jndi1);
			 categoryOldd = productServiceRemote.DisplayALlProductsNamesCyrine();
			 for (String category : categoryOldd) {
					categories.add(category);
					ObservableList<String> items = FXCollections.observableArrayList(categories);
					
					ProdCombo.setItems(items);
					
					
					
				}
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
