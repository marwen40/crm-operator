package tn.esprit.CRM.Controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.services.AbonnementServiceRemote;
import tn.esprit.CRM.services.PackServiceRemote;
import tn.esprit.CRM.services.ProductServiceRemote;

public class AddAbonCyrineController  implements Initializable {

    @FXML
    private JFXComboBox<String> AbonCombo;

    @FXML
    private JFXButton ConfirmBtn;

    @FXML
    private JFXButton CloseBtn;
    private String jndi = "CRM-ear/CRM-ejb/PackService!tn.esprit.CRM.services.PackServiceRemote";
   
	private String jndi2 ="CRM-ear/CRM-ejb/AbonnementService!tn.esprit.CRM.services.AbonnementServiceRemote";
	
	public static Pack pack;


    @FXML
    void AddAbon(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
    	PackAjoutController pac = new PackAjoutController ();
    	Pack pack= pac.pack;
    	 Integer p = pack.getId_pack();
    	 String lAb = AbonCombo.getValue();
    	 Integer ab =packServiceRemote.findIdAbonnemmentByTitle(lAb);
    	 packServiceRemote.Addabbon(p, ab);
    	 Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    		alert.setTitle("Success!!");
    		alert.setContentText("The Subscription has been successfully added to the pack");
    		alert.showAndWait();
    		Stage stage = (Stage) ConfirmBtn.getScene().getWindow();
    		
    		stage.close();

    }

    @FXML
    void CloseAction(ActionEvent event) {
    	Stage stage = (Stage) CloseBtn.getScene().getWindow();
	stage.close();

    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		List<String> categoriesab = new ArrayList<>();
		try {
			Context context = new InitialContext();
			List<String> categoryOlddAbb = new ArrayList<>();
			PackServiceRemote packServiceRemote = (PackServiceRemote) context.lookup(jndi);
		
			 AbonnementServiceRemote  abonnementServiceRemote = (AbonnementServiceRemote) context.lookup(jndi2);
			 categoryOlddAbb=packServiceRemote.DisplayALlAbonnementNames();
			 for (String categorys : categoryOlddAbb) {
				 categoriesab.add(categorys);
				 ObservableList<String> itemsAb = FXCollections.observableArrayList(categoriesab);
				 AbonCombo.setItems(itemsAb);
			 }

		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
