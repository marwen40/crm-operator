package tn.esprit.CRM.Controller;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;

import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFileChooser;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.stage.Stage;
import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.services.TaskServiceRemote;



public class TaskUpdateController implements Initializable {
	@FXML
    private Label taskTitle;

    @FXML
    private JFXComboBox<String> priorityCB;

    @FXML
    private JFXDatePicker startDate;

    @FXML
    private JFXDatePicker endDate;

    @FXML
    private JFXTextArea desciption;

    @FXML
    private Button addFileBtn;

    @FXML
    private JFXButton updateBtn;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private Label fileDirectory;
    public static Task task;
    private String jndi1 = "CRM-ear/CRM-ejb/TaskService!tn.esprit.CRM.services.TaskServiceRemote";
    String path = null;
    @FXML
    void CancelUpdate(ActionEvent event) {
    	Stage stage = (Stage) cancelBtn.getScene().getWindow();
		stage.close();
    }

    @FXML
    void addFile(ActionEvent event) {
    	final JFileChooser fc = new JFileChooser();
        fc.showOpenDialog(fc);
        File selectedFile = fc.getSelectedFile();
        String path = selectedFile.getAbsolutePath();
        this.path = path;
        fileDirectory.setText(path);
    }

    @FXML
    void updateTask(ActionEvent event) throws NamingException  {
     	TaskManagmentController tc = new TaskManagmentController();
    		Task task = tc.task;
        	Context context = new InitialContext();
    		TaskServiceRemote taskServiceRemote =  (TaskServiceRemote) context.lookup(jndi1);
    		task.setPriority(priorityCB.getValue());
    		task.setDescription_task(desciption.getText());
    		task.setDocument(path);
    		fileDirectory.setText(path);
    		task.setEnd_date_task(java.sql.Date.valueOf(endDate.getValue()));
    		task.setStart_date_task(java.sql.Date.valueOf(startDate.getValue()));
    		
    		taskServiceRemote.update(task);
    		
    		
    		Stage stage = (Stage) updateBtn.getScene().getWindow();
    		
    		stage.close();
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		updateBtn.setCursor(Cursor.HAND);
		cancelBtn.setCursor(Cursor.HAND);
		
		TaskManagmentController tc = new TaskManagmentController();
		Task task = tc.task;
		taskTitle.setText(task.getTitle());
		priorityCB.setValue(task.getPriority());
		desciption.setText(task.getDescription_task());
		Date start = task.getStart_date_task();
    	LocalDate date1 = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    	startDate.setValue(date1);
    	Date end = task.getEnd_date_task();
    	LocalDate date2 = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    	endDate.setValue(date2);
		path = task.getDocument();
		fileDirectory.setText(task.getDocument());
	}
}
