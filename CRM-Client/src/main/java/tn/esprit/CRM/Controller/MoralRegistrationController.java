package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import tn.esprit.CRM.persistence.Corporation;
import tn.esprit.CRM.services.UserServiceRemote;

public class MoralRegistrationController implements Initializable {

	private static final String JNDI_NAME = "CRM-ear/CRM-ejb/UserService!tn.esprit.CRM.services.UserServiceRemote";
	Corporation corporation = new Corporation();

	@FXML
	private JFXTextField firstName;

	@FXML
	private JFXTextField userName;

	@FXML
	private JFXPasswordField tf_password;

	@FXML
	private JFXTextField email;

	@FXML
	private JFXPasswordField tf_repeter;

	@FXML
	private JFXTextField tf_tel;

	@FXML
	private FontAwesomeIconView next1;

	@FXML
	private FontAwesomeIconView retour_first;

	@FXML
	private Label ver_nom;

	@FXML
	private Label ver_email;

	@FXML
	private Label ver_tel;

	@FXML
	private Label ver_mot;

	@FXML
	private Label ver_rep;

	@FXML
	private Label ver_user;
	@FXML
	private JFXTextField tf_address;

	@FXML
	private JFXTextField tf_employees;

	@FXML
	private JFXTextField tf_idFiscale;

	@FXML
	private FontAwesomeIconView retour;

	@FXML
	private FontAwesomeIconView retour1;

	@FXML
	private Label ver_employees;

	@FXML
	private Label ver_idFiscale;

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@FXML
	void CheckIdFiscale(KeyEvent event) {

	}

	@FXML
	void CheckNumberEmployees(KeyEvent event) {

	}

	@FXML
	void doRegistration(MouseEvent event) throws IOException {
		try {
			corporation.setAddress(tf_address.getText());
			corporation.setEmployeesNumber(Integer.parseInt(tf_employees.getText()));
			corporation.setId_fiscale(Integer.parseInt(tf_idFiscale.getText()));
			corporation.setRole_user("Moral");
			corporation.setType_client("Prospect");

			createUser();

			Parent loginView = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/LoginFXML.fxml"));
			MainFX.stage.getScene().setRoot(loginView);
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void CheckNom(KeyEvent event) {
		String regex = "^[a-zA-Z]+$";
		if (firstName.getText().matches(regex)) {
			ver_nom.setText("Correct ");
			ver_nom.setTextFill(Color.GREEN);
			ver_nom.setVisible(true);
		} else {
			ver_nom.setText("Ce n'est pas un nom!!");
			ver_nom.setTextFill(Color.RED);
			ver_nom.setVisible(true);
		}

	}

	@FXML
	void CheckPassword(KeyEvent event) {
		String pas = tf_password.getText();
		String confirmpas = tf_repeter.getText();

		if ((pas.length() > 0) && (confirmpas.length() > 0) && (pas.equals(confirmpas))) {

			ver_rep.setText("Passwords Identique");
			ver_rep.setTextFill(Color.GREEN);
			ver_rep.setVisible(true);

		} else {
			ver_rep.setText("Passwords Non Identique");
			ver_rep.setTextFill(Color.RED);
			ver_rep.setVisible(true);
		}
	}

	@FXML
	void CheckPhonenumber(KeyEvent event) {
		if ((tf_tel.getText().length() == 8) && (IsNumber(tf_tel.getText()))) {
			ver_tel.setText("Not a PhoneNumber");
			ver_tel.setTextFill(Color.GREEN);
			ver_tel.setVisible(true);
		} else {
			ver_tel.setText("Correct PhoneNumber");
			ver_tel.setTextFill(Color.RED);
			ver_tel.setVisible(true);
		}
	}

	public boolean IsNumber(String x) {
		boolean verif = true;
		try {
			Float.parseFloat(x);
		} catch (NumberFormatException e) {
			verif = false;
		}
		return verif;
	}

	@FXML
	void CheckStength(KeyEvent event) {

		String passs = tf_password.getText();
		boolean lengthRule = passs.length() >= 8 && passs.length() <= 50;

		boolean upperRule = !passs.equals(passs.toLowerCase());
		boolean lowerRule = !passs.equals(passs.toUpperCase());
		boolean numeralRule = passs.matches("[0-9]");
		boolean nonAlphaRule = passs.matches("(.*)[^A-Za-z0-9](.*)");
		int ruleCount = (upperRule ? 1 : 0) + (lowerRule ? 1 : 0) + (numeralRule ? 1 : 0) + (nonAlphaRule ? 1 : 0);
		if (ruleCount >= 3 && lengthRule) {
			ver_mot.setText("Strong password");
			ver_mot.setTextFill(Color.GREEN);
			ver_mot.setVisible(true);
		} else {

			if (ruleCount < 3 || !lengthRule) {
				ver_mot.setText("Weak password!");
				ver_mot.setTextFill(Color.GREEN);
				ver_mot.setVisible(true);
			} else {
				ver_mot.setText("Rpeter!");
				ver_mot.setTextFill(Color.RED);
				ver_mot.setVisible(true);
			}
		}
	}

	@FXML
	void CheckUser(KeyEvent event) throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);
		String regex = "^[a-zA-Z0-9.\\-_]+$";
		if (userName.getText().matches(regex)) {
			ver_user.setText("C'est possible");
			ver_user.setTextFill(Color.GREEN);
			ver_user.setVisible(true);
		}

		else {
			if (proxy.findUserByUserName(userName.getText()).equals(userName)) {
				ver_user.setText("Exist");
				ver_user.setTextFill(Color.RED);
				ver_user.setVisible(true);
				return;

			}

			else {
				ver_user.setText("Verifie user_name!!");
				ver_user.setTextFill(Color.BLUE);
				ver_user.setVisible(true);
			}
		}
	}

	@FXML
	void Verif_mail(KeyEvent event) throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);
		if (proxy.findUserByEmail(email.getText()).getId_user() != null) {
			ver_email.setText("mail exists");
			ver_email.setTextFill(Color.RED);
			ver_email.setVisible(true);
			return;
		}

		else {
			if (validateEmail(email.getText())) {
				ver_email.setText("email Correct!");
				ver_email.setTextFill(Color.GREEN);
				ver_email.setVisible(true);
			}

			else {
				ver_email.setText("Mail incorrect!");
				ver_email.setTextFill(Color.RED);
				ver_email.setVisible(true);
			}
		}

	}

	public static boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	@FXML
	void back_to_menu(MouseEvent event) throws IOException {

		Parent root = FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/LoginFXML.fxml"));
		MainFX.stage.getScene().setRoot(root);
	}

	@FXML
	void close(MouseEvent event) {
		System.exit(0);

	}

	@FXML
	void goToRegistrationStepThree(MouseEvent event) throws IOException {
		corporation.setFirst_Name(firstName.getText());
		corporation.setUser_Name(userName.getText());
		corporation.setPassword(tf_password.getText());
		corporation.setEmail(email.getText());

		// change view
		Parent thirdView = FXMLLoader
				.load(getClass().getResource("/tn/esprit/CRM/gui/ThirdMoralRegistrationFXML.fxml"));
		MainFX.stage.getScene().setRoot(thirdView);
	}

	private void createUser() throws NamingException {
		Context ctx = new InitialContext();
		UserServiceRemote proxy = (UserServiceRemote) ctx.lookup(JNDI_NAME);

		proxy.createUser(corporation);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
}