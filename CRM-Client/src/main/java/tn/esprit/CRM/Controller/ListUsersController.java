package tn.esprit.CRM.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.UserServiceRemote;

public class ListUsersController implements Initializable {

	private static final String JNDI_NAME = "CRM-ear/CRM-ejb/UserService!tn.esprit.CRM.services.UserServiceRemote";

	@FXML
	private VBox pnItems;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadUserGrid();
	}

	@FXML
	void close(MouseEvent event) {
	}

	@FXML
	void close_app(MouseEvent event) {
		System.exit(0);
	}

	private void loadUserGrid() {
		List<User> users = fetchUsers();
		Node[] nodes = new Node[users.size()];

		AtomicInteger i = new AtomicInteger(0);
		users.forEach(user -> {
			int j = i.getAndIncrement();
			Node node = nodes[j] = loadNewItemNode();

			displaUserDetails(node, user);

			setupActions(node, user, j);

			setHoverStyleForNode(nodes, j);

			pnItems.getChildren().add(node);
		});

		if (nodes.length > 0) {
			pnItems.setStyle("-fx-background-color : #53639F");
			pnItems.toFront();
		}
	}

	/*
	 * private List<User> fetchUsers() { try { return
	 * getUserServiceRemote().findAll(); } catch (RuntimeException e) {
	 * e.printStackTrace(); }
	 * 
	 * return Collections.emptyList(); }
	 * 
	 * private Node loadNewItemNode() { try { return
	 * FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/Item.fxml"));
	 * } catch (IOException e) { throw new RuntimeException(); } }
	 */
	private List<User> fetchUsers() {
		try {
			return getUserServiceRemote().findAllClients("Physique", "Moral");
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		return Collections.emptyList();
	}

	private Node loadNewItemNode() {
		try {
			return FXMLLoader.load(getClass().getResource("/tn/esprit/CRM/gui/Item.fxml"));
		} catch (IOException e) {
			throw new RuntimeException();
		}
	}

	private void displaUserDetails(Node node, User user) {
		Label item_userName = (Label) node.lookup(".item_userName");
		item_userName.setText(user.getFirst_Name());
		Label item_telefone = (Label) node.lookup(".item_telefone");
		item_telefone.setText(user.getPhone_number().toString());
		Label item_role = (Label) node.lookup(".item_role");
		item_role.setText(user.getRole_user());
		// other properties
		// ...
	}

	private void setupActions(Node node, User user, int index) {
		Button deleteButton = (Button) node.lookup(".item_action_delete");
		deleteButton.setOnMouseClicked(deleteEventHandler(user.getId_user(), index));
	}

	private void setHoverStyleForNode(Node[] nodes, int i) {
		final int j = i;
		nodes[i].setOnMouseEntered(even -> {
			nodes[j].setStyle("-fx-background-color : #0A0E3F");
		});
		nodes[i].setOnMouseExited(even -> {
			nodes[j].setStyle("-fx-background-color : #02030A");
		});
	}

	private EventHandler<MouseEvent> deleteEventHandler(Integer id_user, int index) {
		return event -> {
			getUserServiceRemote().deleteUser(id_user);

			pnItems.getChildren().remove(index);
		};
	}

	private UserServiceRemote getUserServiceRemote() {
		Context context;
		try {
			context = new InitialContext();
			return (UserServiceRemote) context.lookup(JNDI_NAME);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
