package tn.esprit.CRM.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import tn.esprit.CRM.Controller.MainFX;
import tn.esprit.CRM.persistence.Staff;
import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.StaffServiceRemote;

public class ProfilStaffController implements Initializable{
	@FXML
	private Label fname;
	@FXML
	private Label lname;
	@FXML
	private Label salary;
	@FXML
	private Label holiday;
	@FXML
	private Label username;
	@FXML
	private Label cin;
	@FXML
	private Label number;
	@FXML
	private Button conge;

	// Event Listener on Button[#conge].onAction
	@FXML
	public void conge(ActionEvent event) {
		// TODO Autogenerated
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			StaffServiceRemote proxy;

			proxy = (StaffServiceRemote) ctx.lookup("CRM-ear/CRM-ejb/StaffService!tn.esprit.CRM.services.StaffServiceRemote");
			Staff s=proxy.FindStaffByCin(1111);

			fname.setText(s.getFirst_Name());
			 lname.setText(s.getLast_Name());
			 salary.setText(String.valueOf(s.getSalary_staff()));
			 holiday.setText(String.valueOf((s.getNbjrscp())));
			 username.setText(String.valueOf(s.getUser_Name()));
			 cin.setText(String.valueOf(s.getCIN()));
			 number.setText(String.valueOf(s.getPhone_number()));
		
			
			
		} catch (NamingException e1) {
		
		} 
		
	}
}
