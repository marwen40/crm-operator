import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.BeforeClass;
import org.junit.Test;

import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.UserServiceRemote;

public class UserServiceTest {

	private static UserServiceRemote userserviceremote;

	@BeforeClass
	public static void setup() throws NamingException {
		Context context = new InitialContext();
		String jndi = "CRM-ear/CRM-ejb/UserService!tn.esprit.CRM.services.UserServiceRemote";
		userserviceremote = (UserServiceRemote) context.lookup(jndi);
	}
	
	@Test
	public void free(){
		List<User> users = new ArrayList<>();
		users = userserviceremote.findAllClients("Physique", "Moral");
		for(User u:users){
			System.out.println(u.getRole_user());
		}
	}
	
	/*@Test
	public void TesteUserName(){
		System.out.println(userserviceremote.findUserByUserName("islam").getId_user());
		System.out.println(userserviceremote.findUserByCIN(10).getId_user());
		System.out.println(userserviceremote.findUserByEmail("ok").getEmail());
	}*/
	/*
	@Test
	public void should_create_user() {
		String firstName = "marwen";
		String lastName = "saidi";

		long oldUserCount = countUserByFirstAndLastName(firstName, lastName);
		createUser(firstName, lastName);
		long addedUserCount = countUserByFirstAndLastName(firstName, lastName);

		Assert.assertEquals(addedUserCount - oldUserCount, 1);
	}
	
	@Test
	public void should_create_admin_user() {
		String firstName = "adminFirstName";
		String lastName = "addminLastName";

		long oldUserCount = countUserByFirstAndLastName(firstName, lastName);
		createAdminUser(firstName, lastName);
		long addedUserCount = countUserByFirstAndLastName(firstName, lastName);

		Assert.assertEquals(addedUserCount - oldUserCount, 1);
	}
	
	@Test
	public void should_delete_user() {
		String firstName = "deleteme";
		String lastName = "deleteme";

		long oldUserCount = countUserByFirstAndLastName(firstName, lastName);
		createUser(firstName, lastName);
		long addedUserCount = countUserByFirstAndLastName(firstName, lastName);	
		Assert.assertEquals(addedUserCount - oldUserCount, 1);
			
		deleteUser(firstName, lastName);
		long finalUserCount = countUserByFirstAndLastName(firstName, lastName);	
		Assert.assertEquals(oldUserCount, finalUserCount);
	}
	
	
	/** PRIVATE METHOD GOES HERE */
	/*
	private void deleteUser(String firstName, String lastName) {
		User user = findUser(firstName, lastName);
		removeUser(user.getId_user());
	}
	
	private void removeUser(Integer id_user) {
		userserviceremote.deleteUser(id_user);
	}
	
	private User findUser(String firstName, String lastName) {
		List<User> users = userserviceremote.findAll();
		return users.stream()
				.filter(user -> userWithFirstAndLastName(user, firstName, lastName))
				.findFirst()
				.orElseGet(() -> null);
	}

	private void createUser(String firstName, String lastName) {
		User u = new User();
		u.setFirst_Name(firstName);
		u.setLast_Name(lastName);
		u.setAddress("ariana");
		u.setCIN(1);
		
		userserviceremote.createUser(u);
	}

	private void createAdminUser(String firstName, String lastName) {
		Admin a = new Admin();
		a.setFirst_Name(firstName);
		a.setLast_Name(lastName);
		a.setAdmin_type(Role.Commercial_Director);
		
		userserviceremote.createUser(a);
	}

	private long countUserByFirstAndLastName(String firstName, String lastName) {
		List<User> users = userserviceremote.findAll();
		return users.stream()
				.filter(user -> userWithFirstAndLastName(user, firstName, lastName))
				.count();
	}
	
	private boolean userWithFirstAndLastName(User user, String firstName, String lastName) {
		return user.getFirst_Name() != null 
				&& user.getFirst_Name().equals(firstName)
				&& user.getLast_Name() != null
				&& user.getLast_Name().equals(lastName);
	}
	*/
}
