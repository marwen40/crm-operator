package tn.esprit.CRM.services;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import tn.esprit.CRM.persistence.Message;



/**
 * Simple chat logic
 
 */
@Singleton
@Startup
public class MessageManager implements MessageManagerLocal {
 
    private final List<Message> messages = Collections.synchronizedList(new LinkedList());

	@Override
	public Message getFirstAfter(Date lastUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sendMessage(Message message) {
		  messages.add(message);
		  message.setDateSent(new Date());
	};
 
   
	}
 
