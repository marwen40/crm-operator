package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Proposition;

@Local
public interface PropServiceLocal {

	public void Ajouter(Proposition p);
	public List<Proposition> Afficher();
}
