package tn.esprit.CRM.services;




import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Task;






@Remote
public interface TaskServiceRemote {
	Integer createTask(Task task);
	void deleteTask(Task task);
	void updateTask(String title, int taskId);
	
	 List<Task> FindAllTask();
	 List<String> FindAllTaskNames();
	 int FindTaskByTitle (String title);
	 int FindIdTaskByUser (int id);
	 Long findTotalStatus(String ToDo);
	 Task findTaskByTitle(String title);
	 Task update(Task task);
	 List<Task> findTaskByStatus(String status);
	 List<Task> getTasksByTitle(String title);
	 List<Task> getTasksByUser(int id);
	 List<Task> getTaskByStatus(String status);
	 List<Task> sortTaskByPriority() ;
	 List<Task> sortTaskByStatus() ;
	 List<Task> sortTaskByEndDate() ;
	 List<Task> sortTaskByStartDate() ;
	 Task find(int entityID);



}
