package tn.esprit.CRM.services;



import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import tn.esprit.CRM.persistence.Task;








@Stateless
@LocalBean
public class TaskService  implements TaskServiceLocal, TaskServiceRemote {
	
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;

	@PersistenceContext
	private EntityManager entityManager;
	
	public TaskService() {
		

	}	
	@Override
	public Integer createTask(Task task) {
		em.persist(task);
		return task.getId_task();

	}	
	@Override
	public void deleteTask(Task task) {
		Task entityToBeRemoved = em.merge(task);

		em.remove(entityToBeRemoved);
	}

	public void updateTask(String title, int taskId) { 
		Task task = em.find(Task.class,taskId);
		task.setTitle(title);
	
	}
	
	@Override
	public Task update(Task task) {
		return em.merge(task);
	}
	
	
	public int FindTaskByTitle (String title)
	{
		String req = "SELECT p.id_task FROM Task p WHERE p.title like :title";
		return (int) em.createQuery(req).setParameter("title", title).getSingleResult();
	}
	
	
	
	public int FindIdTaskByUser (int id)
	{
		
		String req = "SELECT p.id_task FROM Task p WHERE p.user_id_user like :id";
		return (int) em.createQuery(req).setParameter("user_id_user", id).getSingleResult();
	}
	
	public List<String> findAllTitles() {
		String req ="SELECT u.user_Name from User u ";
		return em.createQuery(req).getResultList();	
		}
	
	public List<String> FindAllTaskNames() {
		String req ="SELECT u from Task u ";
		return em.createQuery(req).getResultList();	
		}

	public List<Task> FindAllTask() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Task.class));
		return em.createQuery(cq).getResultList();
		
	}
	
	@Override
	public Long findTotalStatus(String ToDo) {
		String req = "SELECT count(p) FROM Task p WHERE p.status_task like :ToDo";
		return (Long) em.createQuery(req).setParameter("status_task", ToDo).getSingleResult();
			}
	@Override
	public Task findTaskByTitle(String title) {
		String req = "SELECT t FROM Task t WHERE t.title like :title ";
		return (Task) entityManager.createQuery(req).setParameter("title", title).getSingleResult();
	}
	
	
	@Override
	public List<Task> findTaskByStatus(String status) {
		String req = "SELECT p from Task p WHERE p.status_task like :status";
		return em.createQuery(req).setParameter("status", status).getResultList();
	}
	
	@Override
	public List<Task> getTasksByTitle(String title) {
		String jpql = "SELECT t FROM Task t WHERE t.title like :title";
		return entityManager.createQuery(jpql).setParameter("title", title+"%").getResultList();

	}
	@Override
	public List<Task> getTasksByUser(int id) {
		String jpql = "SELECT t FROM Task t WHERE t.user_id_user like :id";
		return entityManager.createQuery(jpql).setParameter("user_id_user", id).getResultList();
		
	}
	
	@Override
	public List<Task> getTaskByStatus(String status_task) {
		String jpql = "SELECT A FROM Task A WHERE A.status_task like :status_task";
		return entityManager.createQuery(jpql).setParameter("status_task", status_task+"%").getResultList();
	}
	@Override
	public List<Task> sortTaskByPriority() {
		String req = "SELECT t FROM Task t ORDER BY t.priority"; 
		return entityManager.createQuery(req).getResultList();
	}
	@Override
	public List<Task> sortTaskByStatus() {
		String req = "SELECT t FROM Task t ORDER BY t.status_task"; 
		return entityManager.createQuery(req).getResultList();
	}
	@Override
	public List<Task> sortTaskByEndDate() {
		String req = "SELECT t FROM Task t ORDER BY t.end_date_task"; 
		return entityManager.createQuery(req).getResultList();
	}
	@Override
	public List<Task> sortTaskByStartDate() {
		String req = "SELECT t FROM Task t ORDER BY t.start_date_task"; 
		return entityManager.createQuery(req).getResultList();
	}
	public Task find(int entityID) {
		return em.find(Task.class, entityID);
	}
}
