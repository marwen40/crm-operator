package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.persistence.Product;

@Remote

public interface OffreServiceRemote {
	public List <Offre> findalladmin();
	public List<Offre> findallclient();
	public Integer createOffre(Offre offre);
	public Offre update(Offre offre);
	public void deleteOffre(Offre offre);
	public Offre findOffreByTitle(String title) ;
	public void updateOffretitle(String title, int id_offre );
	public void invisible(Offre offre);
	public void visible(Offre offre);
	public void visibdate(Offre offre);
	public List<String> findproductlibre();
	public List<String> findprocutre9ed();
	public List<Offre> getOffersByTitle(String title);
}
