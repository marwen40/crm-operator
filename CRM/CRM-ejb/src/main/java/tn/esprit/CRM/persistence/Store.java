package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Store
 *
 */

@Entity
@Stateful
public class Store implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Integer id_store;
	private String address_store;
	private Integer employee_number;
	private String nom_Store;
	private Integer quantite_prod;
	private Integer id_prod;
	private String governorate;
	private String email;
	
	@OneToMany(mappedBy="store",cascade=CascadeType.REMOVE)
	private List<StoreProduit> storeProduits;

	private static final long serialVersionUID = 1L;


	@OneToOne(mappedBy="store",cascade = {CascadeType.PERSIST, CascadeType.REMOVE } ,fetch=FetchType.EAGER)
	private Localisation localisation;
	

	public Store() {
		super();
	}



	public Store(String address_store, Integer employee_number, String nom_Store, String governorate) {
		super();
		this.address_store = address_store;
		this.employee_number = employee_number;
		this.nom_Store = nom_Store;
		this.governorate = governorate;
	}



	public Integer getId_store() {
		return this.id_store;
	}

	public void setId_store(Integer id_store) {
		this.id_store = id_store;
	}   
	public String getAddress_store() {
		return this.address_store;
	}

	public void setAddress_store(String address_store) {
		this.address_store = address_store;
	}   
	public Integer getEmployee_number() {
		return this.employee_number;
	}

	public void setEmployee_number(Integer employee_number) 
	{
		this.employee_number = employee_number;
	}


	public String getNom_Store() {
		return nom_Store;
	}

	public void setNom_Store(String nom_Store) {
		this.nom_Store = nom_Store;
	}

	
	
	
	public Integer getQuantite_prod() {
		return quantite_prod;
	}

	public void setQuantite_prod(Integer quantite_prod) {
		this.quantite_prod = quantite_prod;
	}

	public Integer getId_prod() {
		return id_prod;
	}

	public void setId_prod(Integer id_prod) {
		this.id_prod = id_prod;
	}


	public String getGovernorate() {
		return governorate;
	}

	public void setGovernorate(String governorate) {
		this.governorate = governorate;
	}



	@Override
	public String toString() {
		return nom_Store;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}

	


   
	
}
