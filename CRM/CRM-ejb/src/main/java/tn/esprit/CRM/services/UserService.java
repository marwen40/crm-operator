
package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.CRM.persistence.User;

@Stateless
public class UserService implements UserServiceLocal, UserServiceRemote {

	@PersistenceContext(unitName = "CRM-ejb")
	private EntityManager em;

	@Override
	public void createUser(User u) {
		em.persist(u);
	}

	@Override
	public void deleteUser(Integer id_user) {
		User user = em.find(User.class, id_user);
		em.remove(user);
	}

	@Override
	public void updateUser(User user) {
		em.merge(user);
	}

	@Override
	public List<User> findAll() {
		return em.createQuery("SELECT c FROM User c ", User.class).getResultList();
	}

	@Override
	public User findUserByUserName(String username) {
		return em.createQuery("select u from User u where u.user_Name=?1", User.class).setParameter(1, username)
				.getSingleResult();
	}

	@Override
	public User findUserByCIN(Integer cin) {
		return em.createQuery("select u from User u where u.CIN=?1", User.class).setParameter(1, cin).getSingleResult();
	}

	@Override
	public User findUserByEmail(String email) {
		return em.createQuery("select u from User u where u.email=?1", User.class).setParameter(1, email)
				.getSingleResult();
	}

	@Override
	public User login(String username, String password) {
		User user = null;
		try {
			user = em.createQuery("SELECT u FROM User u WHERE u.user_Name=:l AND u.password=:p", User.class)
					.setParameter("l", username).setParameter("p", password).getSingleResult();
		} catch (Exception e) {
		}
		return user;
	}

	@Override
	public User findUserById(int id) {
		return em.find(User.class, id);
	}

	@Override
	public List<User> findAllClients(String role_physique,String role_moral) {
		return em.createQuery("SELECT u FROM User u WHERE u.role_user=:l OR u.role_user=:p", User.class)
				.setParameter("l", role_physique).setParameter("p", role_moral).getResultList();
	}
}
