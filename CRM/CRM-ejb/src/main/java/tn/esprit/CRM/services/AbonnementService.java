package tn.esprit.CRM.services;



import java.util.List;



import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.CRM.persistence.Abonnement;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Todo;




@Stateful
public class AbonnementService implements AbonnementServiceRemote, AbonnementServiceLocal {

	

	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;

	public AbonnementService() {
		super();
	}
	@Override
	public void create(Abonnement abonnement) {
		System.out.println("enter");
		em.persist(abonnement);
	}
	public List<Todo> findAll() {
		return em.createQuery("from Todo", Todo.class).getResultList();
	}
	@Override
	public void deleteAbonnement(Abonnement abonnement) {

		em.remove(em.merge(abonnement));
	}
	@Override
	public Abonnement findAbonnementById(int idAbonnement) {
		return em.find(Abonnement.class, idAbonnement);
	}
	

	@Override
	public void updateAbonnement(Abonnement abonnement) {

		em.merge(abonnement);
	}

	@Override
	public List<Abonnement> getAllAbonnement() {
		return em.createQuery("select u from Abonnement u", Abonnement.class).getResultList();
	}
	@Override
	public Abonnement findAbonnementByProduct(Product produit) {
		return em.createQuery("select u from Abonnement u where u.produit=?1", Abonnement.class).setParameter(1, produit)
				.getSingleResult();
	}
	//public Abonnement findAbonnementByIdProduct(int id_product){
	//	TypedQuery<Float> query = em .createQuery("select u from Abonnement where u.id )
//	}
}
