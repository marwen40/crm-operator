package tn.esprit.CRM.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Events;
import tn.esprit.CRM.util.IGenericDAO;

@Local
public interface EventServiceLocal extends IGenericDAO<Events> {

	List<Events> findEventByName(String name);
	List<Events> findEventByDate(Date date);
	List<Events> sortByLocalisation(String localisation);
	List<Events> sortBySubject(String subject);
	List<Events> sortEventByDate();
	
}
