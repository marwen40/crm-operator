package tn.esprit.CRM.services;


import java.util.List;
//import java.util.Locale.Category;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Product;

@Remote
public interface ProductServiceRemote {
	
	Integer createProduct(Product p);
	void deleteProduct(Product produit);
	Product findProductById(int idp);
	public Product updateProduct(Product produit);
	public List<Product> findAll();
	//List<Product> findProductByCategory(String description);
	Integer retrunCategoryID(String name_product);
	public List<Product> getProductByName(String title);
	List<String> DisplayALlProductsNames();

////Methode sirine lil offre wel pack
	public List<String> DisplayALlProductsNamesCyrine();
	public int findIdProductByTitleCyrine(String name_product) ;
	public void assignProductToOfferCyrine(int id_offre, int id_product );
	public Product FindProductByTitleCyrine(String name_product) ;
	
}
