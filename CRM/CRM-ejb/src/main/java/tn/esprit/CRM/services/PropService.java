package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.CRM.persistence.Proposition;

@Stateless
@LocalBean
public class PropService implements PropServiceRemote,PropServiceLocal{
	@PersistenceContext
	private EntityManager entityManager;
	
	public PropService() {
		
	}

	@Override
	public void Ajouter(Proposition p) {
		entityManager.persist(p);
		
	}

	@Override
	public List<Proposition> Afficher() {
		return entityManager.createQuery("SELECT c FROM proposition c", Proposition.class).getResultList();
	}

}
