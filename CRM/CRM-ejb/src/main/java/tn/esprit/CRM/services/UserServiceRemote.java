package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.User;

@Remote
public interface UserServiceRemote {
	
	void createUser (User u);
	void deleteUser (Integer id_user);
	public void updateUser(User user);
	public List<User> findAll();
	User findUserByUserName(String username);
	User findUserByCIN(Integer cin);
	User findUserByEmail(String username);
	User findUserById(int id);
	public User login(String username, String password);
	public List<User> findAllClients(String role_user,String role_moral);

}
