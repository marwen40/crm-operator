package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;
import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.persistence.Events;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_user;
	private String first_Name;
	private String last_Name;
	private String user_Name;
	private String password;
	private String email;
	private Integer phone_number;
	private String address;
	private Integer CIN;
	private Date account_Date;
	private String Image_user;
	private String role_user;

	private static final long serialVersionUID = 1L;

	public User(Integer id_user, String first_Name, String last_Name, String user_Name, String password, String email,
			Integer phone_number, String address, Integer cIN, Date account_Date, Date modified_Date,
			Integer state_Account) {
		super();
		this.id_user = id_user;
		this.first_Name = first_Name;
		this.last_Name = last_Name;
		this.user_Name = user_Name;
		this.password = password;
		this.email = email;
		this.phone_number = phone_number;
		this.address = address;
		CIN = cIN;
		this.account_Date = account_Date;

	}

	@OneToMany(mappedBy="user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE } ,fetch=FetchType.LAZY)
	private List<Task> tasks;
	@OneToMany(mappedBy = "employee", cascade= CascadeType.MERGE)
	private List<Events> events;
	@OneToOne
	private Shopping_List shop;

	@OneToMany(mappedBy = "user2")
	private List<Quotation> quotations;

	@ManyToMany
	private List<Forum> forums;

	@OneToMany(mappedBy = "user3")
	private List<Proposition> propositions;

	@OneToMany(mappedBy = "user4")
	private List<Answer> answers;

	@OneToOne
	private Bank_Account bank_account;

	@ManyToMany
	private List<Calendar> calendars;

	@ManyToMany
	private List<Product> products;


	public User(String first_Name, String user_Name) {
		super();
		this.first_Name = first_Name;
		this.user_Name = user_Name;
	}

	public List<Calendar> getCalendar() {
		return calendars;
	}

	public void setCalendar(List<Calendar> calendars) {
		this.calendars = calendars;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public Bank_Account getBank_account() {
		return bank_account;
	}

	public void setBank_account(Bank_Account bank_account) {
		this.bank_account = bank_account;
	}

	public List<Proposition> getPropositions() {
		return propositions;
	}

	public void setPropositions(List<Proposition> propositions) {
		this.propositions = propositions;
	}

	public List<Forum> getForums() {
		return forums;
	}

	public void setForums(List<Forum> forums) {
		this.forums = forums;
	}

	public List<Quotation> getQuotations() {
		return quotations;
	}

	public void setQuotations(List<Quotation> quotations) {
		this.quotations = quotations;
	}

	public Shopping_List getShop() {
		return shop;
	}

	public void setShop(Shopping_List shop) {
		this.shop = shop;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public User() {
		super();
	}

	public Integer getId_user() {
		return this.id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public String getFirst_Name() {
		return this.first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getLast_Name() {
		return this.last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public String getUser_Name() {
		return this.user_Name;
	}

	public void setUser_Name(String user_Name) {
		this.user_Name = user_Name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPhone_number() {
		return this.phone_number;
	}

	public void setPhone_number(Integer phone_number) {
		this.phone_number = phone_number;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCIN() {
		return this.CIN;
	}

	public void setCIN(Integer CIN) {
		this.CIN = CIN;
	}

	public Date getAccount_Date() {
		return this.account_Date;
	}

	public void setAccount_Date(Date account_Date) {
		this.account_Date = account_Date;
	}

	public String getImage_user() {
		return Image_user;
	}

	public void setImage_user(String image_user) {
		Image_user = image_user;
	}


	public String getRole_user() {
		return role_user;
	}

	public void setRole_user(String role_user) {
		this.role_user = role_user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user_Name == null) ? 0 : user_Name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (user_Name == null) {
			if (other.user_Name != null)
				return false;
		} else if (!user_Name.equals(other.user_Name))
			return false;
		return true;
	}
	public User(String user_Name) {
		super();
		this.user_Name = user_Name;
	}
	@Override
	public String toString() {
		return  user_Name ;
	}
	public void linkEventToThisEmployee(List<Events> events) {
		this.events = events;
		for (Events e : events) {
			e.setEmployee(this);
		}
	}
	
}
