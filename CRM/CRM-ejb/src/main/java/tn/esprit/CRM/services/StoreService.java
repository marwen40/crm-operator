package tn.esprit.CRM.services;


import java.util.ArrayList;
import java.util.List;



import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.CRM.persistence.Category;
import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Store;
import tn.esprit.CRM.persistence.StoreProduit;




@Stateful
public class StoreService implements StoreServiceLocal, StoreServiceRemote {

	
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;
	

	public StoreService() {
		
	}

	///////////////*************** CREATE **************//////////////////////////

	 public Integer createStore(Store s) {
		em.persist(s);
		return (s.getId_store());
		
	}
	///////////////*************** DELETE **************//////////////////////////

	@Override
	public void deleteStore(Store store) {
		em.remove(em.merge(store));

		
		
	}
	
	///////////////*************** FIND STORE BY ID **************//////////////////////////


	@Override
	public Store findStoreById(int ids) {
		return em.find(Store.class, ids);
	}
	///////////////*************** FIND STORE BY ADDRESS **************//////////////////////////

	@Override
	public Store findStoreByAddress(String address_store) {

		String req = "SELECT s FROM Store WHERE s.address_store like :address_store";
		return (Store) em.createQuery(req).setParameter("address_store", address_store).getSingleResult();
	}

	///////////////*************** DISPLAY ALL STORES **************//////////////////////////

	@Override
	public List<Store> findAllStores() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Store.class));
		return em.createQuery(cq).getResultList();


	}
	
	///////////////*************** UPDATE  **************//////////////////////////


	@Override
	public Store updateStore(Store store) {
		return em.merge(store);
	
		
	}

	///////////////*************** DISPLAY ALL STORES NAMES **************//////////////////////////

	@Override
	public List<String> DisplayALlStoresNames() {
	String req = "SELECT s.nom_Store FROM Store s";
	return em.createQuery(req).getResultList();
	}

	@Override
	public Store findStoreByName(String nom_Store) {
		String req = "SELECT c FROM Store c WHERE c.nom_Store like :nom_Store";
		return(Store) em.createQuery(req).setParameter("nom_Store", nom_Store).getSingleResult();
	}

	
	//*****************************Assign product to store**************************************//
	@Override
	public void affectProductStore(Product product, Store store, int quantity) 
	{
		StoreProduit storeProduit = searchStoreProduit(store,product);
		if(storeProduit!= null){
			storeProduit.setQuantity(storeProduit.getQuantity()+quantity);
			em.merge(storeProduit);
		}else{
			storeProduit = new StoreProduit();
			storeProduit.setProduct(product);
			storeProduit.setStore(store);
			storeProduit.setQuantity(quantity);
			em.persist(storeProduit);
		}
	   
	}
	
	public StoreProduit searchStoreProduit(Store store,Product product){
		
		for(StoreProduit sp : getAllStoreProduct()){
			if(sp.getProduct().getId_product() == product.getId_product() && sp.getStore().getId_store() == store.getId_store()){
				return sp;
			}
		}
			
		return null;

	}
	
	//*****************************************Lister l'affectation des produits aux différents stores****************************************//

	@Override
	public List<StoreProduit> getAllStoreProduct() {
		String req = "SELECT s FROM StoreProduit s";
		return em.createQuery(req).getResultList();
	}

	@Override
	public List<StoreProduit> getStoreProductByStore(Store store) {
		List<StoreProduit> list = new ArrayList<>();
		for(StoreProduit sp : getAllStoreProduct()){
			if(sp.getStore().getId_store() == store.getId_store()){
				list.add(sp);
			}
		}
		return list;
	}


}
