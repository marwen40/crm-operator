package tn.esprit.CRM.services;


import java.util.List;

import javax.ejb.Stateful;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.CRM.persistence.Category;
import tn.esprit.CRM.persistence.Product;

@Stateful
public class CategoryService implements CategoryServiceLocal, CategoryServiceRemote {

		
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;
	
	
	public CategoryService() {
		super();
	}
	
	///////////////*************** CREATE **************//////////////////////////

	@Override
	public void createCategory(Category c) {
	em.persist(c);		
	}
	
	///////////////*************** DELETE **************//////////////////////////

	@Override
	public void deleteCategory(int idc) {
		//em.remove(em.merge(c));
		Category category = em.find(Category.class, idc);
		em.remove(category);

	}
	
	///////////////*************** FIND CATEGORY BY ID**************//////////////////////////

	@Override
	public Category findCategoryById(int idc) {
	
		return em.find(Category.class, idc);
	}
	
	///////////////*************** UPDATE **************//////////////////////////


	@Override
	public void updateCategory(String nom, int idc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void affecterProduitACategorie(int id_prod, int id_cat) {
		Category categoryManagedEntity = em.find(Category.class, id_cat);
		Product productManagedEntity = em.find(Product.class, id_prod);
		productManagedEntity.setCategorie(categoryManagedEntity);
	
	}

	@Override
	public Category findCategoryByName(String nom_cat) 
	{
		String req = "SELECT c FROM Category c WHERE c.category_type like :nom_cat";
		return(Category) em.createQuery(req).setParameter("nom_cat", nom_cat).getSingleResult();
		
	}

	///////////////*************** FIND ALL CATEGORIES **************//////////////////////////
	@Override
	public List<Category> findAll() {
		/* List<Category> query = em.createQuery("Select * from Category", Category.class).getResultList();
		  return query;
		  */
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Category.class));
		return em.createQuery(cq).getResultList();
		
	}
	
	///////////////*************** DISPLAY ALL CATEGORIES NAMES **************//////////////////////////

	@Override
	public List<String> DisplayALlCategoriesNames() {
	String req = "SELECT c.category_type FROM Category c";
	return em.createQuery(req).getResultList();
	}
	
	//************************************ RETURN CATEGORY'S ID BY NAME ***********************************//

	@Override
	public int returnCategoryID(String category_name) 
	{
		String req = "SELECT c.id_category FROM Category c WHERE c.category_type like :category_name";
		return (int) em.createQuery(req).setParameter("category_name", category_name).getSingleResult();	
	}

	@Override
	public String returnCategoryName(int id_category) {
		String req = "SELECT c.category_type FROM Category c WHERE c.id_category like :id_category";
		return (String) em.createQuery(req).setParameter("id_category", id_category).getSingleResult();
	}




}
