package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.StoreProduit;

@Stateful
public class ProductService implements ProductServiceLocal, ProductServiceRemote {

	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;
	
	
	///////////////*************** CREATE **************//////////////////////////
	@Override
	public Integer createProduct(Product p)
	{
		em.persist(p);
		return (p.getId_product());
	}


	///////////////*************** FIND **************//////////////////////////

	@Override
	public Product findProductById(int idp) {
		return em.find(Product.class, idp);
	}

	///////////////*************** DELETE **************//////////////////////////

	@Override
	public void deleteProduct(Product produit) {
		for(StoreProduit sp : produit.getStoreProduits()){
			em.remove(em.merge(sp));
		}
		em.remove(em.merge(produit));
		
	}

	///////////////*************** UPDATE **************//////////////////////////

	@Override
	public Product updateProduct(Product produit) {
		return em.merge(produit);
		
	}

///////////////*************** FIND ALL PRODUCTS **************//////////////////////////
	@Override
	public List<Product> findAll()
	{
		
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Product.class));
		return em.createQuery(cq).getResultList();

	}

   //*************retourne l'id de la catégorie du produit dont le nom et passé en paramètre***************//
	@Override
	public Integer retrunCategoryID(String name_product) 
	{

		String req = "SELECT p.categorie_id_category FROM Product p WHERE p.name_product like :name_product";
		return (Integer) em.createQuery(req).setParameter("name_product", name_product).getSingleResult();
	}
	
	@Override
	public List<Product> getProductByName(String title) {
		String jpql = "SELECT p FROM Product p WHERE p.name_product like :title";
		return em.createQuery(jpql).setParameter("title", title+"%").getResultList();
	}


	@Override
	public List<String> DisplayALlProductsNames() {
		String req = "SELECT p.name_product FROM Product p";
		return em.createQuery(req).getResultList();
	}

	///////////////*************** FIND ALL BRANDS **************//////////////////////////



	//**************************TRI PAR CATEGORIE ******************************************//

	/*@Override
	public List<Product> sortProductByCategory() {

		String req = "SELECT p FROM Product p ORDER BY p.category_name";
		return em.createQuery(req).getResultList();
	}
*/
	
	////////////////////syrine///////////////////////////////

	@Override
	public List<String> DisplayALlProductsNamesCyrine() {
		String req = "SELECT p.name_product FROM Product p";
		return em.createQuery(req).getResultList();
	}


	@Override
	public int findIdProductByTitleCyrine(String name_product) {
		String req = "SELECT t.id_product FROM Product t WHERE t.name_product like :name_product";
		return(int) em.createQuery(req).setParameter("name_product", name_product).getSingleResult();
	}


	@Override
	public void assignProductToOfferCyrine(int id_offre, int id_product ) {
		Product productManagedEntity = em.find(Product.class ,id_product); 
		Offre offreManagedEntity = em.find(Offre.class,id_offre);
		offreManagedEntity.setProduct(productManagedEntity);
		
		
	}


	@Override
	public Product FindProductByTitleCyrine(String name_product) {
		Product p = new Product(); 
		String req = "SELECT t FROM Product t WHERE t.name_product like :name_product";
		p=(Product) em.createQuery(req, Product.class).setParameter("name_product", name_product).getSingleResult();
		return p;
		
		
				
	}

}
