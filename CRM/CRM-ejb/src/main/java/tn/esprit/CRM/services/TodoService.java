package tn.esprit.CRM.services;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.CRM.persistence.Task;

import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.persistence.Events;


@Stateless
@LocalBean
public class TodoService implements TodoServiceRemote, TodoServiceLocal {
	
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;

    public TodoService() {
    	super();
    }

	public void create(User user) {
		em.persist(user);
	}
	@Override
	public List<String> findAll() {
		String req ="SELECT u.user_Name from User u ";
		return em.createQuery(req).getResultList();

		
	}
	public List<User> FindAllUser() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(User.class));
		return em.createQuery(cq).getResultList();
		
	}
	
	@Override
	public String findEmailUserByUsername(String user_Name) 
	{
		
		String req = "SELECT t.email FROM User t WHERE t.user_Name like :user_Name";
		return(String) em.createQuery(req).setParameter("user_Name", user_Name).getSingleResult();
	}
	@Override
	public int findIdUserByUsername(String user_Name) 
	{
		
		String req = "SELECT t.id_user FROM User t WHERE t.user_Name like :user_Name";
		return(int) em.createQuery(req).setParameter("user_Name", user_Name).getSingleResult();
	}
	@Override
	public void assignTaskToEmployee(int taskId, int userId) 
			{
			User userManagedEntity = em.find(User.class ,userId); 
			Task taskManagedEntity = em.find(Task.class,taskId);
			taskManagedEntity.setUser(userManagedEntity);
			
			}
	@Override
	public User getUserById (int UserID)
	{
	return em.find(User.class, UserID);
		
		}
	public User update(User entity) {
		return em.merge(entity);
	}
	@Override
	public void assignEventToEmployee(Events events, User employee) {
		List<Events> newOne = new ArrayList<>();
		newOne.add(events);
		employee.linkEventToThisEmployee(newOne);
		update(employee);
		
	}
	public List<User> findAllUser() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(User.class));
		return em.createQuery(cq).getResultList();
	}
	public User find(int entityID) {
		return em.find(User.class, entityID);
	}
}
