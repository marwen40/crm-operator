package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Store
 *
 */

@Entity
@Stateful
public class StoreProduit implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Integer id;
	@ManyToOne
	private Product product;
	@ManyToOne
	private Store store;
	
	private int quantity;

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
   
	
}
