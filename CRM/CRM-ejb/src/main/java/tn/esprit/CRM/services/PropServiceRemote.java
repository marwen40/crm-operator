package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Proposition;

@Remote
public interface PropServiceRemote {

	public void Ajouter(Proposition p);
	public List<Proposition> Afficher();
}
