package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.persistence.Product;

@Remote
public interface PackServiceRemote {
	public Integer createPack(Pack pack);
	public Integer update(Pack pack);
	public  void deleteOffre(Pack pack) ;
	public Pack findOffreByTitle(String title) ;
	public List<String> findpackbyid(int  id_pack) ;
	public void updateOffretitle(String title, int id_pack );
	public void invisible(Pack pack);
	public void visible(Pack pack);
	public void visiblestock(Pack pack);
	public int findIdAbonnemmentByTitle(String name_abonnement);
	public Integer Addprod (int id_pack,int id_product );
	public Integer Addabbon (int id_pack,int id_abonnement );
	public List<String> DisplayALlAbonnementNames();
	public List<Pack> findalladminPack();
	public List<Pack> findallInvisiblePack();
	public List<Pack> getPacksByTitle(String title);
	public  void sendMail(String email,String sujet,String contenu);
}
