package tn.esprit.CRM.services;



import java.util.List;

import javax.ejb.Remote;


import tn.esprit.CRM.persistence.Category;

@Remote
public interface CategoryServiceRemote 
{

	void createCategory(Category c);
	void deleteCategory(int idc);
	Category findCategoryById(int idc);
	Category findCategoryByName(String nom_cat);
	public List<Category> findAll();
	public void updateCategory(String nom, int idc);
	public void affecterProduitACategorie(int id_prod, int id_cat); 
	List<String> DisplayALlCategoriesNames();
	int returnCategoryID(String category_name);
	String returnCategoryName(int category_id);

	
}
