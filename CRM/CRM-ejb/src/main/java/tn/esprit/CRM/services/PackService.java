package tn.esprit.CRM.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tn.esprit.CRM.persistence.Abonnement;

import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.persistence.Product;
@Stateless
public class PackService implements PackServiceRemote{
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;

		@Override
	public Integer createPack(Pack pack) {
		pack.setVisibily_pack(false);
		em.persist(pack); 
		return pack.getId_pack();
	}

	@Override
	public Integer update(Pack pack) {
		em.merge(pack);
		return pack.getId_pack();
	}

	@Override
	public void deleteOffre(Pack pack) {
		Pack entityToBeRemoved = em.merge(pack);

		em.remove(entityToBeRemoved);
		
	}

	@Override
	public Pack findOffreByTitle(String title) {
		String req = "SELECT o FROM Pack o WHERE o.title_pack like :title";
		return (Pack) em.createQuery(req).setParameter("title_pack", title).getSingleResult();
	}

	@Override
	public void updateOffretitle(String title, int id_pack) {
		Pack pack = em.find(Pack.class,id_pack);
		pack.setTitle_pack(title);
		
	}

	@Override
	public void invisible(Pack pack) {
		
		pack.setVisibily_pack(false);
	       em.merge(pack);
		
		
	}

	@Override
	public void visible(Pack pack) {
		
		pack.setVisibily_pack(true);
	       em.merge(pack);
		
	}
	public void visibstock(){
		String req = "SELECT all FROM Pack  ";
		List<Pack> packs = new ArrayList<>();
		packs= em.createQuery(req).getResultList();
		
		for (Pack pack : packs) {
			if (pack.getNumbre() == 0){
				invisible(pack);
				
			}
		}
	}
	public void visibdate(){
		//String req = "SELECT all FROM Pack  ";
//		List<Pack> packs = new ArrayList();
//		packs= em.createQuery(req).getResultList();
//		
//		for (Pack pack : packs) {
//			Date d  = (Date) new java.util.Date();
//			int ans = pack.getEnd_date_pack().compareTo(d);
//			if (ans<0){
//				invisible(pack.getId_pack());
//				
//			}
//		}
	}
/*khedmet l abonnement*/
	

	@Override
	public int findIdAbonnemmentByTitle(String name_abonnement) {
		String req = "SELECT t.id_abonnement FROM Abonnement t WHERE t.name_abonnement like :name_abonnement";
		return(int) em.createQuery(req).setParameter("name_abonnement", name_abonnement).getSingleResult();
	}



	@Override
	public Integer Addprod(int id_pack ,int  id_product ) {
		Pack pack = em.find(Pack.class,id_pack);
		Product  product = em.find(Product.class,id_product);
List<Product> products  = pack.getProducts();
products.add(product);
pack.setProducts(products);
em.merge(pack);

		return id_pack;	
		}

	@Override
	public Integer Addabbon(int id_pack, int  id_abonnement) {
		Pack pack = em.find(Pack.class,id_pack);
		Abonnement  abonnement = em.find(Abonnement.class,id_abonnement);
List<Abonnement> abonnements  = pack.getAbonnement();
abonnements.add(abonnement);
pack.setAbonnement(abonnements);
em.merge(pack);

		return id_pack;	
	}

	@Override
	public List<String> DisplayALlAbonnementNames() {
		String req = "SELECT p.name_abonnement FROM Abonnement p";
		return em.createQuery(req).getResultList();
	}

	@Override
	public List<Pack> findalladminPack() {
		return em.createQuery("SELECT o FROM Pack o ", Pack.class).getResultList();
	}

	@Override
	public List<String> findpackbyid(int id_pack) {
		String req = "SELECT o FROM Pack o WHERE o.id_pack like :id_pack";
		Pack p = (Pack) em.createQuery(req).setParameter("id_pack", id_pack).getSingleResult();
		List<Product> products = p.getProducts();
		
		List<Abonnement> abonnments = p.getAbonnement();
		List<String> s  = new ArrayList<>();
for (Product product : products) {
			s.add(product.getName_product());
		}
				return (s) ;
	}

	@Override
	public void visiblestock(Pack pack) {
		if(pack.getNumbre()==0){
			pack.setVisibily_pack(false);

		}
	       em.merge(pack);		
	}

	@Override
	public List<Pack> findallInvisiblePack() {
		String req = "SELECT o FROM Pack o WHERE o.visibily_pack like :visibily_pack";
		return  em.createQuery(req).setParameter("visibily_pack", false).getResultList();
	}

	@Override
	public List<Pack> getPacksByTitle(String title_pack) {
		String jpql = "SELECT t FROM Pack t WHERE t.title_pack like :title_pack";
		return em.createQuery(jpql).setParameter("title", title_pack+"%").getResultList();

	}

	@Override
	public void sendMail(String email, String sujet, String contenu) {
		final String username = "cyrine.jouini@esprit.tn";
		final String password = "sirine.jouini4";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(email));
			message.setSubject(sujet);
			message.setText(contenu);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

		
	}

	

