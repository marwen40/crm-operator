package tn.esprit.CRM.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity
@PrimaryKeyJoinColumn(name = "id_user")
public class Admin extends User implements Serializable {

	@Enumerated(EnumType.STRING)
	private Role admin_type;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();

	}

	public Role getAdmin_type() {
		return admin_type;
	}

	public void setAdmin_type(Role admin_type) {
		this.admin_type = admin_type;
	}

}
