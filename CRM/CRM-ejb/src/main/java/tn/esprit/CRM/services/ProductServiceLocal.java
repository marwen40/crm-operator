package tn.esprit.CRM.services;


import java.util.List;

//import java.util.List;
//import java.util.Locale.Category;

import javax.ejb.Local;


import tn.esprit.CRM.persistence.Product;

@Local
public interface ProductServiceLocal {
	Integer createProduct(Product p);
	void deleteProduct(Product produit);
	Product findProductById(int idp);
	public Product updateProduct(Product produit);
	public List<Product> findAll();
	Integer retrunCategoryID(String name_product);
	public List<Product> getProductByName(String title);
	List<String> DisplayALlProductsNames();

	//tri par catégorie
	//List<Product> sortProductByCategory();

////Methode sirine lil offre wel pack
	public List<String> DisplayALlProductsNamesCyrine();
	public int findIdProductByTitleCyrine(String name_product) ;
	public void assignProductToOfferCyrine(int id_offre, int idprod);
	


}
