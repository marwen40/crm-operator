package tn.esprit.CRM.services;





import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Product;
import tn.esprit.CRM.persistence.Store;
import tn.esprit.CRM.persistence.StoreProduit;




@Local
public interface StoreServiceLocal 
{
	
	Integer createStore(Store s);
	void deleteStore(Store store);
	 Store findStoreById(int ids); 
	 Store findStoreByAddress(String address_store);
	 List<Store> findAllStores() ;
	 Store updateStore(Store store) ;
	 
	 //fct utile pour récupérer les noms des stores dans le combobox
	 List<String> DisplayALlStoresNames();
	
	 Store findStoreByName(String nom_Store);
	 public void affectProductStore(Product product, Store store, int quantity);
	 public List<StoreProduit> getAllStoreProduct();
	 public List<StoreProduit> getStoreProductByStore(Store store);

	 public StoreProduit searchStoreProduit(Store store,Product product); 


}
