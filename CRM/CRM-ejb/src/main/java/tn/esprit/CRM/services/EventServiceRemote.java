package tn.esprit.CRM.services;


import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Events;

import tn.esprit.CRM.util.IGenericDAO;

@Remote
public interface EventServiceRemote extends IGenericDAO<Events> {
	
	List<Events> findEventByName(String name);
	List<Events> findEventByDate(Date date);
	List<Events> sortByLocalisation(String localisation);
	List<Events> sortBySubject(String subject);
	List<Events> sortEventByDate();
}
