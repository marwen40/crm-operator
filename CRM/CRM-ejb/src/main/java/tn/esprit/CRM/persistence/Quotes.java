package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Quotes
 *
 */
@Entity

public class Quotes implements Serializable {

	
	private String open;
	private String close;
	private String high;
	private String lastPrice;
	private String low;
	private String name;
	private String netChange;
	private String percentChange;
	private String symbol;
	private String tradeTimestamp;
	private String volume;   
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private int id_quotes;
	private static final long serialVersionUID = 1L;

	public Quotes() {
		super();
	}   
	public String getOpen() {
		return this.open;
	}

	public void setOpen(String open) {
		this.open = open;
	}   
	public String getClose() {
		return this.close;
	}

	public void setClose(String close) {
		this.close = close;
	}   
	public String getHigh() {
		return this.high;
	}

	public void setHigh(String high) {
		this.high = high;
	}   
	public String getLastPrice() {
		return this.lastPrice;
	}

	public void setLastPrice(String lastPrice) {
		this.lastPrice = lastPrice;
	}   
	public String getLow() {
		return this.low;
	}

	public void setLow(String low) {
		this.low = low;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getNetChange() {
		return this.netChange;
	}

	public void setNetChange(String netChange) {
		this.netChange = netChange;
	}   
	public String getPercentChange() {
		return this.percentChange;
	}

	public void setPercentChange(String percentChange) {
		this.percentChange = percentChange;
	}   
	public String getSymbol() {
		return this.symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}   
	public String getTradeTimestamp() {
		return this.tradeTimestamp;
	}

	public void setTradeTimestamp(String tradeTimestamp) {
		this.tradeTimestamp = tradeTimestamp;
	}   
	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}   
	public int getId_quotes() {
		return this.id_quotes;
	}

	public void setId_quotes(int id_quotes) {
		this.id_quotes = id_quotes;
	}
   
}
