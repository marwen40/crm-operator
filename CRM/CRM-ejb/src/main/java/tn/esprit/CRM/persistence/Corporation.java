package tn.esprit.CRM.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entity implementation class for Entity: Corporation
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "id_user")
public class Corporation extends User implements Serializable {

	private String societyName;
	private String societyAddress;
	private Integer employeesNumber;
	private Integer id_fiscale;
	private String type_client;
	private static final long serialVersionUID = 1L;

	public Corporation() {
		super();
	}

	public String getSocietyName() {
		return this.societyName;
	}

	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}

	public String getSocietyAddress() {
		return this.societyAddress;
	}

	public void setSocietyAddress(String societyAddress) {
		this.societyAddress = societyAddress;
	}

	public Integer getEmployeesNumber() {
		return this.employeesNumber;
	}

	public void setEmployeesNumber(Integer employeesNumber) {
		this.employeesNumber = employeesNumber;
	}

	public Integer getId_fiscale() {
		return this.id_fiscale;
	}

	public void setId_fiscale(Integer id_fiscale) {
		this.id_fiscale = id_fiscale;
	}

	public String getType_client() {
		return type_client;
	}

	public void setType_client(String type_client) {
		this.type_client = type_client;
	}

}
