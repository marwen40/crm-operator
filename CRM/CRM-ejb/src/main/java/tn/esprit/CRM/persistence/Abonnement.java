package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Abonnement
 *
 */
@Entity
public class Abonnement implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_abonnement;
	private String name_abonnement ;
	private Integer number_month;
	private Float price_per_month;
	private Date start_date_abonnement;
	
	public Abonnement(String name_abonnement) {
		super();
		this.name_abonnement = name_abonnement;
	}
	
	@ManyToOne
	private Product produit ;
	@ManyToMany(fetch = FetchType.LAZY,mappedBy="abonnement",cascade = CascadeType.ALL )
	private List<Pack> packs;
	
	public List<Pack> getPacks() {
		return packs;
	}

	public void setPacks(List<Pack> packs) {
		this.packs = packs;
	}

	private static final long serialVersionUID = 1L;
	public Abonnement() {
		super();
	}
	
	public Product getProduit() {
		return produit;
	}

	public Abonnement(String name_abonnement, Integer number_month, Float price_per_month, Date start_date_abonnement,
			Product produit) {
		super();
		this.name_abonnement = name_abonnement;
		this.number_month = number_month;
		this.price_per_month = price_per_month;
		this.start_date_abonnement = start_date_abonnement;
		this.produit = produit;
	}

	public void setProduit(Product produit) {
		this.produit = produit;
	}

	public Date parseDate(Date d) throws ParseException{
		SimpleDateFormat formatAParser = new SimpleDateFormat("yyyy-MM-dd") ;
		String dateString = formatAParser.format(d);		
		Date DateParsed=  formatAParser.parse(dateString);
		return DateParsed ;
	}
	public Abonnement(String name_abonnement, Integer number_month, Float price_per_month, Date start_date_abonnement) {
		super();
		this.name_abonnement = name_abonnement;
		this.number_month = number_month;
		this.price_per_month = price_per_month;
		this.start_date_abonnement = start_date_abonnement;
	}


	public Abonnement(Integer number_month, Float price_per_month, Date start_date_abonnement) {
		super();
		this.number_month = number_month;
		this.price_per_month = price_per_month;
		this.start_date_abonnement = start_date_abonnement;
	}


	public Abonnement(Integer number_month) {
		super();
		this.number_month = number_month;
	}
	




	@Override
	public String toString() {
		return "Abonnement [name_abonnement=" + name_abonnement + ", number_month=" + number_month
				+ ", price_per_month=" + price_per_month + ", start_date_abonnement=" + start_date_abonnement;
	}


	public String getName_abonnement() {
		return name_abonnement;
	}


	public void setName_abonnement(String name_abonnement) {
		this.name_abonnement = name_abonnement;
	}


	public Integer getId_abonnement() {
		return id_abonnement;
	}
	public void setId_abonnement(Integer id_abonnement) {
		this.id_abonnement = id_abonnement;
	}
	public Integer getNumber_month() {
		return number_month;
	}
	public void setNumber_month(Integer number_month) {
		this.number_month = number_month;
	}
	public Float getPrice_per_month() {
		return price_per_month;
	}
	public void setPrice_per_month(Float price_per_month) {
		this.price_per_month = price_per_month;
	}
	public Date getStart_date_abonnement() {
		return start_date_abonnement;
	}
	public void setStart_date_abonnement(Date start_date_abonnement) {
		this.start_date_abonnement = start_date_abonnement;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
   
}
