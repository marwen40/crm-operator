package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Events;
import tn.esprit.CRM.persistence.User;

@Local
public interface TodoServiceLocal {
	
	void create(User user);
	List<String> findAll();
	 void assignTaskToEmployee(int taskId, int userId) ;
	 //List<String> findAllNames();
	 int findIdUserByUsername(String user_Name) ;
	 User getUserById (int UserID);
	 String findEmailUserByUsername(String user_Name) ;
	 public List<User> FindAllUser();
	 void assignEventToEmployee(Events events, User employee);
	 public User update(User entity);
	 public List<User> findAllUser();
	 public User find(int entityID) ;
	
}
