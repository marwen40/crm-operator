package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.User;

@Local
public interface UserServiceLocal {
	
	void createUser (User u);
	void deleteUser (Integer id_user);
	public void updateUser(User user);
	public List<User> findAll();
	User findUserByUserName(String username);
	User findUserByCIN(Integer cin);
	User findUserByEmail(String username);
	User findUserById(int id);
	public User login(String username, String password);
	public List<User> findAllClients (String role_user,String role_moral);
}
