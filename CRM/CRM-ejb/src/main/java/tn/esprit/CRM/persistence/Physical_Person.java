package tn.esprit.CRM.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Entity implementation class for Entity: Physical_Person
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "id_user")
public class Physical_Person extends User implements Serializable {

	private String type_client;

	private static final long serialVersionUID = 1L;

	public Physical_Person() {
		super();
	}

	public String getType_client() {
		return type_client;
	}

	public void setType_client(String type_client) {
		this.type_client = type_client;
	}

}
