package tn.esprit.CRM.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.CRM.persistence.Offre;
import tn.esprit.CRM.persistence.Pack;
import tn.esprit.CRM.persistence.Product;

@Stateless
public class OffreService implements OffreServiceLocal,OffreServiceRemote {
	@PersistenceContext(unitName="CRM-ejb")
	private EntityManager em;

	public OffreService() {
		super();
	}

	@Override
	public List<Offre> findalladmin() {
		return em.createQuery("SELECT o FROM Offre o ", Offre.class).getResultList();
		
	}

	@Override
	public List<Offre> findallclient() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer createOffre(Offre offre) {
		System.out.println(offre);
		offre.setVisibility_offre(true);
			em.persist(offre); 
		return offre.getId_offre();
		
	}

	@Override
	public Offre update(Offre offre) {
		return em.merge(offre);
		
	}

	@Override
	public void deleteOffre(Offre offre) {
		Offre entityToBeRemoved = em.merge(offre);

		em.remove(entityToBeRemoved);
		
		
	}

	@Override
	public Offre findOffreByTitle(String title) {
		String req = "SELECT o FROM Offre o WHERE o.title like :title";
		return (Offre) em.createQuery(req).setParameter("title", title).getSingleResult();
	}

	@Override
	public void updateOffretitle(String title, int id_offre) {
		Offre offre = em.find(Offre.class,id_offre);
		offre.setTitle_offre(title);
		
	}

	@Override
	public void invisible(Offre offre ) {
		
		  offre.setVisibility_offre(false);
	       em.merge(offre);
		
	}

	@Override
	public void visible(Offre offre ) {
		
		 offre.setVisibility_offre(true);
	       em.merge(offre);		
		
	}

	@Override
	public void visibdate(Offre offre) {
		
			String req = "SELECT all FROM Offre  ";
			List<Offre> offres = new ArrayList<>();
			//offres= em.createQuery(req).getResultList()s
			offres=em.createQuery(req).getResultList();
			for (Offre offress : offres) {
				Date d  = (Date) new java.util.Date();
				int ans = offre.getEnd_date_offre().compareTo(d);
				if (ans<0){
					invisible(offre);
					
				}
			}
		}
	
	@Override
	public List<String> findproductlibre(){
		List<Offre> offres = em.createQuery("SELECT o FROM Offre o ").getResultList();
		List<Product> products = em.createQuery("SELECT o FROM Product o ").getResultList();
		List<String> productslibre = new ArrayList<>();
		for (Product product : products) {
			boolean lib = true;
			for (Offre offre : offres) {
				if (offre.getProduct().getId_product() == product.getId_product()){
					lib = false;
				}
			}
			if(lib == true ){
				productslibre.add(product.getName_product());
			}
		}
		return productslibre;
	}

	@Override
	public List<String> findprocutre9ed() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Offre> getOffersByTitle(String title_offre) {
		
			String jpql = "SELECT t FROM Pack t WHERE t.title_offre like :title_offre";
			return em.createQuery(jpql).setParameter("title_offre", title_offre+"%").getResultList();

		
		
	} }

	
	


