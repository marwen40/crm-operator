package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Offre
 *
 */
@Entity

public class Offre implements Serializable {

	   
	@Id    
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id_offre;
	private String title_offre;
	private Date start_date_offre;
	private Date end_date_offre;
	private Boolean visibility_offre;
	private Float percent_gain_offre;
	private static final long serialVersionUID = 1L;
	@OneToOne
	 @JoinColumn(name = "product_id")	
	private Product product ;

	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Offre() {
		//super();
	}   
	
	public Integer getId_offre() {
		return this.id_offre;
	}

	public void setId_offre(Integer id_offre) {
		this.id_offre = id_offre;
	}   
	public String getTitle_offre() {
		return this.title_offre;
	}

	public void setTitle_offre(String title_offre) {
		this.title_offre = title_offre;
	}   
	public Date getStart_date_offre() {
		return this.start_date_offre;
	}

	public void setStart_date_offre(Date start_date_offre) {
		this.start_date_offre = start_date_offre;
	}   
	public Date getEnd_date_offre() {
		return this.end_date_offre;
	}

	public void setEnd_date_offre(Date end_date_offre) {
		this.end_date_offre = end_date_offre;
	}   
	public Boolean getVisibility_offre() {
		return this.visibility_offre;
	}

	public void setVisibility_offre(Boolean visibility_offre) {
		this.visibility_offre = visibility_offre;
	}   
	public Float getPercent_gain_offre() {
		return this.percent_gain_offre;
	}

	public void setPercent_gain_offre(Float percent_gain_offre) {
		this.percent_gain_offre = percent_gain_offre;
	}
	
	public Offre(String title_offre) {
		//super();
		this.title_offre = title_offre;
	}
	public Offre(String title_offre, Date start_date_offre, Date end_date_offre, Float percent_gain_offre
			) {
		super();
		this.title_offre = title_offre;
		this.start_date_offre = start_date_offre;
		this.end_date_offre = end_date_offre;
		this.percent_gain_offre = percent_gain_offre;

	}
	public Offre(Integer id_offre, String title_offre, Date start_date_offre, Date end_date_offre,
			Boolean visibility_offre, Float percent_gain_offre, Product product) {
		super();
		this.id_offre = id_offre;
		this.title_offre = title_offre;
		this.start_date_offre = start_date_offre;
		this.end_date_offre = end_date_offre;
		this.visibility_offre = visibility_offre;
		this.percent_gain_offre = percent_gain_offre;
		this.product = product;
	}
	@Override
	public String toString() {
		return "Offre [id_offre=" + id_offre + ", title_offre=" + title_offre + ", start_date_offre=" + start_date_offre
				+ ", end_date_offre=" + end_date_offre + ", visibility_offre=" + visibility_offre
				+ ", percent_gain_offre=" + percent_gain_offre + ", product=" + product + "]";
	}
	
	
	
	
	
   
}
