package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity
@Stateful
public class Category implements Serializable {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_category;
	private String category_type;
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy="categorie",cascade={CascadeType.PERSIST , CascadeType.REMOVE} , fetch=FetchType.LAZY)
	private List<Product> produits1;


	
	
		
	public Category(String category_type) {
		super();
		this.category_type = category_type;
	}



	@Override
	public String toString() {
		
		return category_type ;
	}
	
	


	public List<Product> getProduits1() {
		return produits1;
	}
	public void setProduits1(List<Product> produits1) {
		this.produits1 = produits1;
	}
	public Category() {
		super();
	}   
	public Integer getId_category() {
		return this.id_category;
	}

	public void setId_category(Integer id_category) {
		this.id_category = id_category;
	}   
	public String getCategory_type() {
		return this.category_type;
	}

	public void setCategory_type(String category_type) {
		this.category_type = category_type;
	}
   
}
