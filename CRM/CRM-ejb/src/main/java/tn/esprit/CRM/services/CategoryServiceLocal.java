package tn.esprit.CRM.services;


import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Category;

@Local
public interface CategoryServiceLocal
{
	void createCategory(Category c);
	void deleteCategory(int idc);
	Category findCategoryById(int idc);
	Category findCategoryByName(String nom_cat);
	public List<Category> findAll();
	public void updateCategory(String nom, int idc);
	public void affecterProduitACategorie(int id_store, int id_cat); 
	List<String> DisplayALlCategoriesNames();
	int returnCategoryID(String category_name);
	String returnCategoryName(int category_id);


}
