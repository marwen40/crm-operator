package tn.esprit.CRM.persistence;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



/**
 * Entity implementation class for Entity: Product
 *
 */

@Entity
@Stateful
public class Product implements Serializable 
{
	
	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id_product;
	private String name_product;
	private Integer quantity_product;
	private Float price_product;
	private String description_product;
	private String picture_product;
	private String brand_name;
	private String category_name;
	private String imageName;

	private Date date_creationp;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="product",cascade=CascadeType.REMOVE,fetch=FetchType.EAGER)
	private List<StoreProduit> storeProduits;
	@OneToOne
	private Abonnement abonnement;
	
	@ManyToOne
	private Category categorie;
	
	@ManyToOne
	private Basket basket;
	//////syrine
	@OneToOne( mappedBy="product")
	private Offre offres;
	
	public Offre getOffres() {
		return offres;
	}
	public void setOffres(Offre offres) {
		this.offres = offres;
	}
	@ManyToMany(fetch = FetchType.LAZY,mappedBy="products",cascade = CascadeType.ALL)
	private List<Pack> packs;
	

	public List<Pack> getPacks() {
		return packs;
	}
	public void setPacks(List<Pack> packs) {
		this.packs = packs;
	}
	

	
	
	
	
	
	/////////////
	/*
	 * RELATION ManyToOne AVEC STORE
	 * @ManyToOne
	 * private Store store; 
	 */

	
/*	public Product(String name_product, Integer quantity_product, Float price_product, String description_product) {
		super();
		this.name_product = name_product;
		this.quantity_product = quantity_product;
		this.price_product = price_product;
		this.description_product = description_product;
	}

	*/
	
	
	public List<StoreProduit> getStoreProduits() {
		return storeProduits;
	}


	public void setStoreProduits(List<StoreProduit> storeProduits) {
		this.storeProduits = storeProduits;
	}


	public Product(String brand_name) {
		super();
		this.brand_name = brand_name;
	}


	/*public Product(String name_product, Integer quantity_product, Float price_product, String description_product,
			String picture_product, String brand_name, String category_name) {
		super();
		this.name_product = name_product;
		this.quantity_product = quantity_product;
		this.price_product = price_product;
		this.description_product = description_product;
		this.picture_product = picture_product;
		this.brand_name = brand_name;
		this.category_name = category_name;
	}*/
	




	public Product(String name_product, Integer quantity_product) {
		super();
		this.name_product = name_product;
		this.quantity_product = quantity_product;
	}



	public Product(String name_product, Integer quantity_product, Float price_product, String description_product,
			String picture_product, String brand_name, String category_name, Date date_creationp) {
		super();
		this.name_product = name_product;
		this.quantity_product = quantity_product;
		this.price_product = price_product;
		this.description_product = description_product;
		this.picture_product = picture_product;
		this.brand_name = brand_name;
		this.category_name = category_name;
		this.date_creationp = date_creationp;
	}


	public Product() {
			super();
		}

	public Basket getBasket() {
		return basket;
	}
	
	
	
	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	public void setBasket(Basket basket) {
		this.basket = basket;
	}
	public Category getCategorie() {
		return categorie;
	}
	public void setCategorie(Category categorie) {
		this.categorie = categorie;
	}
	public Abonnement getAbonnement() {
		return abonnement;
	}
	public void setAbonnement(Abonnement abonnement) {
		this.abonnement = abonnement;
	}
 
	public Integer getId_product() {
		return this.id_product;
	}

	public void setId_product(Integer id_product) {
		this.id_product = id_product;
	}   
	public String getName_product() {
		return this.name_product;
	}

	public void setName_product(String name_product) {
		this.name_product = name_product;
	}   
	public Integer getQuantity_product() {
		return this.quantity_product;
	}

	public void setQuantity_product(Integer quantity_product) {
		this.quantity_product = quantity_product;
	}   
	public Float getPrice_product() {
		return this.price_product;
	}

	public void setPrice_product(Float price_product) {
		this.price_product = price_product;
	}   
	public String getDescription_product() {
		return this.description_product;
	}

	public void setDescription_product(String description_product) {
		this.description_product = description_product;
	}   
	public String getPicture_product() {
		return this.picture_product;
	}

	public void setPicture_product(String picture_product) {
		this.picture_product = picture_product;
	}

	

	public String getCategory_name() {
		return category_name;
	}



	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}



	public String getBrand_name() {
		return brand_name;
	}



	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}



	public Date getDate_creationp() {
		return date_creationp;
	}


	public void setDate_creationp(Date date_creationp) {
		this.date_creationp = date_creationp;
	}

	


	@Override
	public String toString() {
		return name_product;
	}

}

	

   

