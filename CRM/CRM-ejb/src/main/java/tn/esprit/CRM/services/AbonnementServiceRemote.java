package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.CRM.persistence.Abonnement;
import tn.esprit.CRM.persistence.Product;

@Remote
public interface AbonnementServiceRemote {
	public void create(Abonnement abonnement);
	public void deleteAbonnement(Abonnement abonnement);
	public Abonnement findAbonnementById(int idAbonnement);
	public void updateAbonnement(Abonnement abonnement);
	public List<Abonnement> getAllAbonnement();
	public Abonnement findAbonnementByProduct(Product produit);

}
