package tn.esprit.CRM.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.CRM.persistence.Offre;

@Local
public interface OffreServiceLocal {
	public List <Offre> findalladmin();
	public List<Offre> findallclient();
	public Integer createOffre(Offre offre);
	public Offre update(Offre offre);
	public void deleteOffre(Offre offre);
	public Offre findOffreByTitle(String title) ;
	public void updateOffretitle(String title, int id_offre );
	public void invisible(Offre offre);
	public void visible(Offre offre);
	public void visibdate(Offre offre);
	public List<String> findproductlibre();
	public List<String> findprocutre9ed();

}
