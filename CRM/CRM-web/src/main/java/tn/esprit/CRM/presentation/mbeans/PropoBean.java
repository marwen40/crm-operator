package tn.esprit.CRM.presentation.mbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.CRM.persistence.Proposition;
import tn.esprit.CRM.services.PropServiceLocal;

@ManagedBean
@SessionScoped
public class PropoBean {

	private List<Proposition> props=new ArrayList<Proposition>();
	Proposition proposition=new Proposition();
	private int id_proposition;
	private String Description;
	private RepeatPaginator paginator;
	public RepeatPaginator getPaginator() {
		return paginator;
	}

	public void setPaginator(RepeatPaginator paginator) {
		this.paginator = paginator;
	}
	@EJB
	private PropServiceLocal ServiceLocal;
	
	public String doAddCo() {

		ServiceLocal.Ajouter(proposition);	
		intitialization();
		paginator =new RepeatPaginator(props);
		proposition = new Proposition();
		String n = "/ajouterp?faces-redirect=true";
		return n; 
	}
	public void addComment()
	{  
		proposition.setDescription(Description);
		ServiceLocal.Ajouter(proposition);
		//commentService.addComment(new Comment(date, comment, blog, rating, jobyuser));
		
	}

	@PostConstruct
	public void intitialization() {

		props = ServiceLocal.Afficher();
	}
	
	
	public String affiche() {
		System.out.println("mmmmmmmmmmmaaaaaaaaa certtiiiiiiifff:::::" );
		intitialization();
		String n = "/AddConge?faces-redirect=true";

		return n;
	}

	
	public List<Proposition> getProps() {
		return props;
	}
	public void setProps(List<Proposition> props) {
		this.props = props;
	}
	public int getId_proposition() {
		return id_proposition;
	}
	public void setId_proposition(int id_proposition) {
		this.id_proposition = id_proposition;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}

	public Proposition getProposition() {
		return proposition;
	}

	public void setProposition(Proposition proposition) {
		this.proposition = proposition;
	}

}
