package tn.esprit.CRM.presentation.mbeans;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.PieChartModel;

import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.services.TaskServiceLocal;

@ManagedBean
@ViewScoped
public class StaticsBean {

	private PieChartModel pieModel1;
	private List<Task> tasks;


	@EJB
	private TaskServiceLocal TaskServiceLocal;



	@PostConstruct
	public void init() {

		List<Task> ToDo = new ArrayList<>();
		List<Task> Doing = new ArrayList<>();
		List<Task> Done = new ArrayList<>();
	
	
		
		tasks = TaskServiceLocal.FindAllTask();

		for (Task task : tasks) {
			if (task.getStatus_task().equalsIgnoreCase("ToDo")) {

				ToDo.add(task);
			}
			if (task.getStatus_task().equalsIgnoreCase("Doing")) {

				Doing.add(task);
			}
			if (task.getStatus_task().equalsIgnoreCase("Done")) {

				Done.add(task);
			}
		
		}
	
		pieModel1 = new PieChartModel();

		pieModel1.set("ToDo", ToDo.size());
		pieModel1.set("Doing", Doing.size());
		pieModel1.set("Done", Done.size());
		pieModel1.setTitle("Task Status Stype  Statistics");
		pieModel1.setLegendPosition("w");


		
		

	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	
}
