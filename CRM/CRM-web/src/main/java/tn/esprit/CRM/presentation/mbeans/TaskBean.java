package tn.esprit.CRM.presentation.mbeans;



import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


import tn.esprit.CRM.persistence.Status_type;
import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.TaskServiceLocal;
import tn.esprit.CRM.services.TodoServiceLocal;



@ManagedBean
@ViewScoped
public class TaskBean {
	private Task task;
	private List<Task> tasks;
	private Integer idTask;
	private Integer idEmployee;
	private String nomEmployee;
	public String getNomEmployee() {
		return nomEmployee;
	}

	public void setNomEmployee(String nomEmployee) {
		this.nomEmployee = nomEmployee;
	}

	private List<User> employees;
	private boolean showForm;





	@EJB
	private TaskServiceLocal taskServiceLocal;

	@EJB
	private TodoServiceLocal employeeServiceLocal;

	@PostConstruct
	private void init() {
		task = new Task();
		employees = employeeServiceLocal.FindAllUser();
		tasks = taskServiceLocal.FindAllTask();
		showForm = false;
		

	}

	public void doSelect() {
		showForm = true;
	}

	public void doCancel() {
		showForm = false;
	}
	

	public void doDeleteTask() {
		taskServiceLocal.deleteTask(task);
		this.init();
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Integer getIdTask() {
		return idTask;
	}

	public void setIdTask(Integer idTask) {
		this.idTask = idTask;
	}

	public List<User> getEmployees() {
		return employees;
	}

	public void setEmployees(List<User> employees) {
		this.employees = employees;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}



	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	
}
