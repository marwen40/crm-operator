package tn.esprit.CRM.presentation.mbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.DragDropEvent;

import tn.esprit.CRM.persistence.Status_type;
import tn.esprit.CRM.persistence.Task;
import tn.esprit.CRM.services.TaskServiceLocal;


@ManagedBean
@ViewScoped
public class TaskPlanningBean {
	private Task task;
	private List<Task> droppedPT;
	private List<Task> droppedPD;
	private List<Task> todo;
	private List<Task> doing;
	private List<Task> done;
	@EJB
	private TaskServiceLocal taskServiceLocal;

	
	@PostConstruct
	public void init(){
		droppedPT = new ArrayList<>();
		droppedPD = new ArrayList<>();
		todo = new ArrayList<>();
		doing = new ArrayList<>();
		done = new ArrayList<>();
		todo = taskServiceLocal.findTaskByStatus("ToDo");
		doing = taskServiceLocal.findTaskByStatus("Doing");
		done = taskServiceLocal.findTaskByStatus("Done");
	}
	public void doUpdateDoing(){
		Status_type taskStatus;
		taskStatus = Status_type.valueOf("Doing");
		task.setStatus_task(taskStatus.toString());
		taskServiceLocal.update(task);
		this.init();
	}
	public void doUpdateDone(){
		Status_type taskStatus;
		taskStatus = Status_type.valueOf("Done");
		task.setStatus_task(taskStatus.toString());
		taskServiceLocal.update(task);
		this.init();
	}
	public void onTodoDrop(DragDropEvent ddEvent) {
        Task p = ((Task) ddEvent.getData());
        droppedPT.add(p);
        todo.remove(p);
    }
	public void onDoingDrop(DragDropEvent ddEvent) {
        Task p = ((Task) ddEvent.getData());
        droppedPD.add(p);
        doing.remove(p);
    }

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public List<Task> getTodo() {
		return todo;
	}

	public void setTodo(List<Task> todo) {
		this.todo = todo;
	}

	public List<Task> getDoing() {
		return doing;
	}

	public void setDoing(List<Task> doing) {
		this.doing = doing;
	}

	public List<Task> getDone() {
		return done;
	}

	public void setDone(List<Task> done) {
		this.done = done;
	}
	public List<Task> getDroppedPT() {
		return droppedPT;
	}
	public void setDroppedPT(List<Task> droppedPT) {
		this.droppedPT = droppedPT;
	}
	public List<Task> getDroppedPD() {
		return droppedPD;
	}
	public void setDroppedPD(List<Task> droppedPD) {
		this.droppedPD = droppedPD;
	}
	
	
}
