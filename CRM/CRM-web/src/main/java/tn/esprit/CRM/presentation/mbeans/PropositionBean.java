package tn.esprit.CRM.presentation.mbeans;

import java.security.Provider.Service;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.CloseEvent;
import org.primefaces.event.MoveEvent;

import tn.esprit.CRM.persistence.Proposition;
import tn.esprit.CRM.persistence.Staff;
import tn.esprit.CRM.persistence.User;
import tn.esprit.CRM.services.PropService;
import tn.esprit.CRM.services.StaffService;

@ManagedBean
@SessionScoped
public class PropositionBean {
	private Integer id_proposition;
	private String Description;
	
	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public PropService getCommentService() {
		return commentService;
	}

	public void setCommentService(PropService commentService) {
		this.commentService = commentService;
	}

	public Staff getUser() {
		return user;
	}

	public void setUser(Staff user) {
		this.user = user;
	}

	public StaffService getSeviceStaff() {
		return seviceStaff;
	}

	public void setSeviceStaff(StaffService seviceStaff) {
		this.seviceStaff = seviceStaff;
	}



	@EJB
	PropService commentService;
	
	private Staff user;
	StaffService seviceStaff=new StaffService();
	public Integer getId_proposition() {
		return id_proposition;
	}

	public void setId_proposition(Integer id_proposition) {
		this.id_proposition = id_proposition;
	}

	

	public void setComments(List<Proposition> comments) {
		this.comments = comments;
	}



	private List<Proposition> comments;
	

	public void addComment()
	{  
		commentService.Ajouter(new Proposition());
		
		//commentService.addComment(new Comment(date, comment, blog, rating, jobyuser));
	}

	public List<Proposition> getComments() {
		comments = commentService.Afficher(); 
		return comments;
	}
	
	
	public void handleClose(CloseEvent event) {
        addMessage(event.getComponent().getId() + " closed", "So you don't like nature?");
    }
     
    public void handleMove(MoveEvent event) {
        addMessage(event.getComponent().getId() + " moved", "Left: " + event.getLeft() + ", Top: " + event.getTop());
    }
     
    public void destroyWorld() {
        addMessage("System Error", "Please try again later.");
    }
     
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
